import sys
sys.path.append("/home/goatchurch/datalogging/arduinosketchbook/pyscripts")
from flightlog import RL
from flightlog.utils import GetLatestSDfile
from basicgeo import P3, OctahedronAngle
fname = "/home/goatchurch/datalogging/permlogs/2015-07-10-greifenburg.TXT"
rl = RL(fname, sendactivity)
rl.plot("baro")
sendactivity("clearallpoints")
from flightsequence import FlightSequence, SliceTV, MakeFlatFls
sendactivity("contours", contours=[[(t*rl.tfac, p[2]*rl.gfac)  for t, p in rl.postrack.GetPath3()[0]]])
x = rl.postrack.GetPath(fac=rl.gfac)
len(x)
x[0][0]

from flightlog.utils import convexpbiway, ziptvs

t0, t1 = 199.5/rl.tfac, 217/rl.tfac
import math
pfac = 1/1000
poff = math.floor(rl.baros.plo*pfac-1)/pfac
pyoff = 0
bcont = [(pl["t"]*rl.tfac, (pl["p"]-poff)*pfac+pyoff)  for pl in rl.baros.Bpls  if t0<pl["t"]<t1 ]
sendactivity("contours", contours=[bcont])
sendactivity("clearallpoints")
sbcont = convexpbiway(bcont, 0.1)
sendactivity("contours", contours=[sbcont], materialnumber=1)

wcont = [(pl["t"]*rl.tfac, 8000/pl["w"])  for pl in rl.windspeed.WplsL  if t0<pl["t"]<t1]
len(wcont)
sendactivity("contours", contours=[wcont], materialnumber=3)
swcont = convexpbiway(wcont, 0.01)
sendactivity("contours", contours=[swcont], materialnumber=3)
k = 100
dsbcont = [ (sbcont[i][0]+0.03, (sbcont[i+k][1]-sbcont[i-k][1])/(sbcont[i+k][0]-sbcont[i-k][0]))  for i in range(k, len(sbcont)-k)] # shift by 2 seconds
sendactivity("contours", contours=[dsbcont], materialnumber=0)

# in theory this should be the polar curve
# however I think that the lower speed means we've just slowed down, 
# so this change of speed needs factoring
# or we have to be very strict with holding a speed constant at a time.
# use the pitot tube and watch the windspeed reading on it for 20 seconds at a time
x = ziptvs(dsbcont, swcont)
len(x)
x[100]
sendactivity("points", points=[(y[1],y[2])  for y in x])


---------------

import sys, math
sys.path.append("/home/goatchurch/datalogging/flightloganalysis")
from flightlog import RL
from flightlog.utils import GetLatestSDfile, TrapeziumAvg, TSampleRateG, SimpleLinearRegression, differentiatetvs
from flightlog.utils import TimeSampleRate, convexp2, varexp2, convexpbiway, ziptvs, ziptvsG
from basicgeo import P3, P2, Quat, Along
from flightlog.differentials import AccelerationDerivations
import flightlog.utils
from flightlog import ShowOrientSpectrum

fname = "/home/goatchurch/hgstuff/2016-05-12-llangollen/005.TXT"


rl = RL(fname, sendactivity)
rl.gfac = 0.01
rl.avgdrift = P3(rl.avgdrift[0], rl.avgdrift[1], 0)

rl.plot("diffacont")
rl.plot("diffgcont")
rl.plot("alttmp")
rl.plot("baro")
rl.plot("wind")

rl.plot("avgdrift13")


