import re, time, math, scipy.optimize

digpower = [ 1 << 4*i  for i in range(16) ]

def parseline(line):
    res = { "C":line[0] }
    for k, v1, v in re.findall("([a-z]+)(?:(\"[^\"\n]*\")|([0-9A-F]+))", line[1:]):
        if v:
            res[k] = int(v, 16) - (re.match("[89A-F]", v[0]) and digpower[len(v)] or 0)
        else:
            assert v1, [line]
            res[k] = v1[1:-1]  # isodate
    return res

class RL:
    def __init__(self, fname, lsendactivity):
        self.sendactivity = lsendactivity
        fin = open(fname)
        print(fin.readline())
        while not re.match("\s*$", fin.readline()):  pass
        self.plines = [ parseline(line)  for line in fin ]
        
        self.Cpl = {}
        for pl in self.plines:
            if pl["C"] not in self.Cpl:
                pl["N"] = 0
                self.Cpl[pl["C"]] = pl
            self.Cpl[pl["C"]]["N"] += 1
            
        print(" ".join("%s(%d)" % (pl["C"], pl["N"])  for pl in self.Cpl.values()))
        
        if "R" in self.Cpl:
            Rpl = self.Cpl["R"]
            self.latdivisorE100, self.latdivisorN100 = Rpl["e"], Rpl["n"] 
            self.lngdivisorE100, self.lngdivisorN100 = Rpl["f"], Rpl["o"]
            print(Rpl)
            print(Rpl["d"], self.latdivisorE100, self.latdivisorN100, self.lngdivisorE100, self.lngdivisorN100)
        mins = (self.plines[-1]["t"] - self.plines[0]["t"])/1000/60
        if mins < 120:
            print("duration", mins, "minutes")
        else:
            print("duration", mins/60, "hours")
            
    def gpstrace(self, fac=1.0):
        Rpl = self.Cpl["R"]
        Qpl = self.Cpl["Q"]
        latdivisorE100, latdivisorN100 = Rpl["e"], Rpl["n"] 
        lngdivisorE100, lngdivisorN100 = Rpl["f"], Rpl["o"]

        cont = [ ]
        for pl in self.plines:
            if pl["C"] == 'Q':
                rx = 100*(pl["x"]-Qpl["x"])
                ry = 100*(pl["y"]-Qpl["y"])
                me, mn = ry/latdivisorE100+rx/lngdivisorE100, ry/latdivisorN100+rx/lngdivisorN100
                x, y = me*fac, mn*fac
                cont.append((x, y, (pl["a"]-0*Qpl["a"])*0.1*fac))
        return cont

    def PlotDallas(self):
        self.sendactivity("clearalltriangles")
        self.sendactivity("clearallcontours")

        for i in range(3):
            print(i, sum(pl["i"] == i and 1 or 0  for pl in self.plines  if pl["C"] == "D"))

        unitsperhour = 60
        for i in range(3):
            cont = [(pl["t"]/60000*unitsperhour/60, pl["c"]/16)  for pl in self.plines  if pl["C"] == "D" and pl["i"] == i]
            self.sendactivity("contours", contours=[cont], materialnumber=i)
            
        # tmp36 measurements
        cont = [(pl["t"]/60000*unitsperhour/60, (pl["v"]*1024/32767 - 500)/10)  for pl in self.plines  if pl["C"] == "A"]
        self.sendactivity("contours", contours=[cont], materialnumber=1)

        self.sendactivity("contours", contours=[[(0,i), (1000,i)]  for i in range(0, 21)], materialnumber=1)
        self.sendactivity("contours", contours=[[(0,i), (1000,i)]  for i in range(0, 21, 5)], materialnumber=0)
        self.sendactivity("contours", contours=[[(i*unitsperhour,0), (i*unitsperhour,20)]  for i in range(0, 51)], materialnumber=1)


    def TrimStationarySections(self, nchunksextend=2):
        chunktimems = 5000
        plinechunks = [ ]
        for i, pl in enumerate(self.plines):
            if "t" not in pl:  continue
            if not plinechunks or pl["t"] > t0 + chunktimems:
                t0 = pl["t"]
                plinechunks.append({"i0":i})
            plinechunks[-1]["i1"] = i
        print(len(plinechunks), "Chunks")
        for plinechunk in plinechunks:
            lplines = self.plines[plinechunk["i0"]:plinechunk["i1"]+1]
            try:  maxv = max(pl["v"]/360  for pl in lplines  if pl["C"] == "V")
            except ValueError:  maxv = 0
            try:  maxb = max(pl["p"]  for pl in lplines  if pl["C"] == "B")
            except ValueError:  maxb = 0
            try:  minb = min(pl["p"]  for pl in lplines  if pl["C"] == "B")
            except ValueError:  minb = 0
            plinechunk["Mov"] = (maxv > 5.0 and maxb - minb > 15)
        
        ci0 = 0
        while ci0 + 3 < len(plinechunks):
            if plinechunks[ci0]["Mov"] and (plinechunks[ci0+1]["Mov"] or plinechunks[ci0+2]["Mov"]):
                break
            ci0 += 1
            
        ci1 = len(plinechunks) - 1
        while ci1 > ci0 + 3:
            if plinechunks[ci1]["Mov"] and (plinechunks[ci1-1]["Mov"] or plinechunks[ci1-2]["Mov"]):
                break
            ci1 -= 1
        print(ci0, ci1)
            
        # these are the chunks surrounding the flight movement
        ci0 = max(0, ci0 - nchunksextend)
        ci1 = min(len(plinechunks) - 1, ci1 + nchunksextend)
        print("ci", ci0, ci1, len(plinechunks))
        
        # find stable altitude/barometer relations
        abres = [ ]
        for ci in [ci0//2, (ci1 + len(plinechunks) - 1)//2]:
            sumalt10, nalt = 0.0, 0
            sumbaro, nbaro = 0.0, 0
            for cci in range(max(0, ci - 2), min(len(plinechunks) - 1, ci + 2)):
                if plinechunks[cci]["Mov"]:
                    continue
                for pl in self.plines[plinechunks[ci]["i0"]:plinechunks[ci]["i1"]+1]:
                    if pl["C"] == "B":
                        sumbaro += pl["p"]
                        nbaro += 1
                    elif pl["C"] == "Q":
                        sumalt10 += pl["a"]
                        nalt += 1
            abres.append(sumalt10*0.1/nalt)
            abres.append(sumbaro/nbaro)
        self.alt0, self.baro0, self.alt1, self.baro1 = abres
            
        # trim the sections and reset the t values down
        self.plines = self.plines[plinechunks[ci0]["i0"]:plinechunks[ci1]["i1"]+1]
        t0 = self.plines[0]["t"]
        for pl in self.plines:
            if "t" in pl:
                pl["t"] -= t0

        mins = (self.plines[-1]["t"] - self.plines[0]["t"])/1000/60
        if mins < 120:
            print("new duration", mins, "minutes")
        else:
            print("new duration", mins/60, "hours")
        print("alt/baro0", self.alt0, self.baro0, "alt/baro1", self.alt1, self.baro1)
        
        # update initial take-off point
        for pl in self.plines:
            if pl["C"] == "Q":
                self.Cpl["Q"] = pl
                break
    
    def altibaromc(self):
        rl = self
        rl.contB = [(pl["t"]/1000, pl["p"])  for pl in rl.plines  if pl["C"] == "B"]
        rl.contA = [(pl["t"]/1000, pl["a"]*0.1)  for pl in rl.plines  if pl["C"] == "Q"]

        rl.sendactivity("contours", contours=[[(t/60, (p-100000)/100)  for t, p in rl.contB]], materialnumber=0)
        rl.sendactivity("contours", contours=[[(t/60, a/10)  for t, a in rl.contA]], materialnumber=1)
        rl.rcontB = tconvolveexpS(self.contB, 100)
        rl.sendactivity("contours", contours=[[(t/60, (p-100000)/100)  for t, p in rl.rcontB]], materialnumber=2)
        
        pts = []
        i = 0
        for t, a in rl.contA:
            while i < len(rl.contB) and rl.contB[i][0] < t:
                i += 1
            if i == len(rl.contB):
                break
            pts.append((a, rl.contB[i][1]))
        rl.sendactivity("points", points=[(a/10, (p-100000)/100)  for a, p in pts], materialnumber=3)

        # find least squares to fit p -> a
        sa = sum(a  for a, p in pts)
        sa2 = sum(a**2  for a, p in pts)
        sap = sum(a*p  for a, p in pts)
        sp = sum(p  for a, p in pts)
        sp2 = sum(p**2  for a, p in pts)

        self.mb = (sap*len(pts) - sa*sp) / (sp2*len(pts) - sp*sp)
        self.cb = (sa - self.mb*sp)/len(pts)
        rl.sendactivity("contours", contours=[[((self.mb*p + self.cb)/10, (p-100000)/100)  for p in [80000, 110000]]])
        

        Rpl = self.Cpl["R"]
        Qpl = self.Cpl["Q"]
        latdivisorE100, latdivisorN100 = Rpl["e"], Rpl["n"] 
        lngdivisorE100, lngdivisorN100 = Rpl["f"], Rpl["o"]

        fac = 0.1
        cont = [ ]
        pb = 0
        i = 0
        for pl in rl.plines:
            if pl["C"] == 'Q':
                while i < len(rl.rcontB) - 1 and rl.rcontB[i][0] < pl["t"]/1000:
                    i += 1
                rx = 100*(pl["x"]-Qpl["x"])
                ry = 100*(pl["y"]-Qpl["y"])
                me, mn = ry/latdivisorE100+rx/lngdivisorE100, ry/latdivisorN100+rx/lngdivisorN100
                x, y = me*fac, mn*fac
                pb = self.rcontB[i][1]
                cont.append((x, y, (pb*self.mb + self.cb)*fac))
        rl.sendactivity("contours", contours=[cont], materialnumber=3)
        
    # make the wind rose by deriving time spent in each window by turning in degrees
    # but this really should be splined to properly smooth the result
    def velocitydrift(self):
        rl = self
        cont = [(pl["t"]/60000, pl["d"]/100, pl["v"]/100/3.6)  for pl in rl.plines  if pl["C"] == "V"]
        degs = [0]*360
        sdegs = [0]*360
        dp = None
        for t, dn, v in cont:
            if dp is None:
                dp = dn
                continue
            if dp > dn + 180:
                d0, d1 = min(dp, dn+360), max(dp, dn+360)
            elif dn > dp + 180:
                d0, d1 = min(dp+360, dn), max(dp+360, dn)
            else:
                d0, d1 = min(dp, dn), max(dp, dn)
            #assert 0.0 <= d1 - d0 < 45, (t, d0, d1, v)
            ols = 0
            for d in range(int(d0), int(d1)+1):
                if d1 != d0:
                    ol = (min(d+1, d1) - max(d, d0)) / (d1 - d0)
                else:
                    ol = 1.0
                degs[d % 360] += ol
                ols += ol
                sdegs[d % 360] += ol*v
            assert abs(ols - 1) < 0.1, ols
            dp = dn
        rl.sendactivity("contours", contours=[[(d/10, s*10)  for d, s in enumerate(degs)]], materialnumber=0)
        rl.sendactivity("contours", contours=[[(d/10, s/10)  for d, s in enumerate(sdegs)]], materialnumber=2)

        rl.sendactivity("contours", contours=[[(d/10, sdegs[d]/s)  for d, s in enumerate(degs)]], materialnumber=3)
        rl.sendactivity("contours", contours=[[(sdegs[d]/s*math.sin(math.radians(d)), sdegs[d]/s*math.cos(math.radians(d)))  for d, s in enumerate(degs)]], materialnumber=3)

        cdiag = [(sdegs[d]/s*math.sin(math.radians(d)), sdegs[d]/s*math.cos(math.radians(d)))  for d, s in enumerate(degs)]
        rl.sendactivity("contours", contours=[cdiag], materialnumber=3)

        # error function
        def fun(x):
            cx, cy, r = x
            return sum((math.sqrt((c[0] - cx)**2 + (c[1] - cy)**2) - r)**2  for c in cdiag)

        x0 = (0,0,1)
        print(fun((0,0,1)), fun(x0))
        bnds = None#((-500,500), (0.001,0.05), (-500,500), (0.001,0.05), (-500,500), (0.001,0.05))
        g = scipy.optimize.minimize(fun, x0, method=None, bounds=bnds, options={"maxiter":500}) 
        print(g)
        print(x0)
        print(list(g.x))
        rl.wcx, rl.wcy, rl.was = list(g.x)  # was = average glider air speed
        rl.sendactivity("contours", contours=[[(rl.was*math.sin(math.radians(d))+rl.wcx, rl.was*math.cos(math.radians(d))+rl.wcy)  for d in range(361)]], materialnumber=3)


        # now can we plot the locations of the GPS offset by this value per second
        Rpl = rl.Cpl["R"]
        Qpl = rl.Cpl["Q"]
        latdivisorE100, latdivisorN100 = Rpl["e"], Rpl["n"] 
        lngdivisorE100, lngdivisorN100 = Rpl["f"], Rpl["o"]

        fac = 0.1
        cont = [ ]
        self.tposdedrift = [ ]
        i = 0
        for pl in rl.plines:
            if pl["C"] == 'Q':
                while i < len(rl.rcontB) - 1 and rl.rcontB[i][0] < pl["t"]/1000:
                    i += 1
                rx = 100*(pl["x"]-Qpl["x"])
                ry = 100*(pl["y"]-Qpl["y"])
                me, mn = ry/latdivisorE100+rx/lngdivisorE100, ry/latdivisorN100+rx/lngdivisorN100
                t = pl["t"]/1000
                x, y = (me-t*rl.wcx), (mn-t*rl.wcy)
                pb = self.rcontB[i][1]
                p = (x, y, (pb*self.mb + self.cb))
                self.tposdedrift.append((t, p))
                cont.append((x*fac, y*fac, (pb*self.mb + self.cb)*fac))
                #cont.append((x, y, (pl["a"]-0*Qpl["a"])*0.1*fac))
        rl.sendactivity("contours", contours=[cont], materialnumber=0)



        
def convolveexp2(cont, fac):
    rcont = [ ]
    maxtd = 0
    td0, td1 = 0, 0
    for i in range(len(cont)):
        t = cont[i][0]
        sv = cont[i][1]
        sm = 1.0
        
        j0 = 1
        while j0 <= i:
            td0 = t - cont[i-j0][0]
            m = math.exp(-fac*td0*td0)
            sm += m
            sv += m*cont[i-j0][1]
            if m < 0.01:
                break
            j0 += 1
        j1 = 1
        while j1 < len(cont) - i:
            td1 = cont[i+j1][0] - t
            m = math.exp(-fac*td1*td1)
            sm += m
            sv += m*cont[i+j1][1]
            if m < 0.01:
                break
            j1 += 1
        maxtd = max(maxtd, td0, td1)
        rcont.append((t, sv/sm))
    print("maxtd", maxtd)
    return rcont
    
    
nexpcalls = 0
def tconvolveexp2(cont, fac, tfac=1):
    rcont = [ ]
    maxtd = 0
    td0, td1 = 0, 0
    expmap = { 0:1 }
    global nexpcalls
    nexpcalls = 0
    def mexp(td):
        global nexpcalls
        nexpcalls += 1
        if td not in expmap:
            expmap[td] = math.exp(-fac*((td*0.001)**2))
        return expmap[td]
    for i in range(len(cont)):
        t = cont[i][0]
        sv = cont[i][1]
        sm = 1.0
        j0 = 1
        while j0 <= i:
            td0 = t - cont[i-j0][0]
            m = mexp(td0)
            sm += m
            sv += m*cont[i-j0][1]
            if m < 0.01:
                break
            j0 += 1
            maxtd = max(maxtd, td0)
        j1 = 1
        while j1 < len(cont) - i:
            td1 = cont[i+j1][0] - t
            m = mexp(td1)
            sm += m
            sv += m*cont[i+j1][1]
            if m < 0.01:
                break
            j1 += 1
            maxtd = max(maxtd, td1)
        rcont.append((t*tfac, sv/sm))
    print("maxtd", maxtd*0.001, len(expmap), nexpcalls)
    #print(expmap)
    return rcont
    

nexpcalls = 0
def tconvolveexpS(cont, fac):
    rcont = [ ]
    maxtd = 0
    td0, td1 = 0, 0
    expmap = { 0:1 }
    global nexpcalls
    nexpcalls = 0
    def mexp(td):
        global nexpcalls
        nexpcalls += 1
        if td not in expmap:
            expmap[td] = math.exp(-fac*((td)**2))
        return expmap[td]
    for i in range(len(cont)):
        t = cont[i][0]
        sv = cont[i][1]
        sm = 1.0
        j0 = 1
        while j0 <= i:
            td0 = t - cont[i-j0][0]
            m = mexp(td0)
            sm += m
            sv += m*cont[i-j0][1]
            if m < 0.01:
                break
            j0 += 1
            maxtd = max(maxtd, td0)
        j1 = 1
        while j1 < len(cont) - i:
            td1 = cont[i+j1][0] - t
            m = mexp(td1)
            sm += m
            sv += m*cont[i+j1][1]
            if m < 0.01:
                break
            j1 += 1
            maxtd = max(maxtd, td1)
        rcont.append((t, sv/sm))
    print("maxtd", maxtd, len(expmap), nexpcalls)
    #print(expmap)
    return rcont
    
