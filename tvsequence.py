
blinregress = False

def Vinterp(itm, it, iv):
    assert ((itm >= 0) and (itm <= it))
    if (it == 0):
        return iv 
    return itm * iv / it

def GoutTV(t, v, typ): 
    print("OutTV: ", t, v)


class TVSequence:
    def __init__(self, ivlintol, tvarrsize=20):
        self.ivlintol = ivlintol
        self.tvarrsize = tvarrsize
        self.tvarr = [0]*(tvarrsize*2)
        self.ntvoutput = 0
        self.Dntvoutput = 0
        self.ntvsegment = 0; 
        
    def FirstTV(self, t, v, lntv, typ): 
        GoutTV(t, v, typ) 
        self.Dntvoutput += 1 
        self.t0 = t; 
        self.v0 = v; 
        self.ntvupper = 0; 
        self.ntvlower = 0; 
        self.ntvtotal = lntv; 
        self.ntvsegment = 1; 
        
        if blinregress:
            self.st2 = 0
            self.stv = 0

    
    def DeleteBackToConvexityTVabove(self, it, iv):
        bres = False 
        while self.ntvupper > 0:
            im1 = self.ntvupper - 1; 
            itp = 0; 
            ivp = 0; 
            if self.ntvupper != 1:
                im2 = self.ntvupper - 2; 
                itp = self.tvarr[im2*2]; 
                ivp = self.tvarr[im2*2 + 1]; 
            ivm = ivp + Vinterp(self.tvarr[im1*2] - itp, it - itp, iv - ivp) 
            if ivm < self.tvarr[im1*2 + 1]:
                break 
            self.ntvupper -= 1 
            bres = True; 
        return bres; 

    def DeleteBackToConvexityTVbelow(self, it, iv):
        bres = False; 
        while self.ntvlower > 0:
            im1 = self.tvarrsize - self.ntvlower; 
            itp = 0; 
            ivp = 0; 
            if self.ntvlower != 1:
                im2 = self.tvarrsize - self.ntvlower + 1; 
                itp = self.tvarr[im2*2]; 
                ivp = self.tvarr[im2*2 + 1]; 
            ivm = ivp + Vinterp(self.tvarr[im1*2] - itp, it - itp, iv - ivp); 
            if self.tvarr[im1*2 + 1] < ivm:
                break
            self.ntvlower -= 1 
            bres = True; 
        return bres 

    def ConsolidateTVabove(self, it, iv, m):
        self.ntvlower += 1 
        assert self.ntvlower + self.ntvupper <= self.tvarrsize 
        self.tvarr[(self.tvarrsize - self.ntvlower)*2] = self.itn; 
        self.tvarr[(self.tvarrsize - self.ntvlower)*2 + 1] = self.ivn; 
        if not self.DeleteBackToConvexityTVbelow(it, iv):
            i = self.tvarrsize - self.ntvlower
            while i < self.tvarrsize:
                if blinregress:
                    ivdiff = self.tvarr[i*2]*m - self.tvarr[i*2 + 1] 
                else:
                    ivdiff = Vinterp(self.tvarr[i*2], it, iv) - self.tvarr[i*2 + 1] 
                assert ivdiff >= 0
                if ivdiff > self.ivlintol:
                    return False; 
                i += 1
        self.DeleteBackToConvexityTVabove(it, iv); 
        return True; 


    def ConsolidateTVbelow(self, it, iv, m):
        self.ntvupper += 1
        assert self.ntvlower + self.ntvupper <= self.tvarrsize 
        self.tvarr[(self.ntvupper - 1)*2] = self.itn 
        self.tvarr[(self.ntvupper - 1)*2 + 1] = self.ivn 
        
        if not self.DeleteBackToConvexityTVabove(it, iv):
            i = self.ntvupper - 1
            while i >= 0:
                if blinregress:
                    ivdiff = self.tvarr[i*2 + 1] - self.tvarr[i*2]*m
                else:
                    ivdiff = self.tvarr[i*2 + 1] - Vinterp(self.tvarr[i*2], it, iv); 
                assert ivdiff >= 0 
                if ivdiff > self.ivlintol:
                    return False; 
                i -= 1
        self.DeleteBackToConvexityTVbelow(it, iv); 
        return True; 
        
    def AddTV(self, t, v):
        self.ntvtotal += 1 
        assert self.ntvsegment != 0 
        fit = t - self.t0 
        if ((fit >= 32767) and (self.ntvsegment != 1)):
            return False; 
        
        it = fit # (int)fit; // should be unsigned
        iv = v - self.v0 # (int)floor((v - v0) * vfac + 0.5); 
        if self.ntvsegment == 1:
            self.tn = t 
            self.vn = v 
            self.itn = it 
            self.ivn = iv  
            self.ntvsegment = 2 
            if blinregress:
                self.st2 += it*it
                self.stv += it*iv
            return True 
        assert t >= self.tn
        
        self.ntvsegment += 1
        if self.ivlintol == -1:
            return False;      # all points 
        if self.ivlintol == 0:
            if iv != self.ivn or iv != 0:
                return False;  # just where there is a change in v
            self.tn = t 
            self.vn = v 
            return True 
        
        if self.ntvlower + self.ntvupper == self.tvarrsize:
            #DumpState(); 
            return False # memory overflow
        
        if blinregress:
            m = (self.stv + it*iv) / (self.st2 + it*it)
            livn = m * self.itn
        else:
            livn = Vinterp(self.itn, it, iv) 
            m = 0
        
        if livn >= self.ivn:
            bcontinuesequence = self.ConsolidateTVabove(it, iv, m) 
        else:
            bcontinuesequence = self.ConsolidateTVbelow(it, iv, m) 
        if not bcontinuesequence:
            return False
            
        self.tn = t 
        self.vn = v 
        self.itn = it 
        self.ivn = iv 
        
        if blinregress:
            self.st2 += it*it
            self.stv += it*iv

        assert self.ntvlower + self.ntvupper <= self.ntvsegment - 2
        return True; 
        

    def GAddTV(self, t, v):
        if self.ntvsegment == 0:
            return self.FirstTV(t, v, 1, 'f') 
        if not self.AddTV(t, v): 
            if blinregress:
                m = self.stv / self.st2
                self.FirstTV(self.tn, self.v0 + m*self.it, self.ntvtotal - 1, 'm')
            else:
                self.FirstTV(self.tn, self.vn, self.ntvtotal - 1, 'm')
            self.AddTV(t, v)  

    def GFinish(self):
        self.FirstTV(self.tn, self.vn, self.ntvtotal, 'e')
        self.ntvsegment = 0 
     
        


tol = 0.09
contf = [ ]
def GoutTV(t, v, typ): 
    contf.append((t, v))
tvseq = TVSequence(tol) 
for t, v in cont:
    tvseq.GAddTV(t, v) 
tvseq.GFinish(); 
print(len(cont), len(contf))
sendactivity("clearalltriangles")
sendactivity("clearallcontours")
sendactivity("contours", contours=[cont], materialnumber=0)
sendactivity("contours", contours=[contf], materialnumber=3)

tm = cont[-1][0]
sendactivity("contours", contours=[[(0,c),(tm,c)]  for c in range(21)], materialnumber=1)
sendactivity("contours", contours=[[(0,c),(tm,c)]  for c in range(0,21,5)], materialnumber=3)
sendactivity("contours", contours=[[(t*0.001/60*0.2,0),(t*0.001/60*0.2,20)]  for t in range(0, int(mxt), 1000*3600)], materialnumber=1)


