import re, time, math, sys, imp, scipy.optimize
sys.path.append("/home/goatchurch/datalogging/arduinosketchbook/pyscripts")
import parselog
imp.reload(parselog)

rl = parselog.RL("/home/goatchurch/datalogging/templogs/231.TXT", sendactivity)
rl.TrimStationarySections(5)

sendactivity("clearalltriangles")
sendactivity("clearallcontours")
sendactivity("clearallpoints")
rl.altibaromc()
rl.velocitydrift()


# what is speed on the gps?

Rpl = rl.Cpl["R"]
Qpl = rl.Cpl["Q"]
latdivisorE100, latdivisorN100 = Rpl["e"], Rpl["n"] 
lngdivisorE100, lngdivisorN100 = Rpl["f"], Rpl["o"]

fac = 0.1
avcont, vcont, awcont = [ ], [ ], [ ]
i = 0
pme, pmn = None, None
for pl in rl.plines:
    if pl["C"] == 'Q':
        rx = 100*(pl["x"]-Qpl["x"])
        ry = 100*(pl["y"]-Qpl["y"])
        me, mn = ry/latdivisorE100+rx/lngdivisorE100, ry/latdivisorN100+rx/lngdivisorN100
        t = pl["t"]/1000
        if pme is not None:
            avx, avy = (me - pme)/(t - pt), (mn - pmn)/(t - pt)
            avcont.append((t/60, math.sqrt(avx**2 + avy**2)))
            avxw, avyw = avx - rl.wcx, avy - rl.wcy
            awcont.append((t/60, math.sqrt(avxw**2 + avyw**2)))
        pme, pmn, pt = me, mn, t
    if pl["C"] == 'V':
        vcont.append((pl["t"]/60000, pl["v"]/360))
rl.sendactivity("contours", contours=[avcont], materialnumber=0)
rl.sendactivity("contours", contours=[vcont], materialnumber=1)

rl.sendactivity("contours", contours=[awcont], materialnumber=3)
cont = [(pl["t"]/60000, pl["v"]/360)  for pl in rl.plines  if pl["C"] == "V"]
sendactivity("contours", contours=[cont], materialnumber=2)

rawcont = tconvolveexpS(awcont, 5000)
rl.sendactivity("contours", contours=[rawcont], materialnumber=0)

cont = [(pl["t"]/60000, pl["v"]/360)  for pl in rl.plines  if pl["C"] == "V"]
rcont = tconvolveexpS(cont, 5000)
rl.sendactivity("contours", contours=[rcont], materialnumber=0)
sendactivity("contours", contours=[cont], materialnumber=2)

len(rawcont)
sendactivity("contours", contours=[[(-100,0), (100,0)], [(0,-100), (0,100)]])


print(sum(v  for t, v in vcont)/len(vcont))
print(sum(v  for t, v in avcont)/len(avcont))
print(rl.was)  # bigger because spent most of the time into the wind
print(math.sqrt(rl.wcx**2 + rl.wcy**2))  # speed of wind


cont = [(pl["t"]/60000, 80000./max(1, pl["w"]))  for pl in rl.plines  if pl["C"] == "W"]

rawcont = tconvolveexpS(awcont, 10)
rcont = tconvolveexpS(cont, 10)
sendactivity("contours", contours=[rcont], materialnumber=2)

i = 0
pts = [ ]
for t, w in rcont:
    while i < len(rawcont) and rawcont[i][0] < t:
        i += 1
    if i == len(rawcont):
        break
    pts.append((w, rawcont[i][1]))
sendactivity("clearallpoints")
sendactivity("points", points=pts)

    
    
    


