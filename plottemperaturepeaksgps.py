import re, time, math, sys, imp
sys.path.append("/home/goatchurch/datalogging/arduinosketchbook/pyscripts")
import parselog
imp.reload(parselog)

rl = parselog.RL("/home/goatchurch/datalogging/templogs/231.TXT", sendactivity)

#rl.TrimStationarySections(5)
sendactivity("clearalltriangles")



sendactivity("clearallpoints")
sendactivity("clearallcontours")
sendactivity("contours", contours=[[(0,i), (1000,i)]  for i in range(0, 21)], materialnumber=1)
sendactivity("contours", contours=[[(0,i), (1000,i)]  for i in range(0, 21, 5)], materialnumber=0)
sendactivity("contours", contours=[[(i,0), (i,20)]  for i in range(0, 501)], materialnumber=1)



# dallas temperature device
dcont = [(pl["t"]/60000, pl["c"]/16)  for pl in rl.plines  if pl["C"] == "D" and pl["i"] == 1]
sendactivity("contours", contours=[dcont], materialnumber=3)

# find the peaks in that temperature
du = 8
dcontupper = [dcont[j]  for j in range(du, len(dcont) - du)  if dcont[j][1] >= max(dcont[j-du][1], dcont[j+du][1]) + 0.01]
dcontlower = [dcont[j]  for j in range(du, len(dcont) - du)  if dcont[j][1] <= min(dcont[j-du][1], dcont[j+du][1]) - 0.01]
sendactivity("points", points=dcontupper, materialnumber=0)
sendactivity("points", points=dcontupper, materialnumber=1)
print(len(dcont), len(dcontupper), len(dcontlower))


# turn those peaks into intervals
def tptintervals(tvals):
    tsegs = [(t-0.05, -1)  for t in tvals] + [(t+0.05, 1)  for t in tvals]
    tsegs.sort()
    tsegsr = [ ]
    tsegk = 0
    for t, k in tsegs:
        if tsegk == 0 and k == -1:
            tsegsr.append(t)
        elif tsegk == -1 and k == 1:
            tsegsr.append(t)
        tsegk += k
    assert tsegk == 0
    return tsegsr

tsegsrupper = tptintervals([t  for t, v in dcontupper])
tsegsrlower = tptintervals([t  for t, v in dcontlower])

print(len(dcontupper), len(tsegsrupper), len(dcontlower), len(tsegsrlower))
sendactivity("contours", contours=[[(tsegsrupper[j-1], 4.6), (tsegsrupper[j], 4.6)]  for j in range(1, len(tsegsrupper), 2)])
sendactivity("contours", contours=[[(tsegsrlower[j-1], 4.7), (tsegsrlower[j], 4.7)]  for j in range(1, len(tsegsrlower), 2)], materialnumber=1)


# get the timed GPS values
gcont = [ ]
fac = 0.1
Rpl = rl.Cpl["R"]
Qpl = rl.Cpl["Q"]
latdivisorE100, latdivisorN100 = Rpl["e"], Rpl["n"] 
lngdivisorE100, lngdivisorN100 = Rpl["f"], Rpl["o"]
for pl in rl.plines:
    if pl["C"] == 'Q':
        rx = 100*(pl["x"]-Qpl["x"])
        ry = 100*(pl["y"]-Qpl["y"])
        me, mn = ry/latdivisorE100+rx/lngdivisorE100, ry/latdivisorN100+rx/lngdivisorN100
        x, y = me*fac, mn*fac
        gcont.append((pl["t"]/60000, (x, y, (pl["a"]-0*Qpl["a"])*0.1*fac)))


def filtergcont(tsegsr, gcont):
    k = 1
    gcontupper = [ ]
    for t, p in gcont:
        while t > tsegsr[k]:
            k += 2
            if k > len(tsegsr):
                break
        if k > len(tsegsr):
            break
        if t > tsegsr[k-1]:
            gcontupper.append(p)
    print(len(gcont), len(gcontupper))
    return gcontupper
    
sendactivity("points", points=filtergcont(tsegsrupper, gcont))
sendactivity("points", points=filtergcont(tsegsrlower, gcont), materialnumber=1)

sendactivity("contours", contours=[[p  for t, p in gcont]])


def pyclick(p, v):
    px, py = 8.12, 18.49
    px, py = p[0], p[1]
    
    t0 = min(((p[0]-px)**2+(p[1]-py)**2, t)  for t, p in gcont)[1]
    print(t0)

    sendactivity("clearallpoints")
    sendactivity("clearallcontours")
    t1 = t0 + 2
    sendactivity("contours", contours=[[p  for t, p in gcont  if t0 < t < t0 + 2]])

    sendactivity("contours", contours=[[(t, v)  for t, v in dcont  if t0 < t < t1]], materialnumber=0)
    sendactivity("contours", contours=[dcont], materialnumber=3)

    sendactivity("points", points=filtergcont(tsegsrupper))
    sendactivity("points", points=filtergcont(tsegsrlower), materialnumber=1)

    gcontG = [(t, p)  for t, p in gcont  if t0 < t < t1]
    sendactivity("points", points=filtergcont(tsegsrupper, gcontG))
    sendactivity("points", points=filtergcont(tsegsrlower, gcontG), materialnumber=1)


