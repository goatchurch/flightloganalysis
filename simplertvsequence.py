
# needs translating back into C++

def Vinterp(tv0, tM, tv1):
    assert tv0[0] < tM < tv1[0]
    return tv0[1] + (tM - tv0[0]) * (tv1[1] - tv0[1]) / (tv1[0] - tv0[0])

            
class STVSequence:
    def __init__(self, ivlintol):
        self.ivlintol = ivlintol
        self.tv0 = None
        self.tvupper = [ ]
        self.tvlower = [ ]
        self.tvE = None
        self.resseq = [ ]
        
    def AddTV(self, tvN):
        # initial cases
        if self.tv0 is None:
            self.tv0 = tvN
            self.resseq.append(tvN)
            return
        if self.tvE is None:
            self.tvE = tvN
            return
            
        self.tvlower.append(self.tvE)
        self.tvupper.append(self.tvE)
        
        # trim convexity
        while self.tvlower:
            tvM = self.tvlower[-1]
            tvP = (len(self.tvlower) > 1) and self.tvlower[-2] or self.tv0
            if Vinterp(tvP, tvM[0], tvN) > tvM[1]:
                break
            self.tvlower.pop()
            
        while self.tvupper:
            tvM = self.tvupper[-1]
            tvP = (len(self.tvupper) > 1) and self.tvupper[-2] or self.tv0
            if Vinterp(tvP, tvM[0], tvN) < tvM[1]:
                break
            self.tvupper.pop()
            
        # detect tolerance
        tol = 0
        for tvM in self.tvupper:
            vM = Vinterp(self.tv0, tvM[0], tvN)
            assert vM <= tvM[1], (vM, tvN[1])
            tol = max(tol, tvM[1] - vM)
        for tvM in self.tvlower:
            vM = Vinterp(self.tv0, tvM[0], tvN)
            assert vM >= tvM[1]
            tol = max(tol, vM - tvM[1])
            
        if tol > self.ivlintol:
            self.resseq.append(self.tvE)
            self.tv0 = self.tvE
            self.tvlower = [ ]
            self.tvupper = [ ]
        self.tvE = tvN


