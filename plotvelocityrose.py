# Plots and calculates the wind drift and then replots the flight 
# compensating for it

import re, time, math, sys, imp, scipy.optimize
sys.path.append("/home/goatchurch/datalogging/arduinosketchbook/pyscripts")
import parselog
imp.reload(parselog)

rl = parselog.RL("/home/goatchurch/datalogging/templogs/231.TXT", sendactivity)

rl.TrimStationarySections(5)
sendactivity("clearalltriangles")
sendactivity("clearallcontours")
sendactivity("clearallpoints")

sendactivity("contours", contours=[[(-100,0), (100,0)], [(0,-100), (0,100)]])


# make the wind rose by deriving time spent in each window by turning in degrees
# but this really should be splined to properly smooth the result
cont = [(pl["t"]/60000, pl["d"]/100, pl["v"]/100/3.6)  for pl in rl.plines  if pl["C"] == "V"]
degs = [0]*360
sdegs = [0]*360
dp = None
for t, dn, v in cont:
    if dp is None:
        dp = dn
        continue
    if dp > dn + 180:
        d0, d1 = min(dp, dn+360), max(dp, dn+360)
    elif dn > dp + 180:
        d0, d1 = min(dp+360, dn), max(dp+360, dn)
    else:
        d0, d1 = min(dp, dn), max(dp, dn)
    #assert 0.0 <= d1 - d0 < 45, (t, d0, d1, v)
    ols = 0
    for d in range(int(d0), int(d1)+1):
        if d1 != d0:
            ol = (min(d+1, d1) - max(d, d0)) / (d1 - d0)
        else:
            ol = 1.0
        degs[d % 360] += ol
        ols += ol
        sdegs[d % 360] += ol*v
    assert abs(ols - 1) < 0.1, ols
    dp = dn
sendactivity("contours", contours=[[(d/10, s*10)  for d, s in enumerate(degs)]], materialnumber=0)
sendactivity("contours", contours=[[(d/10, s/10)  for d, s in enumerate(sdegs)]], materialnumber=2)

sendactivity("contours", contours=[[(d/10, sdegs[d]/s)  for d, s in enumerate(degs)]], materialnumber=3)

sendactivity("contours", contours=[[(sdegs[d]/s*math.sin(math.radians(d)), sdegs[d]/s*math.cos(math.radians(d)))  for d, s in enumerate(degs)]], materialnumber=3)


cdiag = [(sdegs[d]/s*math.sin(math.radians(d)), sdegs[d]/s*math.cos(math.radians(d)))  for d, s in enumerate(degs)]

sendactivity("contours", contours=[cdiag], materialnumber=3)

# error function
def fun(x):
    cx, cy, r = x
    return sum((math.sqrt((c[0] - cx)**2 + (c[1] - cy)**2) - r)**2  for c in cdiag)

x0 = (0,0,1)
print(fun((0,0,1)), fun(x0))
bnds = None#((-500,500), (0.001,0.05), (-500,500), (0.001,0.05), (-500,500), (0.001,0.05))
g = scipy.optimize.minimize(fun, x0, method=None, bounds=bnds, options={"maxiter":500}) 
print(g)
print(x0)
print(list(g.x))
cx, cy, r = list(g.x)

sendactivity("contours", contours=[[(r*math.sin(math.radians(d))+cx, r*math.cos(math.radians(d))+cy)  for d in range(361)]], materialnumber=3)


# now can we plot the locations of the GPS offset by this value per second
Rpl = rl.Cpl["R"]
Qpl = rl.Cpl["Q"]
latdivisorE100, latdivisorN100 = Rpl["e"], Rpl["n"] 
lngdivisorE100, lngdivisorN100 = Rpl["f"], Rpl["o"]

fac = 0.1
cont = [ ]
for pl in rl.plines:
    if pl["C"] == 'Q':
        rx = 100*(pl["x"]-Qpl["x"])
        ry = 100*(pl["y"]-Qpl["y"])
        me, mn = ry/latdivisorE100+rx/lngdivisorE100, ry/latdivisorN100+rx/lngdivisorN100
        t = pl["t"]/1000
        x, y = (me-t*cx)*fac, (mn-t*cy)*fac
        cont.append((x, y, (pl["a"]-0*Qpl["a"])*0.1*fac))
        cont.append((x, y, (pl["a"]-0*Qpl["a"])*0.1*fac))
sendactivity("contours", contours=[cont], materialnumber=0)

# 1) Work out the speed correction and how well it correlates with the GPS samples
# 2) Compare with wind speed and plot at different strengths
# 3) Work out the compass calibration and relate to flight direction somehow
# 4) Plot deviations from mean temperature at each altitude and in time relative to going up, down and height
# 5) Package these functions up into the parselog code

# New I2C code
