import math
from flightlog.utils import TimeSampleRate

# Dplslist - Dallas temperature readings per sensor on the string, c=degC   Approx 750ms reading cycle phased per sensor
# Hpls - Humidity sensors, c=degC, h=humid%   Alternates humidity and temp readings at 100ms each, cycle is 250ms to include reading time
# Ipls - Infrared device, c=degC, i=IR-degC   Set to a 200ms sample rate
# Npls - Auxiliary barometer temperature readings

# from HTU21D data sheet
def SaturationVapourPressure(tempC):
    A, B, C = 8.1332, 1762.39, 235.66
    return 10**(A - B/(tempC + C))*133.322387415
    
def DewpointTemp(tempC, humid):
    A, B, C = 8.1332, 1762.39, 235.66
    svp = 10**(A - B/(tempC + C))*133.322387415
    pvp = svp*humid/100
    return -C - B/(math.log10(pvp/133.322387415) - A)

# http://en.wikipedia.org/wiki/Density_of_air
def CalcAirDensity(absolutepressure, tempC, relativehumidity100):
    saturationvapourpressure = SaturationVapourPressure(tempC)
    PPwatervapour = saturationvapourpressure*relativehumidity100/100
    PPdryair = absolutepressure - PPwatervapour
    Rdryair = 287.058
    Rwatervapour = 461.495
    tempK = tempC + 273.15
    return PPdryair/(Rdryair*tempK) + PPwatervapour/(Rwatervapour*tempK)

# Todo: analyse the noise and correlation between all these readings in different time sections to understand the agreement and delay factors
class AllTemperatures:
    def __init__(self, plines):
        dallasN = max((pl["i"]  for pl in plines  if pl["C"] == 'D'), default=-1)
        def dallasscale16(pl):
            if "r" not in pl:  pl["c"] = pl["c"]/16;  return pl # quick hack for older code
            pl["c"] = pl["r"]/16
            return pl
        self.Dplslist = [ ]
        for i in range(dallasN+1):
            print("\nDallas temp device %d" % i)
            Dpls = [dallasscale16(pl)  for pl in plines  if pl["C"] == 'D' and pl["i"] == i]
            print("    time sample rate %f ms at sd %f ms" % TimeSampleRate(Dpls))
            print("    minC=%f  maxC=%f" % (min(pl["c"]  for pl in Dpls), max(pl["c"]  for pl in Dpls)))
            self.Dplslist.append(Dpls)

        def humidtrans(pl):
            pl["h"] = -6 + (125 * ((pl["r"] & 0xFFFC) / 65536.0))
            pl["c"] = -46.85 + (175.72 * ((pl["a"] & 0xFFFC) / 65536.0)) 
            return pl
        self.Hpls = [ humidtrans(pl)  for pl in plines  if pl["C"] == 'H' ]
        if self.Hpls:
            print("Humidity device", "    time sample rate %f ms at sd %f ms" % TimeSampleRate(self.Hpls), "    minC=%f  maxC=%f" % (min(pl["c"]  for pl in self.Hpls), max(pl["c"]  for pl in self.Hpls)), "    minH=%f  maxH=%f" % (min(pl["h"]  for pl in self.Hpls), max(pl["h"]  for pl in self.Hpls)))
        else:
            print("No Humidity device")

        def irtrans(pl):
            pl["c"] = pl["a"]*0.02 - 273.15
            pl["i"] = pl["r"]*0.02 - 273.15
            return pl
        self.Ipls = [ irtrans(pl)  for pl in plines  if pl["C"] == 'I' ]
        if self.Ipls:
            print("Infrared time sample rate %f ms at sd %f ms" % TimeSampleRate(self.Ipls))
            print("    minC=%f  maxC=%f" % (min(pl["c"]  for pl in self.Ipls), max(pl["c"]  for pl in self.Ipls)))
            print("    minI=%f  maxI=%f" % (min(pl["i"]  for pl in self.Ipls), max(pl["i"]  for pl in self.Ipls)))
            
        self.Lpls = [ pl  for pl in plines  if pl["C"] == 'L' ]
        if self.Lpls:
            print("Light time sample rate %f ms at sd %f ms" % TimeSampleRate(self.Lpls))


    def PlotAll(self, sendactivity, tfac=1/60000):
        refplotsize = 50000
        hplotstep = max(1, int(len(self.Hpls)/refplotsize))
        print("plotstep", hplotstep, "on", len(self.Hpls), "humid points")
        
        sendactivity("contours", contours=[[(pl["t"]*tfac, pl["h"]*0.1)  for pl in self.Hpls[::hplotstep]]], materialnumber=2)
        sendactivity("contours", contours=[[(pl["t"]*tfac, pl["c"])  for pl in self.Hpls[::hplotstep]]])
        sendactivity("contours", contours=[[(pl["t"]*tfac, pl["c"])  for pl in Dpls]  for Dpls in self.Dplslist], materialnumber=1)
        sendactivity("contours", contours=[[(pl["t"]*tfac, pl["c"])  for pl in self.Ipls]])
        sendactivity("contours", contours=[[(pl["t"]*tfac, pl["i"])  for pl in self.Ipls]], materialnumber=2)
        
        lplotstep = max(1, int(len(self.Lpls)/refplotsize))
        print("plotstep", lplotstep, "on", len(self.Lpls), "light points")
        sendactivity("contours", contours=[[(pl["t"]*tfac, (1024-pl["l"])/1024.0)  for pl in self.Lpls[::lplotstep]]], materialnumber=3)

        sendactivity("contours", contours=[[(0,i), (1000,i)]  for i in range(0, 31)], materialnumber=2)
        sendactivity("contours", contours=[[(0,i), (1000,i)]  for i in range(0, 31, 5)], materialnumber=0)
        sendactivity("contours", contours=[[(i*5,0), (i*5,30)]  for i in range(0, 101)], materialnumber=2)
        sendactivity("contours", contours=[[(i*5,-5), (i*5,35)]  for i in range(0, 101, 12)], materialnumber=0)
        print("reds are humidity% and IR tmp, cyan is Dallas, white is baro, humid and IR ambients, yellow is light reversed") 
        return
        
        # print Humidity with the bar lines
        Hpls = self.Hpls
        hlo = min(pl["h"]  for pl in Hpls)
        ihlo = math.floor(hlo/10)*10
        sendactivity("contours", contours=[[(pl["t"]*tfac, pl["h"]-ihlo)  for pl in Hpls]], materialnumber=2)
        jt0, jt1 = math.floor(Hpls[0]["t"]*tfac), math.ceil(Hpls[-1]["t"]*tfac)
        sendactivity("contours", contours=[[(jt0, i*10), (jt1, i*10)]  for i in range(11-int(ihlo/10))], materialnumber=2)
        print("Humidity low line represents %d%%" % ihlo)
        
        
