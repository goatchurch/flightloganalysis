import re, math, os, shutil, time
import scipy.optimize

digpower = [ 1 << 4*i  for i in range(16) ]

def s16(x):  return x - 65536 if x >= 32768 else x
def parseline(line):
    # Wt00001670wFFFFn00000004
    if line[0] == 'W':  
        return {"C":"W", "t":int(line[2:10], 16), "w":int(line[11:15], 16), "n":int(line[16:24], 16) }

    # Zt00002319 xFFFB yFFF4 z000D a016D b0346 cFE9E w242B x3065 yEAE4 z0004 s0000
    # 0123456789 01234 56789 01234 56789 01234 56789 01234 56789 01234 56789 0123456789
    if line[0] == 'Z':
        return {"C":"Z", "t":int(line[2:10], 16), 
                "ax":int(line[11:15], 16), "ay":s16(int(line[16:20], 16)), "az":s16(int(line[21:25], 16)),  # acceleration
                "a":s16(int(line[26:30], 16)), "b":s16(int(line[31:35], 16)), "c":s16(int(line[36:40], 16)), # gravity vector
                "w":s16(int(line[41:45], 16)), "x":s16(int(line[46:50], 16)), "y":s16(int(line[51:55], 16)), "z":s16(int(line[56:60], 16)), # quaternion
                "s":int(line[61:65], 16) } # calibration state

    # Ft0056F8DAp0129C9
    if line[0] == 'F':  
        return {"C":"F", "t":int(line[2:10], 16), "p":int(line[11:17], 16) }
    
    res = { "C":line[0], "t":int(line[2:10], 16) }
    for k, v1, v in re.findall("([a-z]+)(?:(\"[^\"\n]*[\"\n])|([0-9A-F]+))", line[10:]):
        if v and len(v) == 44:
            res[k] = v1  # calibration (which should have quotes)
        elif v:
            assert len(v) < len(digpower), (len(v), v, digpower, res)
            res[k] = int(v, 16) - (re.match("[89A-F]", v[0]) and digpower[len(v)] or 0)  # sign value applied
        else:
            assert v1, [line]
            res[k] = v1[1:-1]  # isodate removing quotes
    if res["C"] == 'O' and 'x' in res:  res["C"] = 'Z'   # fix Orient file (one bad file.  remove when we're done)
    return res


def TimeSampleRate(pls):
    tdsum, tdsqsum, n = 0.0, 0.0, 0
    for ppl, pl in zip(pls, pls[1:]):
        td = pl["t"] - ppl["t"]
        tdsum += td
        tdsqsum += td*td
        n += 1
    if n <= 1:
        return 0, 0
    return tdsum/n, math.sqrt(tdsqsum*n - tdsum*tdsum)/(n-1)

def GetLatestSDfile(dirname="OOG", n=-1):
    mediadir = "/media/goatchurch"
    activedirs = [ os.path.join(mediadir, d)  for d in os.listdir(mediadir)  if os.listdir(os.path.join(mediadir, d)) ]
    fsddir = os.path.join((activedirs and activedirs[0] or mediadir), dirname)
    ftldir = "/home/goatchurch/datalogging/templogs" 
    ld = os.listdir(ftldir)
    ffiletl = max(os.listdir(ftldir))
    if os.path.exists(fsddir):
        if n == -1:
            ffile = max(os.listdir(fsddir))
        else:
            ffile = "%03d.TXT" % n
        fnamesd = os.path.join(fsddir, ffile)
        fnametl = os.path.join(ftldir, ffile)
        if (ffile > ffiletl or n != -1) and os.path.exists(fnamesd):
            print("copying file", fnamesd)
            shutil.copy(fnamesd, fnametl)
        elif ffile != ffiletl:
            print("final file on SD card %s not greater than final file in templogs %s" % (ffile, ffiletl))
    else:
        ffile = ffiletl
    fname = os.path.join(ftldir, ffile)
    print("working with file", fname)    
    return fname

def CopySDfilesMakeDated(dirname="OOH"):
    mediadir = "/media/goatchurch"
    destdir = "/home/goatchurch/datalogging/templogs"
    activedirs = [ os.path.join(mediadir, d)  for d in os.listdir(mediadir)  if os.listdir(os.path.join(mediadir, d)) ]
    fsddir = os.path.join((activedirs and activedirs[0] or mediadir), dirname)
    ld = os.listdir(fsddir)
    ld.sort(reverse=True)
    for fn in ld:
        if re.match("\d\d\d\.TXT", fn):
            fname = os.path.join(fsddir, fn)
            mblank, mdevname, md = None, None, None
            for l in open(fname):
                if not mblank:
                    mblank = re.match("\s*$", l)
                if mblank:
                    if not md:
                        md = re.search('R.*?"([\d\-T:]*?:\d\d)', l)
                        if md:
                            break
                else:
                    if not mdevname:
                        mdevname = re.match("Device number: (\d+)", l)
            if md:
                assert len(md.group(1)) == 16, md.group(1)
                if mdevname:
                    lfnamenew = "%s-device%s.TXT" % (md.group(1), mdevname.group(1))
                else:
                    lfnamenew = "%s.TXT" % (md.group(1))
                print(fn, lfnamenew)
                fnamenew = os.path.join(destdir, lfnamenew)
                if os.path.exists(fnamenew):
                    assert os.path.getsize(fname) == os.path.getsize(fnamenew)
                else:
                    shutil.copy(fname, fnamenew)



def TSampleRateG(ts):
    tprev = None
    tdsum, tdsqsum, n = 0.0, 0.0, 0
    for t in ts:
        if tprev is not None:
            td = t - tprev
            tdsum += td
            tdsqsum += td*td
            n += 1
        tprev = t
    return tdsum/n, math.sqrt(max(0,tdsqsum*n - tdsum*tdsum))/(n-1)

def MeanSD(vs):
    vsum, vsqsum = 0.0, 0.0
    for v in vs:
        vsum += v
        vsqsum += v**2
    return vsum/len(vs), math.sqrt(max(0,vsqsum*len(vs) - vsum**2))/(len(vs)-1)

def expkern(tfac):
    kexp = [ 1 ]
    while kexp[-1] > 0.001:
        kexp.append(math.exp(-(len(kexp)*tfac)**2))
    vfac = 1/(sum(kexp)*2-kexp[0])
    return [ k*vfac  for k in kexp ]
    
def convexp2(tvs, fac):
    tavg, tsd = TSampleRateG(t  for t, v in tvs)
    kexp = expkern(tavg*fac)
    def kerconv(i):
        res = tvs[i][1]*kexp[0]
        jm, jp = 1, 1
        for j in range(1, len(kexp)):
            while i-jm >= 0 and (tvs[i][0] - tvs[i-jm][0]) < (j-0.5)*tavg:
                jm += 1
            if i-jm >= 0:
                res += tvs[i-jm][1]*kexp[j]
            while i+jp < len(tvs) and (tvs[i+jp][0] - tvs[i][0]) < (j-0.5)*tavg:
                jp += 1
            if i+jp < len(tvs):
                res += tvs[i+jp][1]*kexp[j]
        return res
        
    t0 = time.time()
    res = [ ]
    for i in range(len(tvs)):
        if (i % 10) == 0:
            t1 = time.time()
            if t1 - t0 > 10:
                print("%f%% done" % i*100/len(tvs))
                t0 = t1
        res.append((tvs[i][0], kerconv(i)))
    return res

def convexpbiway(tvs, fac):
    tavg, tsd = TSampleRateG(t  for t, v in tvs)
    #kexp = tavg*fac
    t0 = tvs[-1][0] + tavg
    sv = tvs[-1][1]
    rtvs = [ ]
    for t, v in reversed(tvs):
        while t0 - t > tavg*0.5:
            sv = sv*(1-fac) + v*fac
            t0 -= tavg
        rtvs.append((t, sv))
    
    rtvs.reverse()
    t0 = rtvs[0][0] - tavg
    sv = rtvs[0][1]
    res = [ ]
    for t, v in rtvs:
        while t - t0 > tavg*0.5:
            sv = sv*(1-fac) + v*fac
            t0 += tavg
        res.append((t, sv))
        
    return res


# approaches 0 as fac increases to the limit of SD of a one value sequence
def varexp2(tvs, fac):
    tavg, tsd = TSampleRateG(t  for t, v in tvs)
    kexp = expkern(tavg*fac)
    print("kexp", kexp)
    def varconv(i):
        res, ressq = tvs[i][1]*kexp[0], kexp[0]*(tvs[i][1]**2)
        jm, jp = 1, 1
        for j in range(1, len(kexp)):
            while i-jm >= 0 and (tvs[i][0] - tvs[i-jm][0]) < (j-0.5)*tavg:
                jm += 1
            if i-jm >= 0:
                res += tvs[i-jm][1]*kexp[j]
                ressq += kexp[j]*(tvs[i-jm][1]**2)
            while i+jp < len(tvs) and (tvs[i+jp][0] - tvs[i][0]) < (j-0.5)*tavg:
                jp += 1
            if i+jp < len(tvs):
                res += tvs[i+jp][1]*kexp[j]
                ressq += kexp[j]*(tvs[i+jp][1]**2)
        return math.sqrt(max(0, ressq - res**2))
    return [(tvs[i][0], varconv(i))  for i in range(len(tvs))]

# can be test probed with 
#tvs = [ (i*100, not i and 1000 or 0)  for i in range(-20,21) ]
#sendactivity("contours", contours=[[(t*0.001, v*0.001)  for t, v in tvs]])
#sendactivity("contours", contours=[[(t*0.001, v*0.001)  for t, v in convexp2(tvs, 0.003)]], materialnumber=3)

def UnitSpherePlot(cvals, xg, sendactivity, materialnumber):
    def cvalsfun(x):
        xdisp, xfac, ydisp, yfac, zdisp, zfac = x
        return [((c[0] - xdisp)*xfac, (c[1] - ydisp)*yfac, (c[2] - zdisp)*zfac)  for c in cvals]
    if materialnumber == 1:
        sendactivity("contours", contours=[cvalsfun(xg)], materialnumber=2)
    elif materialnumber == 2:
        r = 1  # globe shape
        sendactivity("contours", contours=[[(math.cos(math.radians(th))*math.cos(math.radians(phi))*r, math.sin(math.radians(th))*math.cos(math.radians(phi))*r, math.sin(math.radians(phi))*r)  for th in range(0, 370, 10)]  for phi in range(-90, 100, 10)])
        sendactivity("contours", contours=[[(math.cos(math.radians(th))*math.cos(math.radians(phi))*r, math.sin(math.radians(th))*math.cos(math.radians(phi))*r, math.sin(math.radians(phi))*r)  for phi in range(-90, 100, 10)]  for th in range(0, 370, 10)])

def UnitSphereProj(cvals, sendactivity):
    # initial guess
    xmin, xmax = min(c[0]  for c in cvals), max(c[0]  for c in cvals)
    ymin, ymax = min(c[1]  for c in cvals), max(c[1]  for c in cvals)
    zmin, zmax = min(c[2]  for c in cvals), max(c[2]  for c in cvals)
    x0 = ((xmax + xmin)/2, 2/(xmax - xmin), (ymax + ymin)/2, 2/(ymax - ymin), (zmax + zmin)/2, 2/(zmax - zmin))

    UnitSpherePlot(cvals, x0, sendactivity, 0)
    def errfun(x):
        xdisp, xfac, ydisp, yfac, zdisp, zfac = x
        return sum((math.sqrt(((c[0] - xdisp)*xfac)**2 + ((c[1] - ydisp)*yfac)**2 + ((c[2] - zdisp)*zfac)**2) - 1.0)**2  for c in cvals)

    print("errfuns", errfun((0,1,0,1,0,1)), errfun(x0))
    bnds = ((-xmax,-xmin), (0.1/(xmax-xmin),10/(xmax-xmin)), (-ymax,-ymin), (0.1/(ymax-ymin),10/(ymax-ymin)), (-zmax,-zmin), (0.1/(zmax-zmin),10/(zmax-zmin)))
    print("bnds", bnds)
    #bnds = ((-500,500), (0.001,0.05), (-500,500), (0.001,0.05), (-500,500), (0.001,0.05))
    g = scipy.optimize.minimize(errfun, x0, method=None, bounds=bnds, options={"maxiter":500}) 
    xg = tuple(g.x)
    print(g)
    print("initial guess", x0)
    print("result", xg)

    # plot the new points factored
    print("x0 and x err", errfun(x0), errfun(xg))
    print("    xdisp = %.6f; xfac = %.6f; ydisp = %.6f; yfac = %.6f; zdisp = %.6f; zfac = %.6f;" % xg)
    print("(%.6f, %.6f, %.6f, %.6f, %.6f, %.6f)" % xg)
    UnitSpherePlot(cvals, xg, sendactivity, 2)
    return xg
    
    
# this is used to average a value of a flight within one second time windows (accounting for sample overlaps)
# so the value can be zipped against the a similarly processed value and compared
def TrapeziumAvg(tstart, tstep, tvs, szero=0):
    if type(tvs) == list:
        if len(tvs) <= 2:
            return []
        gtvs = (x  for x in tvs)
    else:
        gtvs = tvs
    res = [ ]
    st0 = tstart
    st1 = tstart + tstep
    lt, lv = tstart, gtvs.__next__()[1]
    s = szero
    for t, v in gtvs:
        while t > st1:
            sl = (v - lv)*(1/(t - lt)) if (t - lt) != 0 else szero
            mv = lv + sl*(st1 - lt)
            s = s + (lv + mv)*(st1 - lt)
            res.append(s*(0.5/tstep))
            st0 = st1
            st1 = st0 + tstep
            lt, lv = st0, mv
            s = szero
        if t >= tstart:
            assert t >= lt
            s = s + (lv + v)*(t - lt)
        lt, lv = t, v
    return res

def SimpleLinearRegression(tvs):
    sumt, sumtsq, sumv, sumtv, n = 0, 0, 0, 0, 0
    for t, v in tvs:
        sumt += t
        sumtsq += t*t
        sumv += v
        sumtv += t*v
        n += 1
    m = (sumtv*n - sumt*sumv) / ((sumtsq*n - sumt*sumt) or 1)
    c = sumv/n - m*sumt/n
    return m, c

def SimpleLinearRegressionR(tvs):
    sumt, sumtsq, sumv, sumvsq, sumtv, n = 0, 0, 0, 0, 0, 0
    for t, v in tvs:
        sumt += t
        sumtsq += t*t
        sumv += v
        sumvsq += v*v
        sumtv += t*v
        n += 1
    m = (sumtv*n - sumt*sumv) / (sumtsq*n - sumt*sumt)
    c = sumv/n - m*sumt/n
    r = (sumtv*n - sumt*sumv) / math.sqrt((sumtsq*n - sumt*sumt)*(sumvsq*n - sumv*sumv))
    return m, c, r

# this one is completely flawed as this is practically the integral between orthogonal functions
# I had wanted to get the phase and step for the GPS values
def StrictTimeSampleRate(ts):
    X0 = (ts[0], TSampleRateG(ts)[0])
    def fun(X):
        t0, tstep = X
        return sum((math.floor((t - t0)/tstep + 0.5)*tstep + t0 - t)**2  for t in ts)/len(ts)
    bnds = ((X0[0]-X0[1], X0[0]+X0[1]), (X0[1]*0.75,X0[1]*1.25))
    res = scipy.optimize.minimize(fun=fun, x0=X0, bounds=bnds)
    print(res)
    return tuple(res.x)
    


# handle some tvsequence work (moved from basicgeo where it's not general enough)
def SliceTV(tvs, t0, t1):
    def binchop(t, i0, i1, bupper):
        while i0 < i1 - 1:
            i = (i0 + i1)//2
            if tvs[i][0] < t:  i0 = i
            else:  i1 = i
        return bupper and i1 or i0
    i0 = binchop(t0, 0, len(tvs) - 1, True)
    i1 = binchop(t1, i0, len(tvs) - 1, False)
    return tvs[i0:i1+1]


def Along(lam, a, b):
    if a == b:
        return a
    return a * (1 - lam) + b * lam

def AlongG(lam, a, b):
    if a == b:
        return a
    vres = a * (1 - lam) + b * lam
    vlen = a.Len() * (1 - lam) + b.Len() * lam
    return vres*(vlen/(vres.Len() or 1))

def ziptvsA(tvs, tvs1, lAlong):
    tvsres = [ ]
    i, j = 0, 1
    while tvs[i][0] < tvs1[j][0]:
        i += 1
    while tvs1[j][0] < tvs[i][0]:
        j += 1
    while i < len(tvs) and j < len(tvs1):
        while tvs1[j][0] < tvs[i][0]:
            j += 1
            if j == len(tvs1):
                return tvsres
        assert tvs1[j-1][0] <= tvs[i][0] <= tvs1[j][0], ("out of range", tvs1[j-1][0], tvs[i][0], tvs1[j][0])
        lam = (tvs[i][0] - tvs1[j-1][0]) / (tvs1[j][0] - tvs1[j-1][0])
        p = lAlong(lam, tvs1[j-1][1], tvs1[j][1])
        tvsres.append(tvs[i]+(p,))
        i += 1
    return tvsres

def ziptvs(tvs, tvs1):
    return ziptvsA(tvs, tvs1, Along)
def ziptvsG(tvs, tvs1):
    return ziptvsA(tvs, tvs1, AlongG)


def differentiatetvs(tvs, dis):
    res = [ ]
    for i in range(len(tvs)):
        i0, i1 = max(0, i-dis), min(len(tvs)-1, i+dis)
        ti0, ti1 = tvs[i0][0], tvs[i1][0]
        vg = (tvs[i1][1] - tvs[i0][1])*(1000/(ti1 - ti0))
        res.append((tvs[i][0], vg))
    return res
trap

def HeadPitchRoll(quat):
    vupright, vbreadth, vout = quat.VecDots()
    degheading = math.degrees(math.atan2(vbreadth.x, vbreadth.y))
    fheading = math.sqrt(vbreadth.x**2 + vbreadth.y**2)
    if degheading < 0:
        degheading += 180
    degpitch = math.degrees(math.atan2(vbreadth.z, fheading))
    tbackfac = -vbreadth.z/fheading
    tbackheading = P3(vbreadth.x*tbackfac, vbreadth.y*tbackfac, fheading)
    degroll = math.degrees(math.asin(P3.Dot(tbackheading, vout)))
    return degheading, degpitch, degroll

    
def tvslzranges(tvs):
    tinsides = [ 0, 0 ]
    for t, v in tvs:
        if v < 0:
            if tinsides[-1] != -1:
                tinsides.append(t)
                tinsides.append(-1)
        else:
            if tinsides[-1] == -1:
                tinsides[-1] = t
    del tinsides[:2]
    return tinsides

def tinsideranges(t, tinsides):
    for i in range(1, len(tinsides), 2):
        if t < tinsides[i]:
            return tinsides[i-1] < t
    return False
