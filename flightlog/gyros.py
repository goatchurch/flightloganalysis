import math, scipy.optimize
from flightlog.utils import TimeSampleRate, convexp2, varexp2, UnitSphereProj
from basicgeo import P3, OctahedronAngle

# this is deprecated.  there are no gyros.  look at orient

class GyroData:
    def __init__(self, plines, sendactivity):
        self.Gpls = [ pl  for pl in plines  if pl["C"] == "G" ]
        print("\nGyros:")
        print("    time sample rate %f ms at sd %f ms" % TimeSampleRate(self.Gpls))
        
        # known calibration
        self.gx = (56.553626, 0.000060, -429.343377, 0.000061, -2214.837906, 0.000060)
        self.gx = (774.17761422719832, 6.6709083160184496e-05, 376.91726977798771, 6.7806111275396281e-05, -2428.8392467558497, 6.4153593391878935e-05)
        
    def PlotComponents(self):
        return [ [(pl["t"]*0.001, pl[c]*0.001)  for pl in self.Gpls ]  for c in ["x", "y", "z"] ]
    def PlotFilteredComponents(self):
        res = [ ]
        for c in ["x", "y", "z"]:
            tvs = [(pl["t"], pl[c])  for pl in self.Gpls ]
            res.append([(t*0.001, v*0.001)  for t, v in convexp2(tvs, 0.003)])
        return res

    def ExtractStationaryPoints(self, sendactivity, fcutoff=500):
        fac = 0.002
        varC = [ varexp2([ (pl["t"], pl[c])  for pl in self.Gpls ], fac)  for c in ["x", "y", "z"] ]
        varL = [ (v0[0], P3(v0[1], v1[1], v2[1]).Len())  for v0, v1, v2 in zip(*varC) ]
        iminimas = [ ]
        iruncut = [ ]
        for i in range(len(varL)):
            if varL[i][1] < fcutoff:
                iruncut.append((varL[i][1], i))
            elif iruncut:
                iminimas.append(min(iruncut)[1])
                iruncut = [ ]
        print(iminimas)
        sendactivity("contours", contours=self.PlotComponents(), materialnumber=1)
        #sendactivity("contours", contours=rl.gyrodata.PlotFilteredComponents(), materialnumber=3)
        sendactivity("contours", contours=[[(0,y),(100,y)]  for y in range(-5,6)])

        cont = [(t*0.001, v*0.001)  for t, v in varL ]
        sendactivity("contours", contours=[cont])

        # get the smooth static points (do all simpler than just the ones we need)
        smoothC = [ convexp2([ (pl["t"], pl[c])  for pl in self.Gpls ], fac)  for c in ["x", "y", "z"] ]
        self.statpoints = [ P3(smoothC[0][i][1], smoothC[1][i][1], smoothC[2][i][1])  for i in iminimas ]
        print(self.statpoints)
        print(len(self.statpoints), [p.Len()  for p in self.statpoints])
        self.gx = UnitSphereProj(self.statpoints, sendactivity)
        # xdisp = 360.678443; xfac = 0.000062; ydisp = 770.463164; yfac = 0.000061; zdisp = -2458.557360; zfac = 0.000059;
        # xdisp = -170.653976; xfac = 0.000061; ydisp = -170.258617; yfac = 0.000061; zdisp = -2384.831234; zfac = 0.000060;
        # xdisp = 56.553626; xfac = 0.000060; ydisp = -429.343377; yfac = 0.000061; zdisp = -2214.837906; zfac = 0.000060;

    # find fair distribution of slowest in motion across sample in distributed projective plane
    def ExtractSlowMotionPoints(self, sendactivity, oaifac=15):
        fac = 0.002
        smoothC = [ convexp2([ (pl["t"], pl[c])  for pl in self.Gpls ], fac)  for c in ["x", "y", "z"] ]
        varC = [ varexp2([ (pl["t"], pl[c])  for pl in self.Gpls ], fac)  for c in ["x", "y", "z"] ]
        varL = [ (v0[0], P3(v0[1], v1[1], v2[1]).Len())  for v0, v1, v2 in zip(*varC) ]
        assert len(smoothC[0]) == len(varL)
        octominsamps = { } # octoangle => min(varL, statpoint)
        for i in range(len(varL)):
            spt = P3(smoothC[0][i][1], smoothC[1][i][1], smoothC[2][i][1])
            oa = OctahedronAngle(*spt)
            oai = (int(oa[0]*oaifac), int(oa[1]*oaifac))
            if oai not in octominsamps or varL[i][1] < octominsamps[oai][0]:
                octominsamps[oai] = (varL[i][1], spt)
        self.statpoints = [spt  for v, spt in octominsamps.values()]
        print(len(self.statpoints))

        sendactivity("clearallpoints")
        xdisp, xfac, ydisp, yfac, zdisp, zfac = self.gx
        cont = [ ((x - xdisp)*xfac, (y - ydisp)*yfac, (z - zdisp)*zfac)  for x, y, z in self.statpoints ]
        sendactivity("points", points=cont)
        self.gx = UnitSphereProj(self.statpoints, sendactivity)

    def PlotVarL(self, sendactivity, fac=0.002):
        varC = [ varexp2([ (pl["t"], pl[c])  for pl in self.Gpls ], fac)  for c in ["x", "y", "z"] ]
        varL = [ (v0[0], P3(v0[1], v1[1], v2[1]).Len())  for v0, v1, v2 in zip(*varC) ]
        cont = [(t*0.001, v*0.001)  for t, v in varL ]
        sendactivity("contours", contours=[cont])

    def PlotGyros(self, sendactivity):
        conts = [ [(pl["t"]*0.001, pl[c]*0.001)  for pl in self.Gpls ]  for c in ["a", "b", "c"] ]
        sendactivity("contours", contours=conts, materialnumber=2)

    def PlotGforce(self, sendactivity, fac=0.002):
        xdisp, xfac, ydisp, yfac, zdisp, zfac = self.gx
        smoothC = [ convexp2([ (pl["t"], pl[c])  for pl in self.Gpls ], fac)  for c in ["x", "y", "z"] ]
        print(smoothC)
        smoothL = [ (c0[0], P3((c0[1] - xdisp)*xfac, (c1[1] - ydisp)*yfac, (c2[1] - zdisp)*zfac).Len())  for c0, c1, c2 in zip(*smoothC) ]
        cont = [(t*0.001, v)  for t, v in smoothL ]
        sendactivity("contours", contours=[cont], materialnumber=3)
        sendactivity("contours", contours=[[(0,y),(100,y)]  for y in range(0,3)])

    def GetTPath(self):
        xdisp, xfac, ydisp, yfac, zdisp, zfac = self.gx
        return [ (pl["t"], P3((pl["x"] - xdisp)*xfac, (pl["y"] - ydisp)*yfac, (pl["z"] - zdisp)*zfac))  for pl in self.Gpls ]


#sendactivity("contours", contours=rl.gyrodata.PlotComponents(), materialnumber=1)
#sendactivity("contours", contours=rl.gyrodata.PlotFilteredComponents(), materialnumber=3)

# next find windows of stability.  filter on sum of variance calculation in the window to score constantness.  
# then scan for periods of maximal constancy in all three values (combined)
# then get those positions where we are maximally constant and above a threshold 
# and plot them in a compass sphere to see if we can get something to calibrate against

