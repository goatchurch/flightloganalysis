import math, scipy.optimize
from flightlog.utils import TimeSampleRate, convexp2, varexp2, TrapeziumAvg, SimpleLinearRegression, ziptvs
from flightlog.alltemperatures import CalcAirDensity, SaturationVapourPressure, DewpointTemp
from basicgeo import P3, Quat


class GeoFrame:
    def __init__(self, postrack, baros, alltemps, orientdata, windspeed):
        self.postrack = postrack
        self.baros = baros
        self.alltemps = alltemps
        self.orientdata = orientdata
        self.windspeed = windspeed
        self.flightt0, self.flightt1 = self.postrack.t0, self.postrack.t1
        #if self.postrack.Vplslist:
        #    self.flightt0, self.flightt1 = self.FindSingleStartEndTime()
        #else:
        #    self.flightt0, self.flightt1 = 0, 100000
        self.TrapeziumSequences() 
        self.altfac = 0.1
        
    def FindSingleStartEndTime(self):   # defunct function
        tstart, tstep = self.postrack.Vplslist[0][0]["t"], 10000  # consider avg velocity in 10second windows
        stationarymps = 4.0
        g10s = TrapeziumAvg(tstart, tstep, ((pl["t"], pl["v"]*0.01/3.6)  for pl in self.postrack.Vplslist[0]))
        i0, i1 = 0, len(g10s) - 1
        while i0 < i1 and g10s[i0] < stationarymps and g10s[i0+1] < stationarymps:
            i0 += 1
        while i1 > i0 and g10s[i1] < stationarymps and g10s[i1-1] < stationarymps:
            i1 -= 1
        return tstart + tstep*i0,  tstart + tstep*(i1+1)

    # resequences the various measurements into one-second intervals so they can be aligned
    def TrapeziumSequences(self):
        t0, t1 = self.flightt0, self.flightt1
        tstart, tstep = t0, 1000  # average over 1sec intervals of the flight
        self.tstart, self.tstep = tstart, tstep
        gcont = self.postrack.GetPath3()[0]
        self.Aatrap = TrapeziumAvg(tstart, tstep, [ (t, p[2])  for t, p in gcont  if t0 < t < t1])
        self.Actrap = TrapeziumAvg(tstart, tstep, [ (pl["t"], pl["c"])  for pl in self.alltemps.Hpls  if t0 < pl["t"] < t1 ])
        self.Ahtrap = TrapeziumAvg(tstart, tstep, [ (pl["t"], pl["h"])  for pl in self.alltemps.Hpls  if t0 < pl["t"] < t1 ])
        #self.Aitrap = TrapeziumAvg(tstart, tstep, [ (pl["t"], pl["i"])  for pl in self.alltemps.Hpls  if t0 < pl["t"] < t1 ])
        self.Attrap = TrapeziumAvg(tstart, tstep, [ (pl["t"], pl["c"])  for pl in self.alltemps.Dplslist[-1]  if t0 < pl["t"] < t1 ])
        self.Abtrap = TrapeziumAvg(tstart, tstep, [ (pl["t"], pl["p"])  for pl in self.baros.Bpls  if t0 < pl["t"] < t1 ])
        self.Aftrap = TrapeziumAvg(tstart, tstep, [ (pl["t"], pl["p"])  for pl in self.baros.Fpls  if t0 < pl["t"] < t1 ])
        
        if len(self.baros.Fpls) > len(self.baros.Bpls):
            self.Abtrap = self.Aftrap
            print("replacing Btrap with Ftrap because", len(self.baros.Fpls), "is more points than", len(self.baros.Bpls))
        
        self.Adtrap = [ CalcAirDensity(p, t, h)  for p, t, h in zip(self.Abtrap, self.Actrap, self.Ahtrap) ]
        self.Awtrap = TrapeziumAvg(tstart, tstep, [ (t, w)  for t, w in self.windspeed.wspeeds  if t0<t<t1 ])
        
        self.altmin, self.altstep = min(self.Aatrap, default=0), 1.0   
        self.Aarising = [ ((i > 5) and (self.Aatrap[i-5] < v))  for i, v in enumerate(self.Aatrap) ]
        self.hectsteps = [ i*100  for i in range(int(min(self.Aatrap, default=0)/100), int(max(self.Aatrap, default=1)/100+2)) ]
        
    def GetTrapValueAtT(self, t, Atrap):
        i = int((t - self.tstart)/self.tstep)
        return Atrap[max(0, min(len(Atrap)-1, i))]

    # Find the linear regression of gps altitude against barometer (also in 100m intervals) and compare the slope to the measured air density times g
    def PlotGPSbaroscale(self, sendactivity):
        t0, t1 = self.flightt0, self.flightt1
        gcont = self.postrack.GetPath3()[0]
        ptaltbaro = list(zip(self.Aatrap, self.Abtrap))
        ptaltdens = list(zip(self.Aatrap, self.Adtrap))
        sendactivity("contours", contours=[[(a,(v-88000)*0.01)  for a, v in ptaltbaro]], materialnumber=0)
        
        self.maltbaro, self.caltbaro = SimpleLinearRegression(ptaltbaro)
        sendactivity(contours=[[(0,(self.caltbaro-88000)*0.01), (3000,(self.caltbaro+self.maltbaro*3000-88000)*0.01)]], materialnumber=1)
        g = 9.80665
        print("Linear Interpolation to sea level pressure %fMb with a decrease of %fMb per metre" % (self.caltbaro, -self.maltbaro))
        minalt, maxalt = min(self.Aatrap), max(self.Aatrap)
        print("Minmax alts %.0fm %.0fm  minmax densities %f, %f" % (minalt, maxalt, g*min(self.Adtrap), g*max(self.Adtrap)))
        print("Interpolation every 100m")
        altstep = 100
        denslineA, denslineB = [ ], [ ]
        for aR in range(int(minalt/altstep)+1, int(maxalt/altstep)):
            a0, a1 = aR*altstep, (aR+1)*altstep
            sendactivity(contours=[[(a0,0), (a0,500)], [(a1,0), (a1,500)]])
            Tptaltbaro = [(a, b)  for a, b in ptaltbaro  if a0<=a<a1]
            Tdens = [d  for a, d in ptaltdens  if a0<=a<a1]
            avgdens = sum(Tdens)/len(Tdens)
            m, c = SimpleLinearRegression(Tptaltbaro)
            la0, la1 = a0-500, a1+500
            sendactivity(contours=[[(la0, (la0*m+c-88000)*0.01), (la1+20, (la1*m+c-88000)*0.01)]], materialnumber=3)
            print("alt %dm,%dm slope %f, g*density %f" % (a0, a1, -m, avgdens*g))
            denslineA.append((a0/altstep, (avgdens*g-10)*10))
            denslineB.append((a0/altstep, (-m-10)*10))
        sendactivity(contours=[denslineA], materialnumber=2)
        sendactivity(contours=[denslineB], materialnumber=3)


    def PlotGPSbaropath(self, sendactivity):
        t0, t1 = self.flightt0, self.flightt1
        gcont = self.postrack.GetPath3()[0]
        ptaltbaro = list(zip(self.Aatrap, self.Abtrap))
        sendactivity("contours", contours=[[(a,(v-88000)*0.01)  for a, v in ptaltbaro]], materialnumber=0)
        ptaltbaro.sort()
        metrealtbaro = TrapeziumAvg(self.altmin, self.altstep, ptaltbaro)
        sendactivity("points", points=[((i-0.5)*self.altstep+self.altmin, v1)  for i, v1 in enumerate(ptaltbaro)], materialnumber=3)
        self.maltbaro, self.caltbaro = SimpleLinearRegression(((i-0.5)*self.altstep+self.altmin, v)  for i, v in enumerate(metrealtbaro))
        print("Sea level pressure %fMb with a decrease of %fMb per metre" % (self.caltbaro, -self.maltbaro))

        # convert the barometer and resample in order to apply to the GPS steps
        Tbatrap = TrapeziumAvg(self.postrack.gpst0, self.postrack.gpststep, [ (pl["t"], (pl["p"]-self.caltbaro)/self.maltbaro)  for pl in self.baros.Bpls  if t0 < pl["t"] < t1 ])
        self.Dgcont = gcont
        def GetAltT(t):
            ti = (t - self.postrack.gpst0)/self.postrack.gpststep
            i = math.floor(ti)
            if i <= 1:  return Tbatrap[0]
            if i >= len(Tbatrap) - 1:  return Tbatrap[-1]
            tf = (ti - i)
            if tf < 0.5:
                return Tbatrap[i-1]*(0.5-tf) + Tbatrap[i]*(tf+0.5)
            else:
                return Tbatrap[i]*(1.5-tf) + Tbatrap[i+1]*(tf-0.5)
        self.DgcontB = [(t, (p[0], p[1], GetAltT(t)))  for t, p in self.Dgcont]
        #sendactivity("contours", contours=[[(p[1]*0.1, p[2]*0.1, p[0]*0.1)  for t, p in self.DgcontB  if t0<t<t1]], materialnumber=3)
        sendactivity("contours", contours=[[(p[0]*0.1, p[1]*0.1, p[2]*0.1)  for t, p in self.DgcontB  if t0<t<t1]], materialnumber=3)

    def IntegratedDensity(self, sendactivity):
        t0, t1 = self.flightt0, self.flightt1
        ptaltdens = list(zip(self.Aatrap, self.Adtrap))
        ptaltdens.sort()
        sendactivity("points", points=[(a*self.altfac,v)  for a,v in ptaltdens], materialnumber=2)
        sendactivity("contours", contours=[[(a*self.altfac,v)  for a,v in ptaltdens]], materialnumber=1)
        metrealtdens = TrapeziumAvg(self.altmin, self.altstep, ptaltdens)
        sendactivity("contours", contours=[[(self.altfac*((i-0.5)*self.altstep+self.altmin), v1)  for i, v1 in enumerate(metrealtdens)]], materialnumber=3)
        g = 9.80665
        maxpressure = max(pl["p"]  for pl in self.baros.Bpls  if t0 < pl["t"] < t1)
        def accumulate(x, l=[maxpressure]): l[0] -= x*g*self.altstep; return l[0];
        accmetrealtdens = map(accumulate, metrealtdens)
        sendactivity("contours", contours=[[(self.altfac*((i-0.5)*self.altstep+self.altmin), (v1-88000)*0.01)  for i, v1 in enumerate(accmetrealtdens)]], materialnumber=3)

    def PlotDewpoint(self, sendactivity, pfac, poff):  # by altitude
        Asvptrap = [ SaturationVapourPressure(t)*h  for t, h in zip(self.Actrap, self.Ahtrap) ]
        sendactivity("points", points=[(v0*self.altfac, (v1-poff)*pfac)  for v0, v1 in zip(self.Aatrap, Asvptrap)], materialnumber=2)
        Adpttrap = [ DewpointTemp(t, h)  for t, h in zip(self.Actrap, self.Ahtrap) ]
        sendactivity("points", points=[(v0*self.altfac, v1)  for v0, v1 in zip(self.Aatrap, Adpttrap)], materialnumber=1)

    def PlotDew(self, sendactivity, tfac):   # by time
        Adpttrap = [ DewpointTemp(t, h)  for t, h in zip(self.Actrap, self.Ahtrap) ]
        sendactivity(contours=[[((self.tstart + self.tstep*i)*tfac, c)  for i, c in enumerate(Adpttrap)]], materialnumber=3)

    def PlotAgainstRising(self, ltrap, sendactivity):
        sendactivity("points", points=[(v0*self.altfac, v1)  for v0, v1, u in zip(self.Aatrap, ltrap, self.Aarising)  if u], materialnumber=0)
        sendactivity("points", points=[(v0*self.altfac, v1)  for v0, v1, u in zip(self.Aatrap, ltrap, self.Aarising)  if not u], materialnumber=1)

    def PlotAltWindspeed(self, sendactivity):
        pts = [(v0*self.altfac, v1)  for v0, v1 in zip(self.Aatrap, self.Awtrap)]
        sendactivity(points=pts)
        lvs = [ ]
        for i in range(int(min(pts)[0]), int(max(pts)[0])+1):
            sps = [ s  for a, s in pts  if int(a) == i ]
            lvs.append((i, sum(sps)/(len(sps) or 1)))
        sendactivity(contours=[lvs])

    def PlotAltRateDifferential(self, sendactivity):
        g = 9.80665
        Aatrap, Abtrap, Adtrap = self.Aatrap, self.Abtrap, self.Adtrap
        n = len(Adtrap)
        dbdapts = [ ]
        Astep = 3
        dfac = 1/(Astep*self.tstep*0.001)
        for i in range(Astep, n, 1):
            db = -(Abtrap[i] - Abtrap[i-Astep])/(g*Adtrap[i-Astep//2])
            da = (Aatrap[i] - Aatrap[i-Astep])
            dbdapts.append((db*dfac, da*dfac))
        sendactivity(points=dbdapts)
        sendactivity(contours=[[(-50,-50),(50,50)]])

        plottoffset = 10
        sendactivity(contours=[[(plottoffset,i), (plottoffset+500,i)]  for i in range(-5,6)], materialnumber=2)
        sendactivity(contours=[[(plottoffset,0), (plottoffset+500,0)]  for i in range(-5,6)], materialnumber=0)
        sendactivity(contours=[[(i*0.1+plottoffset,db)  for i, (db, da) in enumerate(dbdapts)]])
        sendactivity(contours=[[(i*0.1+plottoffset,da)  for i, (db, da) in enumerate(dbdapts)]], materialnumber=1)
        
        # make the differential time shift case for best match
        tstepfac = 1/(self.tstep*0.001)
        dts = [0.05*i  for i in range(-40, 41)]
        plottoffset = -100
        contdtdva = [ ]
        for dt in dts:
            k1 = ziptvs([(i*tstepfac, v0)  for i, (v0, v1) in enumerate(dbdapts)], [(i*tstepfac+dt, v1)  for i, (v0, v1) in enumerate(dbdapts)])
            v = sum(abs(dv - da)  for t, dv, da in k1)/len(k1)
            contdtdva.append((dt+plottoffset, v*10))
            print((dt, v))
        sendactivity(contours=[contdtdva])
        sendactivity(contours=[[(dt+plottoffset,0), (dt+plottoffset,10)]  for dt in [0.1*i  for i in range(-20, 21)]], materialnumber=3)
        sendactivity(contours=[[(plottoffset,0), (plottoffset,10)]])


#delta club 82
