import math, scipy.optimize
from flightlog.utils import TimeSampleRate, convexp2, varexp2, UnitSphereProj, ziptvs
from basicgeo import P3, OctahedronAngle, Quat

# vector from top of control frame to corner of base bar with keel pointing due north (0,1,0)
controlbase = P3(69.0, 42.06344442205491, -156.2492943010685) 

class OrientData:
    def __init__(self, plines, magdeclination, sendactivity):
        magquat = Quat(math.cos(math.radians(-magdeclination/2)),0,0,math.sin(math.radians(-magdeclination/2)))

        # vector from top of control frame to corner of right hand basebar is aligned with vector0 of device on that upright
        uprightdihedral = math.atan2(controlbase.x, -controlbase.z)
        self.keelrot = Quat(math.cos(uprightdihedral/2), 0, -math.sin(uprightdihedral/2), 0)
        uprightrake = math.asin(controlbase.y/controlbase.Len())
        self.keelrake = Quat(math.cos(uprightrake/2), -math.sin(uprightrake/2), 0, 0)

        self.uprightvert = self.keelrake*self.keelrot
        self.uprightvert = self.keelrot  # for now

        # consolidate two logged records in the input into one
        # sdlogger->logacceleration(mstamp, orientdata->linaccx(), orientdata->linaccy(), orientdata->linaccz(), orientdata->gravvecx(), orientdata->gravvecy(), orientdata->gravvecz()); 
        # sdlogger->logorient(mstamp, orientdata->qw(), orientdata->qx(), orientdata->qy(), orientdata->qz(), ((orientdata->prevcalibstat<<8) | orientdata->calibstat)); 
        
        # Y orientation calibrations s"sequencecode" xyz
        self.Ypls = [ pl  for pl in plines  if pl["C"] == "Y" ]

        # S acceleration(xyz) gravity(abc)
        # Z quaternion(wxyz) c-calibstat
        pla = None
        self.Zpls = [ ]
        for pl in plines:
            if pl["C"] == "S":
                pla = pl
            elif pl["C"] == "Z" and pla:
                assert pla["t"] == pl["t"], ("missing S record between", pla, pl)
                pl["s"] = pl["c"]   # calibstat copy to avoid clash
                pl.update(pla)      # merge two records
                self.Zpls.append(pl)
                #linacc = P3(pl["x"]*0.01, pl["y"]*0.01, pl["z"]*0.01)
                #gvec = P3(pla["a"]*0.01, pla["b"]*0.01, pla["c"]*0.01) 
                # leave off gvec, pl["c"]calibstat) in the qvaccs code
                #q = Quat(pl["w"], pl["x"], pl["y"], pl["z"])
                #self.qvaccs.append((pl["t"], q*magquat, linacc))
                pla = None
            elif pl["C"] == "Z":
                assert "a" in pl, "Z orientation not new full record type"
                self.Zpls.append(pl)
                
        self.qvaccs = [ (pl["t"], Quat(pl["w"], pl["x"], pl["y"], pl["z"])*magquat, P3(pl["x"]*0.01, pl["y"]*0.01, pl["z"]*0.01))  for pl in self.Zpls ]
                
        if not self.Zpls:
            print("\nNo Orient")
            return
            
            
        print("\nOrient:")
        print("    time sample rate %f ms at sd %f ms" % TimeSampleRate(self.Zpls))
        print("    applying magnetic declination %f" % magdeclination)


#http://www.sandvik.coromant.com/en-gb/knowledge/milling/application_overview/holes_and_cavities/slicing_methods

    def PlotGravityAgreements(self, tfac, sendactivity):
        sendactivity("clearallcontours")
        for j in range(3):
            cont = [ ]
            cont2 = [ ]
            for t, quat, linacc in self.qvaccs:
                qv = quat.VecDotsT()
                cont2.append((qv[2][0], qv[2][1], t*tfac))
            #sendactivity("contours", contours=[cont])
            sendactivity("contours", contours=[cont2], materialnumber=1)

    def plotcalibrationquality(tfac, sendactivity):
        self.calibconts = [ ]
        for sh in [6, 0, 2, 4]:
            mask = 0x03 << sh
            calibcont = [ ]
            for pl in self.Zpls:
                if pl["s"]&mask != (pl["s"]>>8)&mask:
                    calibcont.append((pl["t"], ((pl["s"]>>8)>>sh)&0x3))
                    calibcont.append((pl["t"], ((pl["s"])>>sh)&0x3))
            self.calibconts.append(calibcont)
            print(sh/2, len(calibcont))

        for i, calibcont in enumerate(self.calibconts):
            sendactivity(contours=[[(t*tfac, c*0.1+0.01+0.01*i)  for t, c in calibcont]], materialnumber=i)


    def printcalibrations(self):
        calibrationlines = [ ]
        for pl in self.Ypls:
            if pl["s"] != 'r':
                calibrationlines.append(" ".join("%s%s = %d;" % ({"a":"accoffset", "m":"magoffset", "g":"gyroffset"}[pl["s"]], c, pl[c])  for c in "xyz"))
            else:
                calibrationlines.append("accradius = %d; magradius = %d;" % (pl["x"], pl["y"]))
        print("%d sets of calibrations found" % (len(calibrationlines)/4))
        print("\n".join(calibrationlines[-4:]))
        
        
        

    def PlotYawOrientation(self, rl, sendactivity):
        t0, t1 = rl.postrack.t0, rl.postrack.t1
        gcont = rl.postrack.GetPath3()[0]
        lqvacc = [x  for x in rl.orientdata.qvaccs  if t0<x[0]<t1]
        k = ziptvs(lqvacc, gcont)
        dis = 9
        cont = [ ]
        ptpos, ptneg = [ ], [ ]
        for i in range(dis, len(k) - dis):
            tdiff = (k[i+dis][0] - k[i-dis][0])*0.001
            v = (k[i+dis][2] - k[i-dis][2])*(1/tdiff)
            vo = k[i][1].VecDots()[1]
            vlz = v.LenLZ()
            volz = v.LenLZ()
            vdvop = vo[0]*v[1] - vo[1]*v[0]
            vdden = (v.LenLZ()*vo.LenLZ())
            if vdden == 0.0:  continue
            cd = vdvop/vdden
            cont.append((k[i][0]*0.001*0.01, cd))
            if cd < -0.3:
                ptneg.append(k[i][2]*rl.gfac)
            if cd > 0.3:
                ptpos.append(k[i][2]*rl.gfac)
        
        sendactivity("clearallcontours")
        sendactivity("contours", contours=[cont])
        sendactivity("contours", contours=[[p*rl.gfac for t, q, p in k]], materialnumber=1)
        sendactivity("clearallpoints")
        sendactivity("points", points=ptpos, materialnumber=1)
        sendactivity("points", points=ptneg, materialnumber=2)

        sendactivity("contours", contours=[[p*rl.gfac, (p+q.VecDots()[1]*2.0)*rl.gfac]  for t, q, p in k[::10]])




# to calculate the controlbase vector of down the upright when the glider keel is pointing horizontal north
"""
import scipy.optimize, math

upright = 176
basebar = 138
backwire = 205  # was 256, but this from previous measurement
backkeel = 79
frontkeel = 159
frontwire = 207

controltop = P3(0,0,0)
frontwirejoin = P3(0,frontkeel,0)
backwirejoin = P3(0,-backkeel,0)

ds = [(controltop, upright), (frontwirejoin, frontwire), (backwirejoin, backwire) ] 
def fun(x):
    controlbase = P3(*x)
    return (controlbase[0]*2 - basebar)**2 + sum(((p - controlbase).Len() - d)**2  for p, d in ds)
def funA(x):
    controlbase = P3(*x)
    return abs(controlbase[0]*2 - basebar) + sum(abs((p - controlbase).Len() - d)  for p, d in ds)

x0 = (69,76,-168)
g = scipy.optimize.minimize(fun, x0, method="Powell") 
g = scipy.optimize.minimize(funA, x0, method="Powell") 
print(g)
controlbase = P3(*g.x)
print(controlbase[0]*2, basebar)
for p, d in ds:
    print((p - controlbase).Len(), d)

# alternative direct calculation
heightcontrolframe = math.sqrt(upright**2 - (basebar/2)**2)
heightfronttriangle = math.sqrt(frontwire**2 - (basebar/2)**2)
heightbacktriangle = math.sqrt(backwire**2 - (basebar/2)**2)
controlframefrontangle = math.degrees(math.acos((frontkeel**2 + heightcontrolframe**2 - heightfronttriangle**2)/(2*frontkeel*heightcontrolframe)))
controlframebackangle = math.degrees(math.acos((backkeel**2 + heightcontrolframe**2 - heightbacktriangle**2)/(2*backkeel*heightcontrolframe)))

print(180, controlframefrontangle+controlframebackangle)  # should add up to 180
print("rakeforward", 90-controlframefrontangle)

math.degrees(math.asin((basebar/2)/upright))  # angle of upright
"""





"""
fname = "/media/goatchurch/261A-1741/OOR/117.TXT"
import sys
sys.path.append("/home/goatchurch/datalogging/flightloganalysis")
from flightparse import RM
rm = RM(fname, sendactivity)

rm.plot("light")
rm.plot("orgz")

sendactivity(contours=[[(t*self.tfac, (g[2]+9.8)*0.01)  for t, g, q in rm.orient]], materialnumber=3)

for t, g, q in rm.orient[:5000:100]:
    print(int(t*0.001), q.VecDots()[0])

sendactivity(contours=[[(t*rm.tfac, q.VecDots()[0][2])  for t, g, q in rm.orient]], materialnumber=3)

tor = [(t, q.VecDots()[i])  for t, a, q in self.orient  if tgs[0][0]<t<tgs[-1][0]]

#Q 0.98 -0.01 -0.21
Q 0.03 0.85 -0.52

P3.ZNorm(P3(0, 40, 71))

import math
q = Quat(1919.00,-2990.00,13910.00,-7894.00)
controlbase = P3(40, 0, -71)
uprightdihedral = math.atan2(controlbase.x, -controlbase.z)
keelrot = Quat(math.cos(uprightdihedral/2), 0, -math.sin(uprightdihedral/2), 0)

(keelrot*q).VecDots()
(q).VecDots()



controlbase = P3(-20, 40, -71)
Q 359.00 -4599.00 13625.00 -7843.00

q = Quat(1919.00,-2990.00,13910.00,-7894.00)
q.VecDots()
dir(q)

uprightdihedral = math.atan2(controlbase.x, -controlbase.z)
keelrot = Quat(math.cos(uprightdihedral/2), 0, -math.sin(uprightdihedral/2), 0)



import math
from basicgeo import Quat, P3
controlbase = P3(69.0, 42.06344442205491, -156.2492943010685) 

uprightdihedral = math.atan2(controlbase.x, -controlbase.z)
keelrot = Quat(math.cos(uprightdihedral/2), 0, -math.sin(uprightdihedral/2), 0)

uprightrake = math.asin(controlbase.y/controlbase.Len())
keelrake = Quat(math.cos(uprightrake/2), -math.sin(uprightrake/2), 0, 0)

uprightvert = keelrake*keelrot
uprightvert = keelrot  # for now


"""

