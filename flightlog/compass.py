import math, scipy.optimize
from flightlog.utils import TimeSampleRate, UnitSphereProj
from basicgeo import P3


class CompassData:
    def __init__(self, plines, sendactivity):
        self.Cpls = [ pl  for pl in plines  if pl["C"] == "C" ]
        print("\nCompass:")
        print("    time sample rate %f ms at sd %f ms" % TimeSampleRate(self.Cpls))
        
        # known calibration
        self.gx = (-34.553189, 0.006519, 10.273309, 0.006225, -33.473186, 0.006718)  # indoor
        self.gx = (-36.782443615989131, 0.0043630849935933661, 7.1049980296470769, 0.0041775140338351068, -40.844057270322466, 0.0047283632776002225) # outside
        # xdisp = -36.311293; xfac = 0.006548; ydisp = 8.383322; yfac = 0.006238; zdisp = -44.620465; zfac = 0.007143;
        # xdisp = -35.463455; xfac = 0.004380; ydisp = 9.556882; yfac = 0.004203; zdisp = -36.400402; zfac = 0.004857;
        print("    flux calibration L=%f  sd=%f" % self.FluxMeasurement())


    def ExtractCalibrationPoints(self, sendactivity):
        rawcompassvals = [ (pl["x"], pl["y"], pl["z"])  for pl in self.Cpls ]
        
        seenfac = 10;   # 5 unit increments
        pmrange = 500;  # the range limits of the values
        pmexp = pmrange*2//seenfac + 2;

        cvals = [ ]
        seenvals = set()
        nvalscanned = 0
        for c in rawcompassvals:
            ix = (c[0] + pmrange)//seenfac
            iy = (c[1] + pmrange)//seenfac
            iz = (c[2] + pmrange)//seenfac
            iv = ix + pmexp*(iy + pmexp*iz)
            nvalscanned += 1
            if iv not in seenvals:
                cvals.append(c)
                seenvals.add(iv)
                
        sendactivity("clearallpoints")
        print("    total points found", nvalscanned, "selected for calibration", len(cvals))
        self.gx = UnitSphereProj(cvals, sendactivity)  # should check if calibration remotely correct


    def FluxMeasurement(self):
        fxsum, fxsqsum, n = 0.0, 0.0, 0
        for t, p in self.GetTPath():
            lp = p.Len()
            fxsum += lp
            fxsqsum += lp*lp
            n += 1
        return fxsum/n, math.sqrt(max(0,fxsqsum*n - fxsum*fxsum))/(n-1)


    def GetTPath(self):
        xdisp, xfac, ydisp, yfac, zdisp, zfac = self.gx
        return [ (pl["t"], P3((pl["x"] - xdisp)*xfac, (pl["y"] - ydisp)*yfac, (pl["z"] - zdisp)*zfac))  for pl in self.Cpls ]

#sendactivity("contours", contours=[[v  for t, v in rl.compassdata.GetTPath()]], materialnumber=3)

# logged calibrations
# 2015-04-19 in bedroom:
#    xdisp = -37.499990; xfac = 0.004366; ydisp = 10.499862; yfac = 0.004164; zdisp = -39.999957; zfac = 0.004707;
#    xdisp = -34.553189; xfac = 0.006519; ydisp = 10.273309; yfac = 0.006225; zdisp = -33.473186; zfac = 0.006718;
