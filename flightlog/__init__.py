import re, time, math
from flightlog.utils import parseline, TimeSampleRate, differentiatetvs
from flightlog.postrack import PosTrack, FilterTempSeqPlotGPS, FilterTempSeqPlotGPSRising, EstimateVelocityChord, IGCfile, cubecoordinates
from flightlog.orient import OrientData
from flightlog.barometers import BaroData
from flightlog.windspeed import WindspeedData
from flightlog.geoframe import GeoFrame
from flightlog.alltemperatures import AllTemperatures
from flightlog.differentials import AccelerationDerivations
from basicgeo import P3, P2

def Dsendactivity(*args, **kwargs):
    pass

class RL:
    def __init__(self, fname, lsendactivity):
        self.gfac = 0.1
        self.devicenumber = -1
        self.Tgpsfilterforpeaks = 0.03
        self.sendactivity = lsendactivity or Dsendactivity
        self.sendactivity("clearalltriangles")
        self.sendactivity("clearallcontours")
        self.magdeclination = -(1+23/60)   # for Spain (Algonodales)
        self.tclicks = [ ]

        fin = open(fname)
        self.g = 9.80665

        while True:
            hline = fin.readline()
            if re.match("\s*$", hline):
                break
            sdev = re.match('Device number: (.)\n', hline)
            if sdev:
                self.devicenumber = int(sdev.group(1))
                print("\n\nDevice number:", self.devicenumber)
                    
        
        parsetime0 = time.time()
        self.plines = [ parseline(line)  for line in fin ]
        print("Lines found %d in time %.2fseconds" % (len(self.plines), time.time()-parsetime0))
        self.tseconds = (self.plines[-1]["t"] - self.plines[0]["t"])/1000
        self.tfac = (self.tseconds < 5000 and 1/5000 or 1/60000)
        print("File duration: %02d:%02d:%02d, plotting tfac=%f" % (int(self.tseconds/3600), int(self.tseconds/60) % 60, int(self.tseconds) % 60, self.tfac))

        # now parse the different categories of data
        self.baros = BaroData(self.plines)
        self.alltemps = AllTemperatures(self.plines)  # includes humidity
        
        self.blemessages = [ pl  for pl in self.plines  if pl["C"] == 'M' and pl["f"] == 'b' ]

        self.postrack = PosTrack(self.plines)
        self.windspeed = WindspeedData(self.plines)
        self.orientdata = OrientData(self.plines, self.magdeclination, lsendactivity)
        self.geoframe = None
        self.accelerationderivations = None
        self.igcfiles = [ ]
        self.avgdrift = None
        
        # check for unaccounted for readings
        self.Cpl = {}
        for pl in self.plines:
            if pl["C"] not in self.Cpl:
                pl["N"] = 0
                self.Cpl[pl["C"]] = pl
            self.Cpl[pl["C"]]["N"] += 1
        print(" ".join("%s(%d)" % (pl["C"], pl["N"])  for pl in self.Cpl.values()))
        
    def loadIGC(self, fname):
        self.igcfiles.append(IGCfile(fname, self.postrack))
        
    def plot(self, cmd=None):
        res = None
        if cmd == "ble":
            for pl in self.blemessages:
                print(" ble: ", pl["t"]*self.tfac, pl["s"])
                self.sendactivity("contours", contours=[[(pl["t"]*self.tfac,-10), (pl["t"]*self.tfac,90)]])
        elif cmd == "tmps":
            self.alltemps.PlotAll(self.sendactivity, self.tfac)
        elif cmd == "baro":
            self.baros.PlotAll(self.sendactivity, self.tfac)
        elif cmd == "gps":
            self.sendactivity(contours=cubecoordinates(1000*self.gfac, P3(0,0,0)), materialnumber=3)
            self.sendactivity("contours", contours=self.postrack.GetPath(fac=self.gfac))

        elif cmd == "gpsdrift":
            assert self.avgdrift is not None, "call avgdrift first"
            avgdrift = P3(self.avgdrift[0], self.avgdrift[1], 0)
            gcont = self.postrack.Ggcont()
            self.sendactivity(contours=cubecoordinates(1000*self.gfac, P3(0,0,0)), materialnumber=3)
            self.sendactivity("contours", contours=[[(p-avgdrift*((t-self.postrack.t0)*0.001))*self.gfac  for t, p in gcont  if self.postrack.t0<t<self.postrack.t1]])
            
        elif cmd == "gpsbaddilution":
            badhdilution = 120
            dcont = [ (self.postrack.euconc.GetPos3(pl)*self.gfac)  for pl in self.postrack.Qpls0  if pl["P"] and pl["P"]["h"]>badhdilution ]
            self.sendactivity(points=dcont, materialnumber=1)
        elif cmd == "gpsdilution":
            ppl = [ (pl["t"], pl["h"], pl["v"])  for pl in self.plines  if pl["C"] == "P" ]
            cont = [ (t*self.tfac, v, h)  for t, h, v in ppl ]
            self.sendactivity(contours=[cont], materialnumber=3)
        elif cmd[:3] == "igc":
            igcc = self.igcfiles[int(cmd[3:] or '0')]
            self.sendactivity("contours", contours=[igcc.GetPath(fac=self.gfac)], materialnumber=1)
        elif cmd == "gpsalt":
            self.sendactivity("contours", contours=[[(t*self.tfac, p[2]*self.gfac)  for t, p in self.postrack.Ggcont()]])
        elif cmd == "gpsdiffalt":
            gcont = self.postrack.Ggcont()
            dgcont = differentiatetvs(gcont, 15)
            self.sendactivity("contours", contours=[[(t*self.tfac, p[2])  for t, p in dgcont]])
            
            
        elif cmd == "gpsvel":
            vconts = self.postrack.GetVelPath3()
            self.sendactivity("contours", contours=[[(t*self.tfac, d/10)  for t, (d, v) in vcont]  for vcont in vconts])
            self.sendactivity("contours", contours=[[(t*self.tfac, v)  for t, (d, v) in vcont]  for vcont in vconts], materialnumber=3)
        elif cmd == "wind":
            self.windspeed.plotwind(self.sendactivity, self.tfac, sorted(self.tclicks[-2:]))
        elif cmd == "orientspectrum":  # same as ShowOrientSpectrum to look for gaps in the data
            ts = [ ]
            for i in range(1, len(self.orientdata.Spls)):
                ts.append((tfac*rl.orientdata.Zpls[i]["t"], (self.orientdata.Zpls[i]-self.orientdata.Zpls[i-1])*0.1))
            sendactivity(points=ts[:500000])
            return ts
        elif cmd == "orientationcalibration":
            self.orientdata.plotcalibrationquality(self.tfac, self.sendactivity)
            self.sendactivity(contours=[[(pl["t"]*self.tfac, -50), (pl["t"]*self.tfac, 5)]  for pl in self.orientdata.Ypls])
            print("\n".join(("%d %s" % (pl["t"], pl["s"]))  for pl in rl.orientdata.Ypls))
            
        # just bank this display in till we can work out the IR module wasn't just pointing at the sky in different directions
        elif cmd == "IRgpsside":
            FilterTempSeqPlotGPS([ (pl["t"], pl["i"])  for pl in self.alltemps.Ipls ], self.Tgpsfilterforpeaks, self.tfac, self.postrack.Ggcont(), self.gfac, self.sendactivity)
            
        # promising!
        elif cmd == "DLgpsside":
            FilterTempSeqPlotGPS([ (pl["t"], pl["c"])  for pl in self.alltemps.Dplslist[-1] ], self.Tgpsfilterforpeaks, self.tfac, self.postrack.Ggcont(), self.gfac, self.sendactivity)

        elif cmd == "DLgpssideRising":
            FilterTempSeqPlotGPSRising([ (pl["t"], pl["c"])  for pl in self.alltemps.Dplslist[-1] ], self.Tgpsfilterforpeaks*10, self.tfac, self.postrack.Ggcont(), self.gfac, self.sendactivity)
            
        elif cmd == "HTgpsside":
            FilterTempSeqPlotGPS([ (pl["t"], pl["c"])  for pl in self.alltemps.Hpls ], self.Tgpsfilterforpeaks, self.tfac, self.postrack.Ggcont(), self.gfac, self.sendactivity)

        elif cmd[:11] == "avgdriftabs":
            self.postrack.CalcAverageDriftAbs(self.sendactivity, int(cmd[11:] or -1))
        elif cmd[:8] == "avgdrift": 
            self.avgdrift, sd = self.postrack.CalcAverageDrift(self.sendactivity, int(cmd[8:] or -1))
        elif cmd[:8] == "altdrift": 
            zdirs = self.postrack.CalcPlotAltDrift(self.sendactivity, self.gfac)
            maxz = 100*int(zdirs[-1][0]/100+2)
            self.sendactivity(contours=[[(i*self.gfac,-10), (i*self.gfac, 10)]  for i in range(0,maxz,100)], materialnumber=1)
            self.sendactivity(contours=[[(0,0), (maxz,0)]])
            return zdirs
            
        elif cmd == "velocitychord":
            EstimateVelocityChord(self, self.sendactivity)

        elif cmd == "gravityagreement":
            self.orientdata.PlotGravityAgreements(self.tfac, self.sendactivity)

        elif cmd[:3] == "alt" or cmd == "dew":
            if self.geoframe == None:
                self.geoframe = GeoFrame(self.postrack, self.baros, self.alltemps, self.orientdata, self.windspeed)
            if cmd == "dew":
                self.geoframe.PlotDew(self.sendactivity, self.tfac)
            if cmd == "altdew":
                pfac = 1/1000
                poff = math.floor(self.baros.plo*pfac-1)/pfac
                self.geoframe.PlotDewpoint(self.sendactivity, pfac, poff)
            elif cmd == "alttmp":
                self.geoframe.PlotAgainstRising(self.geoframe.Attrap, self.sendactivity)
            elif cmd == "althumid":
                self.geoframe.PlotAgainstRising(self.geoframe.Ahtrap, self.sendactivity)
            elif cmd == "altdens":
                self.geoframe.PlotAgainstRising(self.geoframe.Adtrap, self.sendactivity)
            elif cmd == "altintegrateddensity":
                self.geoframe.IntegratedDensity(self.sendactivity)
            elif cmd == "altgpsbaro":
                self.geoframe.PlotGPSbaroscale(self.sendactivity)  # does 100m interplations to try and match the density
            elif cmd == "altgpsbaropath":
                self.geoframe.PlotGPSbaropath(self.sendactivity)
            elif cmd == "altwind":
                self.geoframe.PlotAltWindspeed(self.sendactivity)
            elif cmd == "altbarodifferential":
                self.geoframe.PlotAltRateDifferential(self.sendactivity)

        elif cmd[:4] == "diff":
            if self.accelerationderivations == None:
                self.accelerationderivations = AccelerationDerivations(self)
            if len(self.tclicks) >= 2:
                ts2 = sorted(self.tclicks[-2:])
                self.accelerationderivations.t0, self.accelerationderivations.t1 = ts2
            else:
                self.accelerationderivations.t0, self.accelerationderivations.t1 = self.postrack.t0, self.postrack.t1

            if cmd[-4:] == "cont":
                self.accelerationderivations.PlotList(cmd[4:])
            elif cmd == "diffdgvshift":
                self.accelerationderivations.Plotshiftaxes()
                self.accelerationderivations.Plotdgvshift()
            elif cmd == "diffdvashift":
                self.accelerationderivations.Plotshiftaxes()
                self.accelerationderivations.Plotdvashift()
            elif cmd == "diffddgashift":
                self.accelerationderivations.Plotshiftaxes()
                self.accelerationderivations.Plotddgashift()
                
            
        else:
            print('plot(cmd) where cmd in ["ble", "tmps", "baro", "gps", "gpsvel", "gpsalt", "wind", "IRgpsside", "HTgpsside", "DLgpsside", "avgdrift[0-3,10-13]", "avgdriftabs", "altdrift"]')
            print('  altitude graphs "altdew", "alttmp", "althumid", "altdens", altintegrateddensity", "altgpsbaro", "altwind", "altbarodifferential"')
            print('  "diffgcont", "diffvcont", "diffacont", "diffdgcont", "diffdvcont", "diffddgcont", "diffdgvshift", "diffdvashift", "diffddgashift", "diffRacont", "diffRq0cont", "diffRtq22cont"(bank), "diffRtq12cont"(pitch)')
            print('  rl.loadIGC(fname); rl.plot("igc0")')

        return res
        
    def Gpyclick(self, p, v):
        gcont = self.postrack.Ggcont()
        cp = P3(*p)*(1/self.gfac)
        vp = P3(*v) 
        vhl = vp.LenLZ()
        if vhl == 0:
            ml, mt, mp = min(((cp.x - gp.x)**2 + (cp.y - gp.y)**2, gt, gp)  for gt, gp in gcont)
        else:
            vhp = P3.ConvertGZ(P2.ConvertLZ(vp)*(vp.z/vhl), -vhl)
            vhc = P3.Cross(vp, vhp)
            ml, mt, mp = min((P3.Dot(gp - cp, vhp)**2 + P3.Dot(gp - cp, vhc)**2, gt, gp)  for gt, gp in gcont)
        tprev = self.tclicks and self.tclicks[-1] or 0
        print("t=%d p=(%.3f, %.3f, %.3f)  %dmins %dsecs %s" % (mt, mp[0], mp[1], mp[2], int(abs(tprev-mt)/60000), int(abs(tprev-mt)/1000)%60, "earlier" if tprev > mt else "later"))
        self.tclicks.append(mt)
        self.sendactivity(points=[mp*self.gfac])
        self.sendactivity(contours=[[(mt*self.tfac, -50), (mt*self.tfac, 50)]], materialnumber=2)
        return (mt, mp)
        

# we want a MS4525DO
# or order: http://www.buildyourowndrone.co.uk/air-speed-sensor-kit-mpxv7002dp.html
# and use the ADS on 5V
# double up to get direction        

#from flightlog import ShowOrientSpectrum
#ts = ShowOrientSpectrum(fname, sendactivity)
def ShowOrientSpectrum(fname, sendactivity):
    import re
    fin = open(fname)
    sre = [int(s, 16)  for s in re.findall("Zt([0-9A-F]{8})", fin.read())]
    tfac = 1/60000
    ts = [ ]
    for i in range(1, len(sre)):
        ts.append((tfac*sre[i], (sre[i]-sre[i-1])*0.1))
    sendactivity(points=ts[:500000])
    return ts
    
def PlotWaypoints(wname, rl):
    #wname = "/home/goatchurch/hgstuff/XCSoarData/BOS_Wales.cup"
    import csv
    k = list(csv.reader(open(wname, "r")))
    wpoints = [ dict(zip(k[0], kk))  for kk in k[1:] ]
    dwpoints = dict([ (wp["name"], rl.postrack.euconc.GetPosIGCP(\
          1000*(int(wp["lat"][:2])*60+float(wp["lat"][2:-1]))*10, \
          -1000*(int(wp["lon"][:3])*60+float(wp["lon"][3:-1]))*10, \
          float(wp["elev"][:-1])*10*0.1))  for wp in wpoints ])
    sendactivity(points=[p*rl.gfac  for p in dwpoints.values()])
    wp = [wp  for wp in wpoints  if wp["name"] == "SGM017 Gyrn Moelfre"][0]
    
    p = dwpoints["SGM017 Gyrn Moelfre"]
    sendactivity(points=[p*rl.gfac], materialnumber=1)
    p = dwpoints["SLG013 Llangollen"]
    sendactivity(points=[p*rl.gfac], materialnumber=1)
    

# test for gaps
"""
import re
fname = "/media/goatchurch/hglogger22/OOR/004.TXT"
fname = "/home/goatchurch/hgstuff/2016-04-30-builthwells/005.TXT"
fname = "/media/goatchurch/hglogger22/OOR/003.TXT"
y = 0.1
fin = open(fname)
while not re.match("\s*$", fin.readline()):
    pass
ts = [ ]
for line in fin:
    m = re.match("\wt([0-9A-f]{8})", line)
    ts.append(int(m.group(1), 16))

ts[0], ts[-1], ts[-10000]

sendactivity("clearalltriangles")
lts = ts[-10000:]
tfac = 0.0002
sendactivity(points=[((t-lts[0])*tfac, y)  for t in lts])

sendactivity(contours=[[(t*tfac,-1), (t*tfac,1)]  for t in range(0, lts[-1] - lts[0], 1000)])
"""
    
