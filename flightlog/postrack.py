import math
from math import sin, cos, radians, sqrt, atan2
from flightlog.utils import TimeSampleRate, convexpbiway, StrictTimeSampleRate, SimpleLinearRegression
from flightlog.utils import MeanSD, TSampleRateG, ziptvs
from basicgeo import P3, P2, Along

def cubecoordinates(edgelength, origp=P3(0,0,0)):
    cs = [P3(0,0,0), P3(0,edgelength,0), P3(edgelength,edgelength,0), P3(edgelength,0,0), origp]
    conts = [ [c+lcs  for lcs in cs]  for c in [origp, origp+P3(0,0,edgelength)] ]
    for lcs in cs[:-1]:
        conts.append([origp+lcs, origp+lcs+P3(0,0,edgelength)])
    return conts

class Eucon:
    def __init__(self, Rpl, Qpl0):
        if Qpl0 is not None:
            self.x0 = Qpl0["x"]   # in minutes*10000
            self.y0 = Qpl0["y"]
            self.a0 = Qpl0["a"]
        else:
            self.x0, self.y0, self.a0 = None, None, None
        if Rpl is not None:
            self.latdivisorE100 = Rpl["e"]
            self.latdivisorN100 = Rpl["n"]
            self.lngdivisorE100 = Rpl["f"]
            self.lngdivisorN100 = Rpl["o"]
                 # convert into fac multiply (division is very bad and can't handle zero fac case)
            self.eyfac = 100/self.latdivisorE100
            self.exfac = 100/self.lngdivisorE100
            self.nyfac = 100/self.latdivisorN100
            self.nxfac = 100/self.lngdivisorN100
        else:
            earthrad = 6378137
            self.nyfac = 2*math.pi*earthrad/360/600000
            self.nxfac = 0
            self.eyfac = 0
            if self.y0 is not None:
                self.exfac = self.nyfac*math.cos(math.radians(self.y0/600000))
            else:
                self.exfac = None
        
    def GetPos3(self, pl):
        rx = (pl["x"]-self.x0)
        ry = (pl["y"]-self.y0)
        #me, mn = ry*100/self.latdivisorE100+rx*100/self.lngdivisorE100, ry*100/self.latdivisorN100+rx*100/self.lngdivisorN100
        me, mn = ry*self.eyfac+rx*self.exfac, ry*self.nyfac+rx*self.nxfac
        ma = pl["a"]*0.1-0*self.a0*0.1
        return P3(me, mn, ma)
        
    def GetPosIGC(self, l):
        assert l[0] == 'B', ("bad IGC line", l)
        utime = int(l[1:3])*3600+int(l[3:5])*60+int(l[5:7])
        latminutes1000 = int(l[7:9])*60000+int(l[9:11])*1000+int(l[11:14])
        lngminutes1000 = (int(l[15:18])*60000+int(l[18:20])*1000+int(l[20:23]))*(l[24]=='E' and 1 or -1) 
        alt10 = int(l[25:31])
        altbaro = int(l[31:])
        ma = alt10*0.1-0*self.a0*0.1
        return utime, self.GetPosIGCP(latminutes1000*10, lngminutes1000*10, ma)

    def GetPosIGCP(self, latminutes10000, lngminutes10000, z):
        if self.x0 is None:
            self.x0 = lngminutes10000
            self.y0 = latminutes10000
            self.exfac = self.nyfac*math.cos(math.radians(self.y0/600000))
        rx = (lngminutes10000-self.x0)
        ry = (latminutes10000-self.y0)
        #me, mn = ry*100/self.latdivisorE100+rx*100/self.lngdivisorE100, ry*100/self.latdivisorN100+rx*100/self.lngdivisorN100
        me, mn = ry*self.eyfac+rx*self.exfac, ry*self.nyfac+rx*self.nxfac
        return P3(me, mn, z)
        

class PosTrack:
    def __init__(self, plines):
        print("\nGPS:") 
        self.Qplslist = [ ]
        self.Vplslist = [ ]  # v velkph100, d veldegrees100 

        self.gpssamplerate = 100  # samples every 100ms (unless it's 500 and we should define this in the header part)
        self.ggcont = None
        
        # insert breaks where there is a data gap of more than 5secs
        Qpl = None
        for pl in plines:
            if pl["C"] == 'V':
                if Qpl and pl["t"] - Qpl["t"] < 180:  # assign this to the previous position record if it's within the timewindow of 200ms
                    Qpl["V"] = pl   # assumed corresponding velocity value
            if pl["C"] == 'Q':
                Qpl = pl
                if not self.Qplslist or not self.Qplslist[-1] and pl["t"] - self.Qplslist[-1][-1]["t"] > 5000:
                    self.Qplslist.append([])
                pl["V"] = None
                self.Qplslist[-1].append(pl)

        for Qpls in self.Qplslist:
            badqsi = [ ]
            maxda, maxdx, maxdy = 1500, 2000, 2000
            for i in range(1, len(Qpls)):
                pl0, pl1 = Qpls[i-1], Qpls[i]
                if abs(pl1["a"] - pl0["a"]) > maxda or abs(pl1["x"] - pl0["x"]) > maxdx or abs(pl1["y"] - pl0["y"]) > maxdy:
                    badqsi.append(i)
            badqsi2 = [badqsi[j-1]  for j in range(1, len(badqsi))  if badqsi[j-1]+1 == badqsi[j]]
            #for i in badqsi2:
            #    print(Qpls[i-1]["a"], Qpls[i]["a"], Qpls[i+1]["a"])
            if badqsi2:
                print("Thinning bad Qpls readings", badqsi2)
            for i in reversed(badqsi2):
                del Qpls[i]

        if self.Qplslist and self.Qplslist[0]:  # quick bodge for fridge data where there are no valid gps records
            self.euconc = Eucon(None, self.Qplslist[0][0])
                
        # (doesnt seem we need to filter the Vplslist for spikes in the examples I've seen yet)

        Rpls = [ pl  for pl in plines  if pl["C"] == "R" ]
        if len(Rpls) == 0:
            print("No GPS track")
            self.t0, self.t1 = plines[0]["t"], plines[-1]["t"]
            return
            
        Rpl = Rpls[0]
        print("GPS date: ", Rpl["d"])
        Qpls0 = self.Qplslist[0]
        Qpl0 = self.Qplslist[0][0]
        #self.euconc = Eucon(Rpl, Qpl0)
        print("ignoring grid rotation", Rpl)
        self.euconc = Eucon(None, Qpl0)
        self.Qpls0 = Qpls0
        
        for nq in range(len(self.Qplslist)):
            self.PrintExtents(nq)

        # this is in v=velkph100 and d=degrees100
        for Qpls in self.Qplslist:
            self.Vplslist.append([ pl  for pl in plines  if pl["C"] == 'V' and Qpls[0]["t"] <= pl["t"] <= Qpls[-1]["t"] ])
        
        self.gpsclockconvert = [ ]
        for Qpls in self.Qplslist:
            if "u" in Qpls[0]:
                self.gpsclockconvert.append(SimpleLinearRegression([ (pl["t"], pl["u"])  for pl in Qpls ]))
            else:
                self.gpsclockconvert.append((1, 0))
        self.gpst0, self.gpststep = 0, self.gpssamplerate/self.gpsclockconvert[0][0]   # need to 
        print("GPS clock convert m=%f c=%f, cycle %f ms" % (self.gpsclockconvert[0][0], self.gpsclockconvert[0][1], self.gpststep))
        print("Start time %02d:%02d end %02d:%02d" % (int(Qpls0[0]["u"]/3600000), (int(Qpls0[0]["u"]/60000) % 60), int(Qpls0[-1]["u"]/3600000), (int(Qpls0[-1]["u"]/60000) % 60)))

        self.t0, self.t1 = self.DeriveFlightTime(self.GetPath3()[0])
        m, c = self.gpsclockconvert[0]
        u0, u1 = self.t0*m + c, self.t1*m + c
        print("Flight time start: %02d:%02d end %02d:%02d" % (int(u0/3600000), (int(u0/60000) % 60), int(u1/3600000), (int(u1/60000) % 60)))
        
    def DeriveFlightTime(self, cont):
        dis, dds = 10, 15
        t0, t1 = cont[0][0], cont[-1][0]
        for i in range(dis, len(cont) - dis*5):
            tdiff = (cont[i][0] - cont[i-dis][0])*0.001
            vv = (cont[i][1] - cont[i-dis][1]).LenLZ()/tdiff
            if vv > 1.1:
                # extra filter if we still have significant movement 15 seconds later
                tdiff1 = (cont[i+dis*dds][0] - cont[i+dis*(dds-1)][0])*0.001
                vv1 = (cont[i+dis*dds][1] - cont[i+dis*(dds-1)][1]).LenLZ()/tdiff1
                if vv1 > 2.1:  
                    t0 = cont[i-dis][0]
                    break
                    
        # usually the gps has stabilized by the end of the flight so only need one stationary test
        for i in range(len(cont)-1, dis-1, -1):
            tdiff = (cont[i][0] - cont[i-dis][0])*0.001
            vv = (cont[i][1] - cont[i-dis][1]).LenLZ()/tdiff
            if vv > 1.1:
                t1 = cont[i][0]
                break
        return t0, t1
        
            
    def PrintExtents(self, nq):
        Qpls = self.Qplslist[nq]
        if nq != 0:
            print("\n**** Qplslist[%d] " % nq)
        print("  %d samples between minute %f and minute %f" % (len(Qpls), Qpls[0]["t"]/60000, Qpls[-1]["t"]/60000))
        Qpl = Qpls[0]
        mpts = [ ]
        cumulativedistance = 0.0
        pme, pmn = None, None
        summe, summn = 0.0, 0.0
        for pl in Qpls:
            rx = (pl["x"]-Qpl["x"])
            ry = (pl["y"]-Qpl["y"])
            #me, mn = ry*100/latdivisorE100+rx*100/lngdivisorE100, ry*100/latdivisorN100+rx*100/lngdivisorN100
            me, mn = ry*self.euconc.eyfac+rx*self.euconc.exfac, ry*self.euconc.nyfac+rx*self.euconc.nxfac
            mpts.append((me, mn))
            summe += me
            summn += mn
            if pme is not None:
                cumulativedistance += math.sqrt((me-pme)**2 + (mn-pmn)**2)
            pme, pmn = me, mn
        midme, midmn = summe/len(Qpls), summn/len(Qpls)
        radsq = max((me-midme)**2 + (mn-midmn)**2  for me, mn in mpts)
        print("    distance extent %f metres, distance moved %f" % (math.sqrt(radsq)*2, cumulativedistance))
        print("    time sample rate %f ms at sd %f ms" % TimeSampleRate(Qpls))

            
    def GetPath(self, fac=1.0):
        if not self.Qplslist:
            print("no GPS track")
            return [ ]

        conts = [ ]
        for Qpls in self.Qplslist:
            conts.append([])
            for pl in Qpls:
                conts[-1].append(self.euconc.GetPos3(pl)*fac)
        return conts
    
    def GetPath3(self):
        if not self.Qplslist:
            print("no GPS track")
            return [ [ ] ]
        conts = [ ]
        for Qpls in self.Qplslist:
            conts.append([])
            for pl in Qpls:
                conts[-1].append((pl["t"], self.euconc.GetPos3(pl)))
        return conts
            
    def Ggcont(self):
        if self.ggcont is None:
            self.ggcont = self.GetPath3()[0]
        return self.ggcont


    def GetVelPath3(self, bpolar=False):
        if not self.Vplslist:
            print("no GPS track")
            return [ ]

        conts = [ ]
        for Vpls in self.Vplslist:
            conts.append([])
            for pl in Vpls:
                d = pl["d"]
                if d > 0xFF00: # occasional negation on the limit
                    d = (d - 0x10000 + 36000)
                dirdeg = d/100
                velms = pl["v"]/360
                if bpolar:
                    conts[-1].append((pl["t"], (d/100, pl["v"]/360)))  # (veldegrees100, velkph100) to (deg, m/s)
                else:
                    conts[-1].append((pl["t"], P2(math.sin(math.radians(dirdeg))*velms, math.cos(math.radians(dirdeg))*velms)))
        return conts

    # function split up to allow for plotting of several slices
    def CalcAverageDrift(self, sendactivity, iquarter=-1):
        vcont = self.GetVelPath3(True)[0]
        if 0 <= iquarter <= 3:
            i0, i1 = len(vcont)*(iquarter)//4, len(vcont)*(iquarter+1)//4
            print("avgdrift quarter %d from minute %f to minute %f" % (iquarter, vcont[i0][0]/60000, vcont[i1-1][0]/60000))
            lvcont = vcont[i0:i1]
        elif 10 <= iquarter <= 13:
            izquarter = iquarter - 10
            t0, t1 = self.t0, self.t1
            gcont = self.Ggcont()
            kvcont = ziptvs(vcont, [(t,p.z)  for t, p in gcont  if t0<t<t1])
            zmin = min(z  for t, v, z in kvcont)
            zmax = max(z  for t, v, z in kvcont)
            iquarter=3
            zlo, zhi = Along(izquarter/4, zmin, zmax), Along((izquarter+1)/4, zmin, zmax)
            lvcont = [(t,v)  for t,v,z in kvcont  if zlo<z<zhi]
            print("avgdrift quarter %d from alt %fm to %fm" % (iquarter, zlo, zhi))
        else:
            lvcont = vcont
            
        deghistrad = self.GetDegHistRad(lvcont)
        return self.CalcPlotAverageDrift(deghistrad, sendactivity)

    # absolute pos value instead of velocity
    def CalcAverageDriftAbs(self, sendactivity, iquarter=-1):
        cont = self.Ggcont()
        deghistrad = self.GetDegHistRadAbs(cont)
        return self.CalcPlotAverageDrift(deghistrad, sendactivity)
        
    def GetDegHistRad(self, vcont):
        degreehistogram = [ [0, 0.0, 0.0]  for i in range(361)]
        for t, (d, v) in vcont:
            if v > 1.5:
                dh = degreehistogram[int(d + 0.5)]
                dh[0] += 1
                dh[1] += v
                dh[2] += v**2
        return [ (sv/max(n,1), sqrt(max(0, svsq - sv**2/max(n,1))/max(n,1)))  for n, sv, svsq in degreehistogram ]
        
    def GetDegHistRadAbs(self, cont):
        degreehistogram = [ [0, 0.0, 0.0]  for i in range(361)]
        dis = 5   # one second approx
        ts = [ t1 - t0  for (t0,p0), (t1,p1) in zip(cont, cont[dis:]) ]
        tdiff = sum(ts)/len(ts)*0.001
        for (t0,p0), (t1,p1) in zip(cont, cont[dis:]):
            vv = p1 - p0
            v = vv.LenLZ()/tdiff
            if v < 1:  continue
            d = math.degrees(math.atan2(vv[0], vv[1]))
            if d < 0:
                d += 360
            dh = degreehistogram[int(d + 0.5)]
            dh[0] += 1
            dh[1] += v
            dh[2] += v**2
        return [ (sv/max(n,1), math.sqrt(max(0, svsq - sv**2/max(n,1))/max(n,1)))  for n, sv, svsq in degreehistogram ]
        
        
    def CalcPlotAverageDrift(self, deghistrad, sendactivity):
        pts = [ (sin(radians(d))*r, cos(radians(d))*r)  for d, (r, sd) in enumerate(deghistrad) ]
        if sendactivity:
            sendactivity("contours", contours=[[(10,0), (0,0), (0,10)]])
            sendactivity("contours", contours=[ pts ], materialnumber=3)
            sendactivity("contours", contours=[ [(0,0), p]  for p in pts[15::30] ], materialnumber=3)
            sendactivity("contours", contours=[ [ (sin(radians(d))*(r+sd*f), cos(radians(d))*(r+sd*f))  for d, (r, sd) in enumerate(deghistrad) ]  for f in [-1,1] ], materialnumber=2)

        import scipy.optimize
        def radsd(C, pts):
            cx, cy = C
            n = len(pts)
            dsqs = [ (cx - x)**2 + (cy - y)**2  for x, y in pts ]
            sumdls = sum(sqrt(dsq)  for dsq in dsqs)
            sumdsqs = sum(dsqs)
            return sumdls/n, sqrt(max(0, sumdsqs*n - sumdls**2))/(n-1)

        def BestCentre(pts):
            def fun(C):
                return radsd(C, pts)[1]
            res = scipy.optimize.minimize(fun=fun, x0=(0, 0))
            return tuple(res.x)

        C = BestCentre(pts)
        C = P2(C[0], C[1])
        r, sd = radsd(C, pts)

        if sendactivity:
            sendactivity("contours", contours=[[(sin(radians(i))*r + C[0], cos(radians(i))*r + C[1])  for i in range(360)]], materialnumber=1)
            sendactivity("contours", contours=[[(0,0), C]], materialnumber=1)
            sendactivity("points", points=[C], materialnumber=1)
        
        print("Wind vector is ", C, "heading", ((C.Arg()+90)%360), "speed", C.Len(), "and the average relative groundspeed is", r, "m/s  sd=", sd)
        return C, sd


    def CalcPlotAltDrift(self, sendactivity, gfac):
        gcont = self.Ggcont()
        vcont = self.GetVelPath3(True)[0]
        t0, t1 = self.t0, self.t1
        kvcont = ziptvs(vcont, [(t,p.z)  for t, p in gcont  if t0<t<t1])
        zmin = min(z  for t, v, z in kvcont)
        zmax = max(z  for t, v, z in kvcont)
        s, dz = 40, 50
        zs = [ Along(lam, zmin, zmax)   for lam in (i/s  for i in range(s+1)) ]
        zdirs = [ ]
        for zm in zs:
            lvcont = [(t,v)  for t,v,z in kvcont  if zm-dz<z<zm+dz]
            deghistrad = self.GetDegHistRad(lvcont)
            p, sd = self.CalcPlotAverageDrift(deghistrad, None)
            zdirs.append((zm, p, sd))
        sendactivity(contours=[[(zm*gfac, sd)  for zm, p, sd in zdirs]], materialnumber=2)
        sendactivity(contours=[[(zm*gfac, 0), (zm*gfac+p[0], p[1])]  for zm, p, sd in zdirs], materialnumber=3)
        return zdirs


# SetRelativeVelocities(rl.postrack.Qpls0, rl.postrack.euconc, 5)
def SetRelativeVelocities(Qpls0, euconc, dis):
    for i in range(dis, len(Qpls0)-dis):
        pl = Qpls0[i]
        tmid = pl["t"]
        pprev, pmid, pnext = euconc.GetPos3(Qpls0[i-dis]), euconc.GetPos3(pl), euconc.GetPos3(Qpls0[i+dis])
        tdprev, tdnext = 1000/(pl["u"] - Qpls0[i-dis]["u"]), 1000/(Qpls0[i+dis]["u"] - pl["u"])
        tdmid = 1000/(Qpls0[i+dis]["u"] - Qpls0[i-dis]["u"])
        pl["vp"] = (pmid - pprev)*tdprev
        pl["vn"] = (pnext - pmid)*tdnext
        pl["vm"] = (pnext - pprev)*tdmid
        if pl["V"]:
            pl["vv"] = P2(sin(radians(pl["V"]["d"]/100))*pl["V"]["v"]/360, cos(radians(pl["V"]["d"]/100))*pl["V"]["v"]/360)
        else:
            pl["vv"] = None
        
    # run-offs to simplify later code
    for i in range(0, dis):
        Qpls0[i]["vp"] = Qpls0[dis]["vp"]
        Qpls0[i]["vn"] = Qpls0[dis]["vn"]
        Qpls0[i]["vm"] = Qpls0[dis]["vm"]
        Qpls0[i]["vv"] = None
    for i in range(-dis, 0):
        Qpls0[i]["vp"] = Qpls0[-dis-1]["vp"]
        Qpls0[i]["vn"] = Qpls0[-dis-1]["vn"]
        Qpls0[i]["vm"] = Qpls0[-dis-1]["vm"]
        Qpls0[i]["vv"] = None


# use this to smooth the infrared temps and plot above and below values on the GPS
def FilterTempSeqPlotGPS(tvs, sfac, tfac, gtrack, gfac, sendactivity):
    sendactivity("contours", contours=[[(t*tfac, v)  for t, v in tvs]], materialnumber=2)
    stvs = convexpbiway(tvs, sfac)
    sendactivity("contours", contours=[[(t*tfac, v)  for t, v in stvs]], materialnumber=3)
    tranges = [ ]
    bopen = False
    for (t, v), (st, sv) in zip(tvs, stvs):
        if v > sv:
            if not bopen:
                tranges.append(t)
                tranges.append(t)
                bopen = True
            tranges[-1] = t
        else:
            bopen = False
        
    def trcontains(t, tranges):
        for i in range(1, len(tranges), 2):
            if tranges[i-1] > t:
                break
            if tranges[i] >= t:
                return True
        return False

    sendactivity("points", points=[(p.x*gfac, p.y*gfac, p.z*gfac)  for t, p in gtrack  if trcontains(t, tranges)], materialnumber=3)



# use this to smooth the infrared temps and plot above and below values on the GPS
def FilterTempSeqPlotGPSRising(tvs, sfac, tfac, gtrack, gfac, sendactivity):
    sendactivity("contours", contours=[[(t*tfac, v)  for t, v in tvs]], materialnumber=2)
    stvs = convexpbiway(tvs, sfac)
    sendactivity("contours", contours=[[(t*tfac, v)  for t, v in stvs]], materialnumber=3)
    tranges = [ ]
    bopen = False
    for (t, v), (st, sv) in zip(stvs, stvs[1:]):
        if v < sv:
            if not bopen:
                tranges.append(t)
                tranges.append(t)
                bopen = True
            tranges[-1] = t
        else:
            bopen = False
        
    def trcontains(t, tranges):
        for i in range(1, len(tranges), 2):
            if tranges[i-1] > t:
                break
            if tranges[i] >= t:
                return True
        return False

    sendactivity("points", points=[(p.x*gfac, p.y*gfac, p.z*gfac)  for t, p in gtrack  if trcontains(t, tranges)], materialnumber=3)


# use euconc function calls
def PlotGPSagreements(postrack0, postrack1, tfac, sendactivity):
    euconc = postrack0.euconc
    Qpls0 = postrack0.Qpls0
    Qpls1 = postrack1.Qpls0
    
    uQpls1 = dict((pl["u"], pl)  for pl in Qpls1)
    cont = [ ]
    contv = [ ]
    ts0, ts1 = [ ], [ ]
    for pl0 in Qpls0:
        if pl0["u"] not in uQpls1:  continue
        if not postrack0.t0 < pl0["t"] < postrack0.t1:  continue
        
        pl1 = uQpls1[pl0["u"]]
        rx = (pl0["x"]-pl1["x"])
        ry = (pl0["y"]-pl1["y"])
        ra = (pl0["y"]-pl1["y"])/10
        #me, mn = ry /latdivisorE100+rx/lngdivisorE100, ry/latdivisorN100+rx/lngdivisorN100
        me, mn = ry*euconc.eyfac+rx*euconc.exfac, ry*euconc.nyfac+rx*euconc.nxfac
        v = P2(me, mn)
        
        cont.append((tfac*pl0["t"], v.Len()))
        contv.append((tfac*pl0["t"], ra))
        ts0.append(pl0["t"])
        ts1.append(pl1["t"])
        
    sendactivity(contours=[cont])
    sendactivity(contours=[contv], materialnumber=2)
    t00, t01 = min(ts0), max(ts0)
    t10, t11 = min(ts1), max(ts1)
    print("common trange0", t00, t01)
    print("common trange1", t10, t11)

    #Ppls = [ pl  for pl in rl1.plines  if pl["C"] == 'P' ]
    #sendactivity(contours=[[(pl["t"]*rl.tfac, pl["v"]*0.1)  for pl in Ppls]], materialnumber=1)

    gfac = 0.1
    sendactivity("contours", contours=[[v*gfac  for t, v in postrack0.GetPath3()[0]  if t00<t<t01]])
    sendactivity("contours", contours=[[v*gfac  for t, v in postrack1.GetPath3()[0]  if t10<t<t11]], materialnumber=3)


def EstimateVelocityChord(rl, sendactivity):
    t0, t1 = rl.postrack.t0, rl.postrack.t1
    Qpls = [ pl  for pl in rl.postrack.Qpls0  if t0<pl["t"]<t1 ]
    euconc = rl.postrack.euconc
    gpsamplerate = 100*int(TSampleRateG(pl["u"]  for pl in Qpls)[0]/100+0.5)
    print("number of gps samples", len(Qpls))

    def RelVelErrs(Qpls, euconc, dis0, dis1):
        tgap = gpsamplerate*(dis1 - dis0)
        relvs, tds, ivs = [ ], [ ], [ ]
        for i in range(len(Qpls)):
            if not 0 < i+dis0 < len(Qpls) or not 0 < i+dis1 < len(Qpls): continue
            pl = Qpls[i]
            Vpl = pl["V"]
            if not Vpl:  continue
            vv = P2(sin(radians(Vpl["d"]/100))*Vpl["v"]/360, cos(radians(Vpl["d"]/100))*Vpl["v"]/360)
            pprev, pnext = euconc.GetPos3(Qpls[i+dis0]), euconc.GetPos3(Qpls[i+dis1])
            td = (Qpls[i+dis1]["u"] - Qpls[i+dis0]["u"])
            if td != tgap:  continue
            if td != 0:
                vm = (pnext - pprev)*(1000/td)
                relvs.append(P2.ConvertLZ(vm) - vv)
                tds.append(td)
                ivs.append(i)
        return relvs, tds, ivs

    d01err = [ ]
    for dis0 in range(-10, 10):
        for dis1 in range(-10, 10):
            if dis0<dis1:
                rve, tds, ivs = RelVelErrs(Qpls, euconc, dis0, dis1)
                errv = sum(v.Len()  for v in rve)*(1/len(rve))
                print(dis0, dis1, errv, MeanSD(tds))
                d01err.append((dis0, dis1, errv))
                sendactivity("clearallpoints")
                #sendactivity(points=rve)

    sendactivity("clearallpoints")
    sendactivity(points=d01err)
    sendactivity(contours=[[(v[0],v[1],0),v]  for v in d01err])
    sendactivity(points=[min((v  for v in d01err  if v[0]==v0), key=lambda x:x[2])  for v0 in set(v[0]  for v in d01err)], materialnumber=1)
    sendactivity(points=[min((v  for v in d01err  if v[1]==v1), key=lambda x:x[2])  for v1 in set(v[1]  for v in d01err)], materialnumber=2)

    d01err.sort(key=lambda x:x[2])
    optp = min(d01err, key=lambda x:x[2])
    sendactivity(points=[optp], materialnumber=3)
    dis0, dis1 = optp[0], optp[1]
    print("GPS velocity best matches chord %f and %f ms giving an error of %f" % (dis0*gpsamplerate, dis1*gpsamplerate, optp[2]))
    print("others:")
    for op in d01err[1:5]:
        print("    chord %f and %f ms error %f" % (op[0]*gpsamplerate, op[1]*gpsamplerate, op[2]))

    # plot error distributions
    rve, tds, ivs = RelVelErrs(Qpls, euconc, dis0, dis1)
    sendactivity(points=[(v[0]+10, v[1])  for v in rve])
    for i, v in zip(ivs, rve):
        Qpls[i]["errv"] = v
          
    vds = [v.Len()  for v in rve]
    vds.sort(reverse=True)
    vds[0]
    sendactivity(contours=[[(v+10, i/len(vds)-10)  for i, v in enumerate(vds)]])


"""
from flightlog.postrack import Eucon, IGCfile
eucon = Eucon(None, None)
igcfile = IGCfile(fname, eucon)
cont = igcfile.GetPath(0.1)
sendactivity(contours=[[(x,-y,z)  for x, y, z in cont]])

-or-
rl.loadIGC(ifname)
rl.plot("igc0")
"""


class IGCfile:
    def __init__(self, fname, postrack):
        if type(postrack) == PosTrack:
            self.postrack = postrack
            self.euconc = postrack.euconc
        elif type(postrack) == Eucon:
            self.euconc = postrack
        else:
            assert postrack is None
            self.euconc = None

        self.Blines = [ ]
        for l in open(fname):
            if l[0] == 'B':
                utime = int(l[1:3])*3600+int(l[3:5])*60+int(l[5:7])
                latminutes1000 = int(l[7:9])*60000+int(l[9:11])*1000+int(l[11:14])
                lngminutes1000 = (int(l[15:18])*60000+int(l[18:20])*1000+int(l[20:23]))*(l[23]=='E' and 1 or -1) 
                alt10 = int(l[25:31])
                altbaro = int(l[31:])
                self.Blines.append((utime, latminutes1000, lngminutes1000, alt10, altbaro))
                

    def GetPath(self, fac):
        return [ self.euconc.GetPosIGCP(latminutes1000*10, lngminutes1000*10, alt10*0.1)*fac  for utime, latminutes1000, lngminutes1000, alt10, altbaro in self.Blines]

    def GetPath3(self):
        if self.euconc is None:
            self.euconc = Eucon(None, {"y":self.Blines[0][1]*10, "x":self.Blines[0][2]*10, "a":self.Blines[0][3]})
        return [(utime*1000, self.euconc.GetPosIGCP(latminutes1000*10, lngminutes1000*10, alt10*0.1))  for utime, latminutes1000, lngminutes1000, alt10, altbaro in self.Blines ]

    #def GetPath3_RL(self):
    #    assert self.postrack
    #    return [(pl["u"], self.euconc.GetPosIGCP(pl["y"], pl["x"], pl["a"]*0.1))  for pl in self.postrack.Qpls0]

