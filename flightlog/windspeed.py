
# http://soldernerd.com/arduino-ultrasonic-anemometer/

# should wind speed calibration from vehicle with bluetooth messages of what the current speed is, for example.  
# or use the gps velocity
class WindspeedData:
    def __init__(self, plines):
        self.Wpls = [ ]    
        prevWpl = None

        for pl in plines:
            if pl["C"] == "W":
                if prevWpl:
                    pl["m"] = pl["n"] - prevWpl["n"]
                    pl["s"] = pl["t"] - prevWpl["t"]
                    pl["w"] = pl["w"] & 0xFFFF  # unsigned (microseconds for a 5-blade revolution)
                    self.Wpls.append(pl)
                prevWpl = pl
                
        self.Wplslist = [ [ ] ]
        for pl in self.Wpls:
            if pl["w"] < 20000:
                self.Wplslist[-1].append(pl)
            elif self.Wplslist[-1]:
                self.Wplslist.append([])
        #print(len(self.Wplslist), max(map(len, self.Wplslist)))
        #print(list(map(len, self.Wplslist)))
        self.WplsL = max(self.Wplslist, key=lambda x: len(x))
        #print(len(self.WplsL))
        
        # bodge a running total back together
        self.wspeeds = sum(([(pl["t"], 80000/pl["w"])  for pl in Wpls]  for Wpls in self.Wplslist), [])
        if self.wspeeds:
            print("Windspeed readings < revtime 20milliseconds:", len(self.wspeeds), "of total", len(self.Wpls))
        else:
            print("No windspeeds")
        self.refplotsize = 6000

    def plotwind(self, sendactivity, tfac, tclicks2):
        if len(tclicks2) == 2:
            t0, t1 = tclicks2
            wt0 = min(i  for i, pl in enumerate(self.Wpls)  if t0<pl["t"]<t1)
            wt1 = max(i  for i, pl in enumerate(self.Wpls)  if t0<pl["t"]<t1)
            nt = wt1 - wt0
        else:
            t0, t1 = self.Wpls[0]["t"], self.Wpls[-1]["t"]
            nt = len(self.Wpls)
        wsplotstep = max(1, int(nt/self.refplotsize))
        print("plotstep", wsplotstep, "on", nt, "wind points")
        
        conts = [[(pl["t"]*tfac, 80000/pl["w"]*0.1)  for pl in Wpls[::wsplotstep]  if t0<pl["t"]<t1]  for Wpls in self.Wplslist]
        sendactivity(contours=conts, materialnumber=1)
        sendactivity(points=[cont[0]  for cont in conts  if cont])



# to plot pitch against windspeed:
"""
import sys
sys.path.append("/home/goatchurch/datalogging/arduinosketchbook/pyscripts")
from flightlog import RL
from flightlog.utils import GetLatestSDfile, TrapeziumAvg, TSampleRateG, SimpleLinearRegression
from flightlog.utils import TimeSampleRate, convexp2, varexp2, UnitSphereProj, UnitSpherePlot, convexpbiway
from flightlog.alltemperatures import CalcAirDensity

from basicgeo import P3, OctahedronAngle

fname = GetLatestSDfile("OOH")
rl = RL(fname, sendactivity)
wv = rl.postrack.CalcAverageWind(sendactivity)
wv = P3(wv[0], wv[1], 0)
g = rl.postrack.GetPath3()[0]
wg = [(t, (p - wv*(t*0.001*0.9)))  for t, p in g]
sendactivity("contours", contours=[[p*rl.gfac  for t, p in wg]], materialnumber=3)

tvs = [(v[0], -v[1].VecDots()[0])  for v in rl.orientdata.qvaccs]
dvecs = [ ]
i = 0
for t, p in wg[::10]:
    while i < len(tvs)-1 and tvs[i][0] < t:
        i += 1
    dvecs.append((p*rl.gfac, p*rl.gfac + tvs[i][1]*1.0))
sendactivity("contours", contours=dvecs, materialnumber=0)

from math import atan2, degrees, sqrt
tvs = [(v[0], -v[1].VecDots()[0])  for v in rl.orientdata.qvaccs]
tda = [(t, degrees(atan2(v[2], sqrt(v[0]**2+v[1]**2))))  for t, v in tvs]
sendactivity("contours", contours=[[(t*rl.tfac, d*0.1)  for t, d in tda]])
rl.plot("wind")
rl.plot("alt")

wcont = [(pl["t"]*rl.tfac, 8000/pl["w"])  for pl in rl.windspeed.Wplslist[0]]
self.sendactivity("contours", contours=[wconts], materialnumber=1)


t0, t1 = rl.geoframe.flightt0, rl.geoframe.flightt1
t0, t1 = 43/rl.tfac, 143/rl.tfac
from flightlog.utils import TimeSampleRate, convexp2, varexp2, TrapeziumAvg, SimpleLinearRegression


sendactivity("clearallcontours")
tstart, tstep = t0, 500
Wtrap = TrapeziumAvg(tstart, tstep, [ (pl["t"], 8000/pl["w"])  for pl in rl.windspeed.WplsL  if t0 < pl["t"] < t1])
sendactivity("contours", contours=[[((i*tstep+tstart)*rl.tfac, v)  for i, v in enumerate(Wtrap)]], materialnumber=3)
Atrap = TrapeziumAvg(tstart, tstep, [ (t, d)  for t, d in tda  if t0 < t < t1])
sendactivity("contours", contours=[[((i*tstep+tstart)*rl.tfac, (v-32)*0.02)  for i, v in enumerate(Atrap)]], materialnumber=1)


sendactivity("clearallpoints")
for i in range(-5, 10):
    pts = [(a, v)  for a, v in zip(Atrap[10:], Wtrap[i+10:])]
    print(i, SimpleLinearRegressionR(pts))
    sendactivity("points", points=[((a-32)*0.02+i*3, v)  for a, v in pts])
"""





# to plot above average windspeeds on the GPS track
"""
tvs = [(pl["t"], 8000/pl["w"])  for pl in rl.windspeed.WplsL]
avgwind = sum(w  for t, w in tvs)/len(cont)
tranges = [ ]
bopen = False
for t, w in tvs:
    if w > avgwind:
        if not bopen:
            tranges.append(t)
            tranges.append(t)
            bopen = True
        tranges[-1] = t
    else:
        bopen = False
len(tranges)
        
def trcontains(t, tranges):
    for i in range(1, len(tranges), 2):
        if tranges[i-1] > t:
            break
        if tranges[i] >= t:
            return True
    return False

fac = 0.1
sendactivity("clearallpoints")
sendactivity("points", points=[(p.x*fac, p.y*fac, p.z*fac)  for t, p in rl.postrack.GetPath3()[0]  if trcontains(t, tranges)], materialnumber=3)

"""

# junk
"""
import sys
sys.path.append("/home/goatchurch/datalogging/arduinosketchbook/pyscripts")
from flightlog import RL
from flightlog.utils import GetLatestSDfile, TSampleRateG
from flightlog.utils import TimeSampleRate, convexp2, varexp2, UnitSphereProj, UnitSpherePlot, convexpbiway

from basicgeo import P3, OctahedronAngle

fname = GetLatestSDfile()
#fname = "/home/goatchurch/datalogging/templogs/022.TXT"
fname = "/home/goatchurch/datalogging/permlogs/2014-03-19-blorenge.TXT"

rl = RL(fname, sendactivity)

gcont = rl.postrack.GetPath3()[0]
#sendactivity("contours", contours=[[(v[0]*0.1, v[1]*0.1, v[2]*0.1)  for t, v in gcont  if t0<t<t1]], materialnumber=1)

sendactivity("clearallcontours")
sendactivity("clearallpoints")
tfac = 1/60000
t0, t1 = 40/tfac, 126/tfac
tvs = [ (pl["t"], pl["p"])  for pl in rl.baros.Bpls ]
sendactivity("contours", contours=[[(t*tfac,(v-99000)*0.01)  for t, v in tvs  if t0<t<t1]])
stvs = convexpbiway(tvs, 0.05)
sendactivity("contours", contours=[[(t*tfac,(v-99000)*0.01)  for t, v in stvs  if t0<t<t1]], materialnumber=1)

sendactivity("contours", contours=[[(pl0["t"]*tfac,0), (pl0["t"]*tfac,-50)]  for pl0, pl1 in zip(rl.postrack.Vplslist[0],rl.postrack.Vplslist[0][1:])   if not (90*100<pl0["d"]<270*100) and not (90*100<pl1["d"]<270*100) and (90*100>pl0["d"]) != (90*100>pl1["d"]) ], materialnumber=2)


lasttm = 0
def pyclick(s, v):
    global lasttm
    tgap = 15000
    tm = s[0]/tfac
    print("seconds diff", (tm - lasttm)*0.001)
    lasttm = tm
    t0, t1 = tm-tgap, tm+tgap
    sendactivity("contours", contours=[[(t*tfac,(v-99000)*0.01)  for t, v in tvs  if t0<t<t1]], materialnumber=3)
    sendactivity("contours", contours=[[(v[0]*0.1, v[1]*0.1, v[2]*0.1)  for t, v in gcont  if t0<t<t1]], materialnumber=1)
    sendactivity("points", points=[min((abs(t-tm), (v[0]*0.1, v[1]*0.1, v[2]*0.1))  for t, v in gcont  if t0<t<t1)[1]], materialnumber=1)

Wpls = [ ]    
prevWplstack = [ ] 
for pl in rl.plines:
    if pl["C"] == "W":
        if prevWplstack:
            pl["m"] = pl["n"] - prevWplstack[0]["n"]
            pl["s"] = pl["t"] - prevWplstack[0]["t"]
            Wpls.append(pl)
        prevWplstack.append(pl)
        while len(prevWplstack) > 60:
            prevWplstack.pop(0)
t0, t1 = 105/tfac, 136/tfac
conts = [ [ ] ]
for pl in Wpls:
    if t0<pl["t"]<t1:
        #if pl["m"] != 1 and conts[-1]:
        #    conts.append([])
        if pl["s"] != 0:
            conts[-1].append(((pl["t"]-pl["s"]/2)*tfac, pl["s"]/pl["m"]*0.2-38))
sendactivity("contours", contours=conts, materialnumber=1)

max((pl["s"], i)  for i, pl in enumerate(Wpls)  if t0<pl["t"]<t1)
for i in range(745002-50,745002+60):
    print(i, Wpls[i])

cont = [(pl["t"]*tfac, pl["w"]*0.0005-38)  for pl in Wpls  if t0<pl["t"]<t1]
sendactivity("contours", contours=[cont], materialnumber=1)
scont = convexpbiway(cont, 0.05)
sendactivity("contours", contours=[scont], materialnumber=0)

    


prevWplstack[-1]["t"]-prevWplstack[0]["t"]

scont = convexpbiway(conts[0], 0.2)
sendactivity("contours", contours=[scont], materialnumber=3)

Wpls[100000]



def pyclick(s, v):
    tm = s[0]/tfac
    sendactivity("contours", contours=[[(tm*tfac,0), (tm*tfac,-50)]], materialnumber=3)


print([pl  for pl in Wpls  if pl["s"] == 0])

ss = [pl["s"]  for pl in Wpls]
sum(ss)/len(ss)
for ms in range(2,10):
    print(ms, sum(1  for s in ss  if s < ms)/len(ss))


    
def slopes(tvs, dt):
    tfs = [ ]
    for i in range(1, len(tvs) - 1):
        t = tvs[i][0]
        i0 = i
        while i0 > 0 and t - tvs[i0][0] < dt:
            i0 -= 1
        i1 = i
        while i1 < len(tvs)-1 and tvs[i1][0] - t < dt:
            i1 += 1
        d = (tvs[i1][1] - tvs[i0][1])/(tvs[i1][0] - tvs[i0][0])
        d0 = (tvs[i][1] - tvs[i0][1])/(t - tvs[i0][0])
        d1 = (tvs[i1][1] - tvs[i][1])/(tvs[i1][0] - t)
        a = (d1 - d0)/(tvs[i1][0]-tvs[i0][0])*2
        tfs.append((tvs[i][0], (d, a)))
    return tfs

sstvs = slopes(stvs, 200)
sendactivity("contours", contours=[[(t*tfac,v[0]*100-30)  for t, v in sstvs  if t0<t<t1]], materialnumber=3)
    



alltemps = rl.alltemps
baros = rl.baros

t0, t1 = 40/tfac, 126/tfac

tfac = 1/60000
ttvs = [(pl["t"], pl["c"])  for pl in rl.alltemps.Dplslist[1]]
sendactivity("contours", contours=[[(t*tfac,v-40)  for t, v in ttvs  if t0<t<t1]])
sttvs = convexpbiway(tvs, 0.1)
sendactivity("contours", contours=[[(t*tfac,v)  for t, v in stvs  if t0<t<t1]], materialnumber=1)



"""
