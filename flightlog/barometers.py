import math, scipy.optimize
from flightlog.utils import TimeSampleRate

class BaroData:
    def __init__(self, plines):
        self.Bpls = []
        self.Opls = []
        self.Fpls = []   # bluefly barometers
        self.initmainbaro(plines)
        #self.initauxiliarybaro(plines)
        self.initblueflybaro(plines)
        
        barovalues = [ pl["p"]  for pl in self.Bpls ] + [ pl["p"]  for pl in self.Opls ]
        if not barovalues:
            barovalues = [ pl["p"]  for pl in self.Fpls ] + [ pl["p"]  for pl in self.Opls ]
        if not barovalues:
            return
        barovalues.sort()
        ib = len(barovalues)//10
        self.plo, self.phi = barovalues[ib], barovalues[-ib]
        print("baro range", self.plo, self.phi)
        
    def initmainbaro(self, plines):
        self.Bpls = [ pl  for pl in plines  if pl["C"] == 'B' ]
        if not self.Bpls:
            return
        prevseq = 999
        prevt = 0
        seqskipsn = { }
        tdsum, tdsqsum, n = 0.0, 0.0, 0
        for pl in self.Bpls:
            td = pl["t"] - prevt
            if td < 100 and pl["s"] == ((prevseq + 1) & 0x3F):
                tdsum += td
                tdsqsum += td*td
                n += 1
                pl["e"] = 1
            elif prevseq != 999:
                e = int((pl["t"] - prevt) / 21.87 + 0.5)  # approx how many skipped steps
                if e not in seqskipsn:
                    seqskipsn[e] = 0
                seqskipsn[e] += 1
                pl["e"] = e
            else:
                pl["e"] = 0
            prevt = pl["t"]
            prevseq = pl["s"]
        self.prlo, self.prhi = min(pl["p"]  for pl in self.Bpls), max(pl["p"]  for pl in self.Bpls)

        print("\nBarometer:")
        print("    skips %f %%" % (100*sum(seqskipsn.values())/(len(self.Bpls)-1)))
        print("    skip hist", seqskipsn)
        print("    time sample rate %f ms at sd %f ms" % (tdsum/n, math.sqrt(tdsqsum*n - tdsum*tdsum)/n))
        print("    Prlo=%f  Prhi=%f" % (self.prlo, self.prhi))
        
    def initblueflybaro(self, plines):
        self.Fpls = [ pl  for pl in plines  if pl["C"] == 'F' ]
        if not self.Fpls:
            return
            
        maxdf = 1000
        badqsi = [ ]
        Fpls = self.Fpls
        for i in range(1, len(Fpls)):
            if abs(Fpls[i]["p"] - Fpls[i-1]["p"]) > maxdf:
                badqsi.append(i)
        badqsi2 = [badqsi[j-1]  for j in range(1, len(badqsi))  if badqsi[j-1]+1 == badqsi[j]]
        #for i in badqsi2:
        #    print(Fpls[i-1]["p"], Fpls[i]["p"], Fpls[i+1]["p"])
        if badqsi2:
            print("Thinning bad Fpls readings", badqsi2)
        for i in reversed(badqsi2):
            del Fpls[i]
            
        prevseq = 999
        prevt = 0
        estskips = 0
        tdsum, tdsqsum, n = 0.0, 0.0, 0
        for pl in self.Fpls:
            td = pl["t"] - prevt
            if td < 100:
                tdsum += td
                tdsqsum += td*td
                n += 1
                #pl["e"] = 1   # for what?
            elif prevseq != 999:
                estskips += int((pl["t"] - prevt) / 20.0 + 0.5)  # approx how many skipped steps
            prevt = pl["t"]
        self.prlo, self.prhi = min(pl["p"]  for pl in self.Fpls), max(pl["p"]  for pl in self.Fpls)
        print("BlueflyBarometer:", "    skips %.2f %%" % ((100*estskips)/(estskips + len(self.Fpls))), "    time sample rate %f ms at sd %f ms" % (tdsum/n, math.sqrt(tdsqsum*n - tdsum*tdsum)/n), "    Prlo=%f  Prhi=%f" % (self.prlo, self.prhi))


    def initauxiliarybaro(self, plines):
        print("\nAux Barometer:")
        Npls = [ pl  for pl in plines  if pl["C"] == 'N' ]
        if not Npls:
            print("No auxiliary barometer")
            return
        Npl = Npls[0]
        c1,c2,c3,c4,c5,c6,c7,c8 = Npl["c"]&0xFFFF, Npl["d"]&0xFFFF, Npl["e"]&0xFFFF, Npl["f"]&0xFFFF, \
                                  Npl["g"], Npl["h"], Npl["i"], Npl["j"]

        def T5403BarometerGetTemperature(temperature_raw):
            return (((((c1 * temperature_raw) >> 8) + (c2 << 6)) * 100) >> 16)*0.01

        def T5403BarometerGetPressure(temperature_raw, pressure_raw):
            s = ((((c5 * temperature_raw) >> 15) * temperature_raw) >> 19) + c3 + ((c4 * temperature_raw) >> 17) 
            o = ((((c8 * temperature_raw) >> 15) * temperature_raw) >> 4) + ((c7 * temperature_raw) >> 3) + (c6 * 0x4000)
            return (s * pressure_raw + o) >> 14

        def ApplyT5403consts(pl):
            pl["p"] = T5403BarometerGetPressure(pl["r"], pl["q"]&0xFFFF) 
            pl["c"] = T5403BarometerGetTemperature(pl["r"])
            return pl

        self.Opls = [ ApplyT5403consts(pl)  for pl in plines  if pl["C"] == 'O' ]
        print("    time sample rate %f ms at sd %f ms" % TimeSampleRate(self.Opls))
        print("    Prlo=%f  Prhi=%f" % (min(pl["p"]  for pl in self.Opls), max(pl["p"]  for pl in self.Opls)))

    def PlotAll(self, sendactivity, tfac=1/60000):
        pfac = 1/1000
        poff = math.floor(self.plo*pfac-1)/pfac
        pyoff = 0
        
        refplotsize = 10000
        plotstep = max(1, int(len(self.Fpls)/refplotsize))
        print("plotstep", plotstep, "on", len(self.Fpls), "baro points")
        
        #sendactivity("contours", contours=[[(pl["t"]*tfac, (pl["p"]-poff)*pfac+pyoff)  for pl in self.Opls]])
        sendactivity("contours", contours=[[(pl["t"]*tfac, (pl["p"]-poff)*pfac+pyoff)  for pl in self.Fpls[::plotstep]]])
        #sendactivity("contours", contours=[[(pl["t"]*tfac, (pl["p"]-poff)*pfac+pyoff)  for pl in self.Bpls]], materialnumber=1)
        #sendactivity("points", points=[(pl["t"]*tfac, (pl["p"]-poff)*pfac+pyoff)  for pl in self.Bpls], materialnumber=1)
        iceil = int(math.ceil((self.phi-poff)*pfac)+2)
        Bpls = self.Bpls or self.Fpls 
        jt0, jt1 = math.floor(Bpls[0]["t"]*tfac/5)*5, math.ceil(Bpls[-1]["t"]*tfac/5)*5
        sendactivity("contours", contours=[[(jt0, i+pyoff), (jt1, i+pyoff)]  for i in range(iceil)], materialnumber=2)
        sendactivity("contours", contours=[[(i, 0+pyoff), (i, iceil-1+pyoff)]  for i in range(jt0, jt1, 5)], materialnumber=2)
        print("Baro steps of %.0fPa from low %.0f and time increments of %0.1f seconds" % (1/pfac, poff, 5/(tfac*1000)))

    
