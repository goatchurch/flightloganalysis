import math
from math import sin, cos, radians, sqrt, atan2
from flightlog.utils import TimeSampleRate, convexpbiway, StrictTimeSampleRate, SimpleLinearRegression
from flightlog.utils import MeanSD, TSampleRateG, differentiatetvs
from basicgeo import P3, P2, Quat
from flightlog.utils import ziptvs, ziptvsG
import random

def FindRecordSpacingBetweenGPSandvelocity(rl):
    Qpl = None
    tVQ = [ ]
    for pl in rl.plines:
        if pl["C"] == "Q":
            Qpl = pl
        elif pl["C"] == "V":
            if Qpl is not None:
                tVQ.append(pl["t"] - Qpl["t"])
            Qpl = None
    print("Average time between GPS position Q record and subsequent velocity V record", sum(tVQ)/len(tVQ), "milliseconds")


def FindGeometricSpacingBetweenGPSandvelocity(rl):
    t0, t1 = rl.postrack.t0, rl.postrack.t1
    gcont = [(t, p)  for t, p in rl.postrack.GetPath3()[0]  if t0<t<t1]
    vcont = [(t, v)  for t, v in rl.postrack.GetVelPath3()[0]  if t0<t<t1]
    dvcont = differentiatetvs(gcont, 3)
    
    dts = [(i-10)*5-250  for i in range(20)]   # it's in this range of about -200ms
    dtspacing = [ ]
    for dt in dts:
        k1 = ziptvs(dvcont, [(t+dt, v)  for t, v in vcont])
        dd = sum((P2.ConvertLZ(dv) - v).Lensq()  for t, dv, v in k1)/len(k1)
        print(dt, dd)
        dtspacing.append((dd, dt))
    dtspacing.sort()
    ddU = dtspacing[0][0]*1.02   # 2% higher
    dtsU = [ dt  for (dd, dt) in dtspacing  if dd<=ddU ]
    print("Velocity time shift is between %.3f and %.3f milliseconds to fit GPS position derived velocity" % (min(dtsU), max(dtsU)))




class AccelerationDerivations:
    def __init__(self, rl):
        self.rl = rl
        self.t0, self.t1 = rl.postrack.t0, rl.postrack.t1
        self.gtimeshift = 0
        self.gcont, self.vcont, self.acont, self.dgcont, self.dvcont, self.ddgcont = None, None, None, None, None, None
        self.dis = 3
        self.refplotsize = 5000
        self.reftotalsize = 5000
        self.plotstep = 3
        self.refstep = 17
        self.uvecfac = 0.3  # unit fac
        self.vvecfac = 0.1
        self.avecfac = 0.1
        
        
    def BuildLists(self, bls):
        if "gcont" in bls:
            self.gcont = [(t, p)  for t, p in self.rl.postrack.GetPath3()[0]]
        if "vcont" in bls:
            self.vcont = [(t, v)  for t, v in self.rl.postrack.GetVelPath3()[0]]
        if "acont" in bls:
            print("building acont")
            self.acont = [(t, q.VecDots0()*a[0] + q.VecDots1()*a[1] + q.VecDots2()*a[2])  for t, q, a in self.rl.orientdata.qvaccs]
        if "dgcont" in bls:
            print("building dgcont")
            self.dgcont = differentiatetvs(self.gcont, self.dis)
        if "dvcont" in bls:
            print("building dvcont")
            self.dvcont = differentiatetvs(self.vcont, self.dis)
        if "ddgcont" in bls:
           print("building ddgcont")
           self.ddgcont = differentiatetvs(self.dgcont, self.dis)
                  
    def PlotList(self, ls, plotstep=3):
        if not self.gcont:  
            self.BuildLists(["gcont"])
        gfac = self.rl.gfac
        t0, t1 = self.t0, self.t1
        
        if ls[0] != "R":
            dcont = self.gcont
            gt0 = min(i  for i, (t, p) in enumerate(self.gcont)  if t0<t<t1)
            gt1 = max(i  for i, (t, p) in enumerate(self.gcont)  if t0<t<t1)
            plotstep = max(1, int((gt1 - gt0)/self.refplotsize))
            print("plotstep", plotstep, "on", gt1-gt0, "acc points")
            lgcont = [(t+self.gtimeshift, p)  for t, p in self.gcont[gt0:gt1:plotstep]]
        else:
            if not self.acont and "Racont" == ls:  self.BuildLists(["acont"])
            at0 = min(i  for i, (t, q, a) in enumerate(self.rl.orientdata.qvaccs)  if t0<t<t1)
            at1 = max(i  for i, (t, q, a) in enumerate(self.rl.orientdata.qvaccs)  if t0<t<t1)
            aplotstep = max(1, int((at1 - at0)/self.refplotsize))
            print("plotstep", aplotstep, "on", at1-at0, "accelerometer points")
            assert not self.acont or len(self.acont) == len(self.rl.orientdata.qvaccs)
            
        if "gcont" == ls:
            self.rl.sendactivity(contours=[[p*gfac  for t, p in lgcont]])
        elif "vcont" == ls:
            if not self.vcont:  self.BuildLists(["vcont"])
            kv = ziptvsG(lgcont, self.vcont)
            self.rl.sendactivity(contours=[(p*gfac, p*gfac+P3.ConvertGZ(v,0)*self.vvecfac)  for t, p, v in kv], materialnumber=1)
        elif "dgcont" == ls:
            if not self.dgcont:  self.BuildLists(["dgcont"])
            kdg = ziptvsG(lgcont, self.dgcont)
            self.rl.sendactivity(contours=[(p*gfac, p*gfac+dg*self.vvecfac)  for t, p, dg in kdg], materialnumber=3)
        elif "acont" == ls:
            if not self.acont:  self.BuildLists(["acont"])
            ka = ziptvsG(lgcont, self.acont)
            self.rl.sendactivity(contours=[(p*gfac, p*gfac+a*self.avecfac)  for t, p, a in ka], materialnumber=1)
        elif "dvcont" == ls:
            if not self.dvcont:  self.BuildLists(["dvcont", self.vcont or "vcont"])
            kdv = ziptvsG(lgcont, self.dvcont)
            self.rl.sendactivity(contours=[(p*gfac, p*gfac+P3.ConvertGZ(dv,0)*self.avecfac)  for t, p, dv in kdv], materialnumber=2)
        elif "ddgcont" == ls:
            if not self.ddgcont:  self.BuildLists(["ddgcont", self.dgcont or "dgcont"])
            kddg = ziptvsG(lgcont, self.ddgcont)
            self.rl.sendactivity(contours=[(p*gfac, p*gfac+ddg*self.avecfac)  for t, p, ddg in kddg], materialnumber=3)

        # these are about relating the 100Hz accelerometer to the 10Hz gps, rather than the other way round
        elif "Racont" == ls:
            if not self.acont:  self.BuildLists(["acont"])
            lacont = [(t-self.gtimeshift, a)  for t, a in self.acont[at0:at1:aplotstep]]
            ka = ziptvs(lacont, self.gcont)
            self.rl.sendactivity(contours=[(p*gfac, p*gfac+a*self.avecfac)  for t, a, p in ka], materialnumber=3)
        
        elif ls[:2]+ls[3:] == "Rqcont":  # Rq1cont
            assert not self.acont or len(self.rl.orientdata.qvaccs) == len(self.acont)
            qi = int(ls[2])
            lqcont = [(t-self.gtimeshift, (self.rl.orientdata.uprightvert*q).VecDots()[qi])  for t, q, a in self.rl.orientdata.qvaccs[at0:at1:aplotstep]]
            kq = ziptvs(lqcont, self.gcont)
            self.rl.sendactivity(contours=[(p*gfac, p*gfac+q*self.uvecfac)  for t, q, p in kq], materialnumber=qi+1)
            
        elif ls[:3]+ls[5:] == "Rtqcont":  # Rtq22cont
            assert not self.acont or len(self.rl.orientdata.qvaccs) == len(self.acont)
            qi = int(ls[3])
            qd = int(ls[4])
            lqdcont = [((t-self.gtimeshift)*self.rl.tfac, (self.rl.orientdata.uprightvert*q).VecDots()[qi][qd])  for t, q, a in self.rl.orientdata.qvaccs[at0:at1:aplotstep]]
            self.rl.sendactivity(contours=[lqdcont], materialnumber=qi+1)
            print("point0", lqdcont[0], " set0 by rl.accelerationderivations.gtimeshift=",lqdcont[0][0]/self.rl.tfac)
            
        else:
            print("unknown diff command", [ls])
            

    def Plotshiftaxes(self):
        self.rl.sendactivity(contours=[[(i*0.1,0),(i*0.1,50)]  for i in range(-10,11)], materialnumber=1)
        self.rl.sendactivity(contours=[[(-10,0),(10,0)], [(0,0), (0,50)]])
        
    def DGcontRefStep(self, dcont):
        t0, t1 = self.t0, self.t1
        dgt0 = min(i  for i, (t, p) in enumerate(dcont)  if t0<t<t1)
        dgt1 = max(i  for i, (t, p) in enumerate(dcont)  if t0<t<t1)
        refstep = max(1, int((dgt1 - dgt0)/self.reftotalsize))
        print("refstep", refstep, "on", dgt1-dgt0, "points")
        return dcont[dgt0:dgt1:refstep]

    def Plotdgvshift(self):
        dts = list(range(-5000, 9001, 60))
        random.shuffle(dts)
        self.BuildLists([self.gcont or "gcont", self.dgcont or "dgcont", self.vcont or "vcont"])
        self.contdtdgv = [ ]
        dcontrefstep = self.DGcontRefStep(self.dgcont)
        for dt in dts:
            k1 = ziptvsG(dcontrefstep, [(t+dt, v)  for t, v in self.vcont])
            v = sum((P2.ConvertLZ(dv) - v).Lensq()  for t, dv, v in k1)/len(k1)
            self.contdtdgv.append((dt, v))
            self.rl.sendactivity(points=[(dt*0.001, v)], materialnumber=0)
        self.contdtdgv.sort()
        self.rl.sendactivity(contours=[[(dt*0.001, v)  for dt, v in self.contdtdgv]], materialnumber=0)
        
    def Plotdvashift(self):
        dts = list(range(-5000, 9001, 60))
        random.shuffle(dts)
        self.BuildLists([self.vcont or "vcont", self.dvcont or "dvcont", self.acont or "acont"])
        self.contdtdva = [ ]
        dcontrefstep = self.DGcontRefStep(self.dvcont)
        for dt in dts:
            k1 = ziptvsG(dcontrefstep, [(t+dt, v)  for t, v in self.acont])
            v = sum((dv - P2.ConvertLZ(a)).Lensq()  for t, dv, a in k1)/len(k1)
            self.contdtdva.append((dt, v))
            self.rl.sendactivity(points=[(dt*0.001, v)])
        self.contdtdva.sort()
        self.rl.sendactivity(contours=[[(dt*0.001, v)  for dt, v in self.contdtdva]], materialnumber=1)

    def Plotddgashift(self):
        dts = list(range(-5000, 9001, 60))
        random.shuffle(dts)
        self.BuildLists([self.gcont or "gcont", self.dgcont or "dgcont", self.ddgcont or "ddgcont", self.acont or "acont"])
        self.contdtddga = [ ]
        dcontrefstep = self.DGcontRefStep(self.ddgcont)
        for dt in dts:
            k1 = ziptvsG(dcontrefstep, [(t+dt, v)  for t, v in self.acont])
            v = sum((ddg - a).Lensq()  for t, ddg, a in k1)/len(k1)
            self.contdtddga.append((dt, v))
            self.rl.sendactivity(points=[(dt*0.001, v)], materialnumber=2)
        self.contdtddga.sort()
        self.rl.sendactivity(contours=[[(dt*0.001, v)  for dt, v in self.contdtddga]], materialnumber=3)


