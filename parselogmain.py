import sys
sys.path.append("/home/goatchurch/datalogging/arduinosketchbook/pyscripts")
from flightlog import RL
from flightlog.utils import GetLatestSDfile, TrapeziumAvg, TSampleRateG, SimpleLinearRegression
from flightlog.utils import TimeSampleRate, convexp2, varexp2, UnitSphereProj, UnitSpherePlot, convexpbiway
from flightlog.alltemperatures import CalcAirDensity

from basicgeo import P3, OctahedronAngle

fname = GetLatestSDfile("OOH")
#fname = "/home/goatchurch/datalogging/templogs/022.TXT"
fname = "/home/goatchurch/datalogging/permlogs/2015-05-04-bache.TXT"
fname = "/home/goatchurch/datalogging/permlogs/2015-10-11-mamtor.TXT"

rl = RL(fname, sendactivity)

sendactivity("clearallpoints")
rl.Tgpsfilterforpeaks = 0.05
rl.plot("HTgpsside")
rl.plot("gps")

rl.plot("tmps")
rl.plot("baro")
rl.alltemps.Apls
rl.plot("wind")



rl = RL(fname, sendactivity)
rl.geoframe.FindLapseRates(sendactivity)
self = rl.geoframe
t0, t1 = self.flightt0, self.flightt1
sendactivity("contours", contours=[[(p[0]*0.1, p[1]*0.1, p[2]*0.1)  for t, p in self.Dgcont  if t0<t<t1]], materialnumber=1)
sendactivity("contours", contours=[[(p[0]*0.1, p[1]*0.1, p[2]*0.1)  for t, p in self.DgcontB  if t0<t<t1]], materialnumber=3)





##########################

import sys
sys.path.append("/home/goatchurch/datalogging/arduinosketchbook/pyscripts")
from flightlog import RL
from flightlog.utils import GetLatestSDfile
from flightlog.utils import TimeSampleRate, convexp2, varexp2, UnitSphereProj, UnitSpherePlot, convexpbiway

from basicgeo import P3, OctahedronAngle

fname = GetLatestSDfile()
#fname = "/home/goatchurch/datalogging/templogs/022.TXT"
fname = "/home/goatchurch/datalogging/permlogs/2015-03-19-blorenge.TXT"

rl = RL(fname, sendactivity)

gcont = rl.postrack.GetPath3()[0]
sendactivity("contours", contours=[[(v[0]*0.1, v[1]*0.1, v[2]*0.1)  for t, v in gcont  if t0<t<t1]], materialnumber=1)

sendactivity("clearallcontours")
sendactivity("clearallpoints")
tfac = 1/60000
t0, t1 = 40/tfac, 126/tfac
tvs = [ (pl["t"], pl["p"])  for pl in rl.baros.Bpls ]
sendactivity("contours", contours=[[(t*tfac,(v-99000)*0.01)  for t, v in tvs  if t0<t<t1]])
stvs = convexpbiway(tvs, 0.05)
sendactivity("contours", contours=[[(t*tfac,(v-99000)*0.01)  for t, v in stvs  if t0<t<t1]], materialnumber=1)

sendactivity("contours", contours=[[(pl0["t"]*tfac,0), (pl0["t"]*tfac,-50)]  for pl0, pl1 in zip(rl.postrack.Vplslist[0],rl.postrack.Vplslist[0][1:])   if not (90*100<pl0["d"]<270*100) and not (90*100<pl1["d"]<270*100) and (90*100>pl0["d"]) != (90*100>pl1["d"]) ], materialnumber=2)


lasttm = 0
def pyclick(s, v):
    global lasttm
    tgap = 15000
    tm = s[0]/tfac
    print("seconds diff", (tm - lasttm)*0.001)
    lasttm = tm
    t0, t1 = tm-tgap, tm+tgap
    sendactivity("contours", contours=[[(t*tfac,(v-99000)*0.01)  for t, v in tvs  if t0<t<t1]], materialnumber=3)
    sendactivity("contours", contours=[[(v[0]*0.1, v[1]*0.1, v[2]*0.1)  for t, v in gcont  if t0<t<t1]], materialnumber=1)
    sendactivity("points", points=[min((abs(t-tm), (v[0]*0.1, v[1]*0.1, v[2]*0.1))  for t, v in gcont  if t0<t<t1)[1]], materialnumber=1)

Wpls = [ ]    
prevWpl = None
for pl in rl.plines:
    if pl["C"] == "W":
        if prevWpl:
            pl["m"] = pl["n"] - prevWpl["n"]
            pl["s"] = pl["t"] - prevWpl["t"]
            Wpls.append(pl)
        prevWpl = pl

conts = [ [ ] ]
for pl in Wpls:
    if pl["n"] == 1 and conts[-1]:
        conts.append([])
    conts[-1].append((pl["t"]*tfac, 8000/pl["s"]))
    
print([pl  for pl in Wpls  if pl["s"] == 0])


    
def slopes(tvs, dt):
    tfs = [ ]
    for i in range(1, len(tvs) - 1):
        t = tvs[i][0]
        i0 = i
        while i0 > 0 and t - tvs[i0][0] < dt:
            i0 -= 1
        i1 = i
        while i1 < len(tvs)-1 and tvs[i1][0] - t < dt:
            i1 += 1
        d = (tvs[i1][1] - tvs[i0][1])/(tvs[i1][0] - tvs[i0][0])
        d0 = (tvs[i][1] - tvs[i0][1])/(t - tvs[i0][0])
        d1 = (tvs[i1][1] - tvs[i][1])/(tvs[i1][0] - t)
        a = (d1 - d0)/(tvs[i1][0]-tvs[i0][0])*2
        tfs.append((tvs[i][0], (d, a)))
    return tfs

sstvs = slopes(stvs, 200)
sendactivity("contours", contours=[[(t*tfac,v[0]*100-30)  for t, v in sstvs  if t0<t<t1]], materialnumber=3)
    



alltemps = rl.alltemps
baros = rl.baros

t0, t1 = 40/tfac, 126/tfac

tfac = 1/60000
ttvs = [(pl["t"], pl["c"])  for pl in rl.alltemps.Dplslist[1]]
sendactivity("contours", contours=[[(t*tfac,v-40)  for t, v in ttvs  if t0<t<t1]])
sttvs = convexpbiway(tvs, 0.1)
sendactivity("contours", contours=[[(t*tfac,v)  for t, v in stvs  if t0<t<t1]], materialnumber=1)








#################################

import sys
sys.path.append("/home/goatchurch/datalogging/arduinosketchbook/pyscripts")
from flightlog import RL
from flightlog.utils import GetLatestSDfile
from flightlog.utils import TimeSampleRate, convexp2, varexp2, UnitSphereProj
from basicgeo import P3, OctahedronAngle

fname = GetLatestSDfile()
#fname = "/home/goatchurch/datalogging/templogs/022.TXT"
#fname = "/home/goatchurch/datalogging/templogs/-old1/231.TXT"
rl = RL(fname, sendactivity)

sendactivity("contours", contours=[[v  for t, v in rl.compassdata.GetTPath()]], materialnumber=3)
rl.compassdata.ExtractCalibrationPoints(sendactivity)

#rl.gyrodata.ExtractStationaryPoints(sendactivity, 650)
rl.gyrodata.ExtractSlowMotionPoints(sendactivity, oaifac=15)

rl.gyrodata.PlotVarL(sendactivity)
rl.gyrodata.PlotGforce(sendactivity)

rl.geoframe.FindCompassRotToConstant(sendactivity)


####################

import sys
sys.path.append("/home/goatchurch/datalogging/arduinosketchbook/pyscripts")
from flightlog import RL
from flightlog.utils import GetLatestSDfile
from basicgeo import P3

from flightlog.utils import TimeSampleRate
from basicgeo import P3


fname = GetLatestSDfile()
#fname = "/home/goatchurch/datalogging/templogs/-old1/231.TXT"
rl = RL(fname, sendactivity)
sendactivity("contours", contours=[[v  for t, v in rl.compassdata.GetTPath()]], materialnumber=3)

#rl.plot("gps")

Gpls = [ pl  for pl in rl.plines  if pl["C"] == "G" ]
print("\nGyros:")
print("    time sample rate %f ms at sd %f ms" % TimeSampleRate(Gpls))
gv = [ (pl["t"]/1000, P3(pl["x"], pl["y"], pl["z"]).Len()/1000)  for pl in Gpls ]
sendactivity("contours", contours=[gv])

sendactivity("contours", contours=[[ (pl["t"]/1000, pl["z"]/1000)  for pl in Gpls ]], materialnumber=1)


import math
tvs = [(pl["t"], pl["z"])  for pl in Gpls ]

tdsum, tdsqsum, n = 0.0, 0.0, 0
tprev = tvs[0][0]
for t, v in tvs[1:]:
    td = t - tprev
    tdsum += td
    tdsqsum += td*td
    n += 1
    tprev = t

tavg, tsd = tdsum/n, math.sqrt(tdsqsum*n - tdsum*tdsum)/(n-1)

fac = 0.002
kexp = [ 1 ]
while kexp[-1] > 0.001:
    kexp.append(math.exp(-(len(kexp)*tavg*fac)**2))
vfac = 1/sum(kexp)
kexp = [ k*vfac  for k in kexp ]
print(kexp)

# must make this step back by the interval tavg
def kerconv(tvs, i):
    res = tvs[i]*kexp[0]
    for j in range(len(kexp)):
        if i+j < len(tvs):  
            res += tvs[i+j]*kexp[j]
        if i-j >= 0:  
            res += tvs[i-j]*kexp[j]
    return res

tvs = [(pl["t"]/1000, pl["z"]/1000)  for pl in Gpls]    
sendactivity("contours", contours=[(tvs[i][0], kerconv(tvs, i))  for i in range(len(tvs))], materialnumber=2)








#########################
import sys
sys.path.append("/home/goatchurch/datalogging/arduinosketchbook/pyscripts")
from flightlog import RL
from basicgeo import P3

fname = "/home/goatchurch/datalogging/templogs/-old1/231.TXT"
rl = RL(fname, sendactivity)
rl.plot("gps")

gc = rl.postrack.GetPath3()[0]

print(len(gc))
print(gc[0])
vel = [ ((gc[i][0]+gc[i-1][0])*0.5, (gc[i][1] - gc[i-1][1])*(1000/(gc[i][0] - gc[i-1][0])))  for i in range(1,len(gc)) ]
sendactivity("contours", contours=[[(t/60000, v.Len())  for t, v in vel]])
accel = [ ((vel[i][0]+vel[i-1][0])*0.5, (vel[i][1] - vel[i-1][1])*(1000/(vel[i][0] - vel[i-1][0])))  for i in range(1,len(vel)) ]
sendactivity("contours", contours=[[(t/60000, v.Len())  for t, v in accel]], materialnumber=1)


gyros = [(pl["t"], P3(pl["x"], pl["y"], pl["z"]))  for pl in rl.plines  if pl["C"] == "G"]
sendactivity("contours", contours=[[(t/60000, (v.Len()-18000)/1000)  for t, v in gyros]], materialnumber=3)

sendactivity("clearallpoints")
pcont = [ ]
ig = 0
for t, v in accel[10000:11000]:
    gacc = P3(0,0,0)
    gaccn = 0
    while ig < len(gyros) and gyros[ig][0] < t:
        gacc += gyros[ig][1]
        gaccn += 1
        ig += 1
    pcont.append((v.Len(), ((gacc*(1/(gaccn or 1))).Len()-18000)/1000))
sendactivity("points", points=pcont)
len(accel)

    


#########################



import re, time, math, sys, imp
sys.path.append("/home/goatchurch/datalogging/arduinosketchbook/pyscripts")
import parselog
imp.reload(parselog)

rl = parselog.RL("/home/goatchurch/datalogging/templogs/208.TXT", sendactivity)

rl.TrimStationarySections(5)
sendactivity("clearalltriangles")

cont = rl.gpstrace(0.1)
sendactivity("contours", contours=[cont[:len(cont)//2]])
sendactivity("contours", contours=[cont[len(cont)//2:]], materialnumber=2)

################

# plot small zone of 
sendactivity("clearallcontours")
sendactivity("contours", contours=[[(0,0), (1000,0)]], materialnumber=1)
sendactivity("contours", contours=[[(i*5,-10), (i*5,50)]  for i in range(0, 451)], materialnumber=1)

f = 60
cont = [(pl["t"]/60000*f, pl["b"]*0.1*0.1*0.1)  for pl in rl.plines  if pl["C"] == "G"]
#sendactivity("contours", contours=[cont], materialnumber=0)

rcont = convolveexp2(cont, 10.0)
sendactivity("contours", contours=[rcont], materialnumber=0)

cont = [(pl["t"]/60000*f, (pl["p"]-100000)/50+30)  for pl in rl.plines  if pl["C"] == "B"]
sendactivity("contours", contours=[cont], materialnumber=3)

cont = [(pl["t"]/60000*f, pl["v"]/360)  for pl in rl.plines  if pl["C"] == "V"]
sendactivity("contours", contours=[cont], materialnumber=1)

cont = [(pl["t"]/60000*f, 80000./max(1, pl["w"]))  for pl in rl.plines  if pl["C"] == "W"]
sendactivity("contours", contours=[cont], materialnumber=2)


################
# dallas temp readings
cont = [(pl["t"]/60000, pl["c"]/16)  for pl in rl.plines  if pl["C"] == "D" and pl["i"] == 1]
sendactivity("contours", contours=[cont])

# dallas temp readings group
print("Dallas count per device")
for i in range(3):
    print(i, sum(pl["i"] == i and 1 or 0  for pl in rl.plines  if pl["C"] == "D"))

for i in range(3):
    cont = [(pl["t"]/60000/20, pl["c"]/16)  for pl in rl.plines  if pl["C"] == "D" and pl["i"] == i]
    sendactivity("contours", contours=[cont], materialnumber=i)


# GPS velocity
cont = [(pl["t"]/60000, pl["v"]/16)  for pl in rl.plines  if pl["C"] == "V"]
sendactivity("contours", contours=[cont], materialnumber=2)

# metres per second
cont = [(pl["t"]/60000, pl["v"]/360)  for pl in rl.plines  if pl["C"] == "V"]
sendactivity("contours", contours=[cont], materialnumber=3)


# barometer
cont = [(pl["t"]/60000, (pl["p"]-100000)/100)  for pl in rl.plines  if pl["C"] == "B"]
sendactivity("contours", contours=[cont], materialnumber=3)

# TMP36 ADC
cont = [(pl["t"]/60000, (pl["v"]*1024/32767 - 500)/10)  for pl in rl.plines  if pl["C"] == "A"]
sendactivity("contours", contours=[cont], materialnumber=1)

# compass
cont = [(pl["t"]/60000, (pl["y"])/50)  for pl in rl.plines  if pl["C"] == "C"]
sendactivity("contours", contours=[cont], materialnumber=2)

# accelerometer
cont = [(pl["t"]/60000, (pl["a"])/2000-30)  for pl in rl.plines  if pl["C"] == "G"]
sendactivity("contours", contours=[cont], materialnumber=0)

##################################

# convolving
f=60
convolvfac = 1
cont = [(pl["t"]/60000*f, (pl["p"]-100000)/50+30)  for pl in rl.plines  if pl["C"] == "B"]
sendactivity("contours", contours=[cont], materialnumber=3)
rcont = parselog.convolveexp2(cont, convolvfac)
sendactivity("contours", contours=[rcont], materialnumber=0)

cont2 = [(pl["t"]/60000*f, (pl["a"]-rl.Cpl["Q"]["a"])*0.1*0.1)  for pl in rl.plines  if pl["C"] == "Q"]
sendactivity("contours", contours=[cont2], materialnumber=3)
rcont2 = parselog.convolveexp2(cont2, convolvfac)
sendactivity("contours", contours=[rcont2], materialnumber=0)

i = 0
pts = [ ]
for t, v in rcont2:
    while i < len(rcont) and rcont[i][0] < t:
        i += 1
    if i < len(rcont):
        pts.append((v, rcont[i][1]+10))
sendactivity("points", points=pts, materialnumber=0)


################################
# try to estimate the air density
convolvfac = 1
contB = [(pl["t"]/1000, pl["p"])  for pl in rl.plines  if pl["C"] == "B"]
rcontB = parselog.convolveexp2(contB, convolvfac)
    
contA = [(pl["t"]/1000, pl["a"]*0.1)  for pl in rl.plines  if pl["C"] == "Q"]
rcontA = parselog.convolveexp2(contA, convolvfac)

# trim middle section
# rcontA = rcontA[:10] + rcontA[-10:]

sx, sy, sxy, sx2, sy2, sn = 0, 0, 0, 0, 0, 0
i = 0
pts = [ ]
for t, vA in rcontA:
    while i < len(rcontB) and rcontB[i][0] < t:
        i += 1
    if i < len(rcontB):
        vB = rcontB[i][1]
        pts.append(((vA - 100)*0.1, (vB-100000)/50+30))
        sx += vA
        sy += vB
        sx2 += vA*vA
        sy2 += vB*vB
        sxy += vA*vB
        sn += 1
sendactivity("points", points=pts, materialnumber=1)
# solve y = mx + c
rxy = (sxy - sx*sy/sn) / math.sqrt((sx2 - sx*sx/sn)*(sy2 - sy*sy/sn))  # correlation
m = (sxy - sx*sy/sn) / (sx2 - sx*sx/sn)  # pascal change per metre
c = sy/sn - m*sx/sn   # assumed sea level pressure

sendactivity("contours", contours=[[((x - 100)*0.1, (m*x + c -100000)/50+30)  for x in [0, 500]]])

# current values of m is too low.  but maybe there is lower pressure due to flight and we should 
# consider just the start and end points

# pressure by flight air speed is reduced by (1/2 density v^2)
# http://en.wikipedia.org/wiki/Density_of_air

# density of air
# standard pressure: 101325 N/m^2
# density of air is 1.225 kg/m^3
# g = 9.8
# 1.225*9.8
# suggests density of this air is -m/9.8

# calculated density is pr/(R*T)
(sy/sn) / (287.058*(15+273.15))






sendactivity("clearallpoints")
