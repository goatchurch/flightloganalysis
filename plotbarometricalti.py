import re, time, math, sys, imp, scipy.optimize
sys.path.append("/home/goatchurch/datalogging/arduinosketchbook/pyscripts")
import parselog
imp.reload(parselog)

rl = parselog.RL("/home/goatchurch/datalogging/templogs/231.TXT", sendactivity)
rl.TrimStationarySections(5)

sendactivity("clearalltriangles")
sendactivity("clearallcontours")
sendactivity("clearallpoints")

contB = [(pl["t"]/1000, pl["p"])  for pl in rl.plines  if pl["C"] == "B"]
contA = [(pl["t"]/1000, pl["a"]*0.1)  for pl in rl.plines  if pl["C"] == "Q"]

sendactivity("contours", contours=[[(t/60, (p-100000)/100)  for t, p in contA]], materialnumber=0)
sendactivity("contours", contours=[[(t/60, a/10)  for t, a in contB]], materialnumber=1)

pts = []
i = 0
for t, a in contA:
    while i < len(contB) and contB[i][0] < t:
        i += 1
    if i == len(contB):
        break
    pts.append((a, contB[i][1]))
sendactivity("points", points=[(a/10, (p-100000)/100)  for a, p in pts], materialnumber=3)

# find least squares to fit p -> a
sa = sum(a  for a, p in pts)
sa2 = sum(a**2  for a, p in pts)
sap = sum(a*p  for a, p in pts)
sp = sum(p  for a, p in pts)
sp2 = sum(p**2  for a, p in pts)

mb = (sap*len(pts) - sa*sp) / (sp2*len(pts) - sp*sp)
cb = (sa - mb*sp)/len(pts)
sendactivity("contours", contours=[[((mb*p + cb)/10, (p-100000)/100)  for p in [80000, 110000]]])



Rpl = rl.Cpl["R"]
Qpl = rl.Cpl["Q"]
latdivisorE100, latdivisorN100 = Rpl["e"], Rpl["n"] 
lngdivisorE100, lngdivisorN100 = Rpl["f"], Rpl["o"]

fac = 0.1
cont = [ ]
for pl in rl.plines:
    if pl["C"] == 'Q':
        rx = 100*(pl["x"]-Qpl["x"])
        ry = 100*(pl["y"]-Qpl["y"])
        me, mn = ry/latdivisorE100+rx/lngdivisorE100, ry/latdivisorN100+rx/lngdivisorN100
        x, y = me*fac, mn*fac
        cont.append((x, y, (pl["a"]-0*Qpl["a"])*0.1*fac))
sendactivity("contours", contours=[cont], materialnumber=2)

# maybe should do some smoothing on B to get this
cont = [ ]
pb = 0
for pl in rl.plines:
    if pl["C"] == 'B':
        pb = pl["p"]
    if pl["C"] == 'Q':
        rx = 100*(pl["x"]-Qpl["x"])
        ry = 100*(pl["y"]-Qpl["y"])
        me, mn = ry/latdivisorE100+rx/lngdivisorE100, ry/latdivisorN100+rx/lngdivisorN100
        x, y = me*fac, mn*fac
        cont.append((x, y, (pb*mb + cb)*fac))
sendactivity("contours", contours=[cont], materialnumber=3)



