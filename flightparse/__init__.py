import math, re
from basicgeo import P3, P2, Along
from .gpsproc import LoadGPS, cubecoordinates, LoadOrient
from .lighthumidtempproc import LoadLHT
from .windbaroproc import LoadWind, LoadBaro, CalcZVelocity
from .utils import ziptvs, ziptvsG, TrapeziumAvg, expfilter, SimpleLinearRegressionR, AirDensity

# kinematic model between light intensity and temperature rise on the humidity meter

# now replot short sections for analysis
# start to detect moments of start climb and get temp changes
# do the baro and alt comparisons
# get the gpsseq things on a timestamp!

# plot cumulative light intensity against temperature rise 
# take a clean range like so: 215.23825198331176, 218.96524997907602
# then select 5 second sequences and plot light intensity against temperature rise randomly
# find the parameters that give the cleanest signal
# the temperatures have clear peaks
# does this correspond with the light being taken off (forward and back from that time?)

# much faster code for processing
rm = None
class RM:
    def __init__(self, fname, sendactivity):
        global rm
        rm = self
        self.fname = fname
        self.sendactivity = sendactivity
        
        # plotting scale factors 
        self.gfac = 0.01
        self.gzfac = 0.05
        self.tfac = 1/60000.0
        self.bsoffs = 99000
        self.bfac = -0.01
        self.boffs = 30
        self.bboffs = 30
        self.flightT0 = 0
        
        self.tpclicks = [ ]  # [ (t, p) ]
        self.gdatetime = "notset" 
        if sendactivity:
            sendactivity("clearallcontours")
            sendactivity("clearalltriangles")
        self.dtmps, self.humid, self.light = None, None, None
        self.Shumid, self.Ghumid, self.irtmp = None, None, None
        self.baro, self.bbaro = None, None
        self.wind, self.dbaro = None, None
        self.orient = None
        self.gpsseq, self.gpsseqpreflight, self.gpsseqpostflight = None, None, None
        self.zvelocity = None
        
            # D(22934) H(38451) L(36148) 
        LoadLHT(self)  # self.dtmps = [ [ (t, c), ...] ], self.humid = [ (t, h, c), ... ], self.light = [ (t, l), ... ]
            # R(1) Q(171114) V(170804) 
        LoadGPS(self)  # self.gpsseq = [ (t,x,y,z), ... ]
            # Y(8) Z(1577159) 
        # LoadOrient(self) # should be time filtered
            # W(2169375) F(705758) 
        # LoadWindBaro(self)

    def applygzfac(self, p):
        return P3(self.p[0]*self.gfac, self.p[1]*self.gfac, self.p[2]*self.gzfac)
    def applybfac(self, b):
        return (b - self.bsoffs)*self.bfac + self.boffs    
    
# function here for trimming the gps to the actual flight path start and finish
# load and plot temperature at moment of change at initial parts of climbs
# load and plot barometer
# load and subplot orientation (by selection) (pitch and bank)
# also look at rate of turn


    def plottimemark(self, mt, mp):
        self.sendactivity(contours=[[(mt*self.tfac, -50), (mt*self.tfac, 250)]], materialnumber=2)
        if mp is not None:
            self.sendactivity(points=[(mp[0]*self.gfac, mp[1]*self.gfac, mp[2]*self.gzfac)])

    def thingen(self, tseq, trg=None):
        if not tseq:
            return []
        i0, i1 = 0, len(tseq)
        if trg != "all" and (len(self.tpclicks) >= 2 or trg):
            t0, t1 = trg if trg else sorted([self.tpclicks[-2][0], self.tpclicks[-1][0]])
            i0 = min(i  for i, v in enumerate(tseq)  if t0 < v[0])
            i1 = max(i  for i, v in enumerate(tseq)  if t1 > v[0])
        else:
            t0, t1 = tseq[0][0], tseq[-1][0]
            i0, i1 = 0, len(tseq)
        istep = int(1 + (i1 - i0)/5000)
        print("istep of %d across %d samples avg %dms" % (istep, i1 - i0, (t1 - t0)/(i1 - i0)))
        return tseq[i0:i1:istep]

    def thingenL(self, tseqseq, trg=None):
        if not tseqseq:
            return [[]]
        if trg != "all" and (len(self.tpclicks) >= 2 or trg):
            t0, t1 = trg if trg else sorted([self.tpclicks[-2][0], self.tpclicks[-1][0]])
            i0 = min((j,i)  for j, tseq in enumerate(tseqseq)  for i, v in enumerate(tseq)  if t0 < v[0])
            i1 = max((j,i)  for j, tseq in enumerate(tseqseq)  for i, v in enumerate(tseq)  if t1 > v[0])
        else:
            t0, t1 = tseqseq[0][0][0], tseqseq[-1][-1][0]
            i0, i1 = (0,0), (len(tseqseq)-1, len(tseqseq[-1]))
        nsteps = i1[1] + sum(map(len, tseqseq[i0[0]:i1[0]])) - i0[1]
        istep = int(1 + nsteps/5000)
        print("istep of %d across %d samples avg %dms" % (istep, nsteps, (t1 - t0)/nsteps))
        if i0[0] == i1[0]:
            return [ tseqseq[i0[0]][i0[1]:i1[1]:istep] ]
        return [ tseqseq[i0[0]][i0[1]::istep] ] + [tseq[::istep]  for tseq in tseqseq[i0[0]:i1[0]] ] + [ tseqseq[i1[0]][:i1[1]:istep] ]
    
    def plot(self, cmd):
        if cmd in ["baro", "altbaroregress", "entropy", "zvel"] and self.baro is None:
            LoadBaro(self, "F")
        
        if cmd == "gps":
            mn = 3
            if not self.tpclicks:
                self.sendactivity(contours=cubecoordinates(1000*self.gfac, 1000*self.gzfac, P3(0,0,self.gpsseq[0][3]*self.gzfac)), materialnumber=3)
                mn = 0
            self.sendactivity(contours=[[(x*self.gfac, y*self.gfac, z*self.gzfac)  for t, x, y, z in self.thingen(self.gpsseq)]], materialnumber=mn)
            self.plottimemark(self.gpsseq[0][0], P3(*self.gpsseq[0][1:]))
            self.plottimemark(self.gpsseq[-1][0], P3(*self.gpsseq[-1][1:]))
        elif cmd == "light":
            self.sendactivity(contours=[[(t*self.tfac, l*10)  for t, l in self.thingen(self.light)]], materialnumber=3)
        elif cmd == "humid":
            self.sendactivity(contours=[[(t*self.tfac, h*10)  for t, h, c in self.thingen(self.humid)]], materialnumber=2)
            self.sendactivity(contours=[[(t*self.tfac, h*10)  for t, h, c in self.thingen(self.Shumid)]], materialnumber=2)
            self.sendactivity(contours=[[(t*self.tfac, h*10)  for t, h, c in self.thingen(self.Ghumid)]], materialnumber=2)
        elif cmd == "tmps":
            self.sendactivity(contours=[[(t*self.tfac, c)  for t, h, c in self.thingen(self.humid)]], materialnumber=1)
            self.sendactivity(contours=[[(t*self.tfac, c)  for t, c in self.thingen(dtmp)]  for dtmp in self.dtmps], materialnumber=1)
            self.sendactivity(contours=[[(t*self.tfac, c)  for t, h, c in self.thingen(self.Shumid)]], materialnumber=1)
            self.sendactivity(contours=[[(t*self.tfac, c)  for t, h, c in self.thingen(self.Ghumid)]], materialnumber=1)
            self.sendactivity(contours=[[(t*self.tfac, c)  for t, i, c in self.thingen(self.irtmp)]], materialnumber=1)
            self.sendactivity(contours=[[(t*self.tfac, i)  for t, i, c in self.thingen(self.irtmp)]], materialnumber=3)
            if self.dbaro:
                self.sendactivity(contours=[[(t*self.tfac, c)  for t, d, c in self.thingen(self.dbaro)]], materialnumber=3)
            
        elif cmd == "tgrid":
            if self.dtmps:
                t0, t1 = min(dtmp[0][0]  for dtmp in self.dtmps), max(dtmp[-1][0]  for dtmp in self.dtmps)
            else:
                sn = self.Shumid or self.Ghumid or self.irtmp
                t0, t1 = sn[0][0], sn[-1][0]
            self.sendactivity(contours=[[(t0*self.tfac, c), (t1*self.tfac, c)]  for c in range(0, 26)], materialnumber=1)
            self.sendactivity(contours=[[(t0*self.tfac, c), (t1*self.tfac, c)]  for c in range(0, 26, 5)], materialnumber=0)
        elif cmd == "grid":
            self.sendactivity(contours=[[(self.gpsseq[0][0]*self.tfac, 0), (self.gpsseq[-1][0]*self.tfac, 0)]], materialnumber=2)
            self.plottimemark(self.gpsseq[0][0], P3(*self.gpsseq[0][1:]))
            self.plottimemark(self.gpsseq[-1][0], P3(*self.gpsseq[-1][1:]))
            for t, p in self.tpclicks[-2:]:
                self.plottimemark(t, p)
        elif cmd == "wind":
            if self.wind is None:
                LoadWind(rm, True)
            self.sendactivity(contours=[[(t*self.tfac, w)  for t, w in self.thingen(self.wind)]], materialnumber=0)
            self.sendactivity(contours=[[(t*self.tfac, d*0.1)  for t, d, c in self.thingen(self.dbaro)]], materialnumber=3)
            
        elif cmd == "baro":
            self.sendactivity(contours=[[(t*self.tfac, self.applybfac(b))  for t, b in self.thingen(self.baro)]], materialnumber=0)
        elif cmd == "bbaro":
            if self.bbaro is None:
                LoadBaro(rm, 'B')
            self.sendactivity(contours=[[(t*self.tfac, self.applybfac(b))  for t, b in self.thingen(self.bbaro)]], materialnumber=1)
            
        elif cmd == "altbaroregress":
            tr = 2000
            tzzs = TrapeziumAvg(rm.flightT0, tr, [(t, z)  for t, x, y, z in rm.gpsseq  if t<rm.flightT1])
            tzbs = TrapeziumAvg(rm.flightT0, tr, rm.baro)
            self.sendactivity(points=[(z*0.1, self.applybfac(b))  for z, b in zip(tzzs, tzbs)])
            print("z against b", SimpleLinearRegressionR(zip(tzbs, tzzs)))
            print("b against z", SimpleLinearRegressionR(zip(tzzs, tzbs)))
        elif cmd == "entropy":
            t0 = self.flightT0
            tr = 3000
            baroexpfilter = expfilter(rm.baro, 0.09)  #  half life of 8 seconds to match temperature delay
            tzcs = TrapeziumAvg(t0, tr, rm.dtmps[0])
            tzfbs = TrapeziumAvg(t0, tr, baroexpfilter)
            self.sendactivity(contours=[[(((i+0.5)*tr+t0)*rm.tfac, rm.applybfac(b))  for i, b in enumerate(tzfbs)]], materialnumber=0)
            self.sendactivity(contours=[[(((i+0.5)*tr+t0)*rm.tfac, c)  for i, c in enumerate(tzcs)]], materialnumber=1)
            gamma = 1.4
            fftze = [ b**(1-gamma)*(c+273.16)**gamma  for b, c in zip(tzfbs, tzcs) ]
            eoffs = sum(fftze)/len(fftze)
            self.sendactivity(contours=[[(((i+0.5)*tr+t0)*rm.tfac, (e-eoffs)*50+10)  for i, e in enumerate(fftze)]], materialnumber=3)
        elif cmd == "zvel":
            if self.zvelocity is None:
                CalcZVelocity(self)
            self.sendactivity(contours=[[(t*rm.tfac, v)  for t, v in rm.thingen(self.zvelocity)]], materialnumber=1)
            self.sendactivity(contours=[[(0,0), (1000,0)]])

        elif cmd[:2] == "or":
            if self.orient is None:
                LoadOrient(self)
            if cmd == "org":
                for i in range(3):
                    self.sendactivity(contours=[[(t*self.tfac, g[i])  for t, a, q, g in thorientseg ]  for thorientseg in self.thingenL(self.orientsegs)], materialnumber=i)  
            if re.match("orq\d", cmd):
                i = int(cmd[3])
                tgs = [(t, P3(x*self.gfac, y*self.gfac, z*self.gzfac))  for t,x,y,z in self.thingen(self.gpsseq)]
                tor = [(t, q.VecDots()[i])  for t, a, q, g in self.orient  if tgs[0][0]<t<tgs[-1][0]]
                k = ziptvsG(tgs, tor)
                self.sendactivity(contours=[(p, p+v*0.2)  for t, p, v in k], materialnumber=i+1)
                
        else:
            print("cmds are gps, light, humid, tmps, wind, baro, orgz, orq0")
        
"""
import sys
sys.path.append("/home/goatchurch/datalogging/flightloganalysis")
from flightparse import RM
fname = "/home/goatchurch/hgstuff/2016-06-23-losergrimming/044.TXT"
rm = RM(fname, sendactivity)
rm.plot("gps")
rm.plot("grid")
rm.plot("tmps")
rm.plot("light")
rm.plot("humid")
"""

def pyclick(p, v):
    cp = P3(p[0]/rm.gfac, p[1]/rm.gfac, p[2]/rm.gzfac)
    vp = P3(v[0], v[1], v[2]*rm.gfac/rm.gzfac) 
    vhl = vp.LenLZ()
    if not rm.gpsseq:
        mt, mp = p[0]/rm.tfac, (0,0,0)
    elif vhl == 0:
        ml, mt, mp = min(((cp.x - x)**2 + (cp.y - y)**2, t, (x,y,z))  for t,x,y,z in rm.gpsseq)

        # check against the zero timeline
        t0, t1 = rm.gpsseq[0][0], rm.gpsseq[-1][0]
        tmt = p[0]/rm.tfac
        if (t0 < tmt < t1) and p[1]**2 < ml:
            mt, mp = min(((t, (x,y,z))  for t,x,y,z in rm.gpsseq), key=lambda X: abs(X[0] - tmt))
    else:
        vhp = P3.ConvertGZ(P2.ConvertLZ(vp)*(vp.z/vhl), -vhl)
        vhc = P3.Cross(vp, vhp)
        ml, mt, mp = min((P3.Dot(P3(x,y,z) - cp, vhp)**2 + P3.Dot(P3(x,y,z) - cp, vhc)**2, t, (x,y,z))  for t, x,y,z in rm.gpsseq)
    mp = P3(*mp)
    tprev = rm.tpclicks and rm.tpclicks[-1][0] or 0
    pprev = rm.tpclicks and rm.tpclicks[-1][1] or mp
    print("t=%d p=(%.3f, %.3f, %.3f)  %dmins %dsecs %s, hdist=%.2fm vdist=%.2fm" % (mt, mp[0], mp[1], mp[2], int(abs(tprev-mt)/60000), int(abs(tprev-mt)/1000)%60, ("earlier" if tprev > mt else "later"), (mp - pprev).LenLZ(), (mp[2] - pprev[2])))
    rm.tpclicks.append((mt, mp))
    rm.plottimemark(mt, mp)
    return (mt, mp)

