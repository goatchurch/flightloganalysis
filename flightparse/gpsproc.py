import math, re
from basicgeo import P3, P2, Along

def cubecoordinates(edgelength, zedgelength, origp=P3(0,0,0)):
    cs = [P3(0,0,0), P3(0,edgelength,0), P3(edgelength,edgelength,0), P3(edgelength,0,0), P3(0,0,0)]
    conts = [ [c+lcs  for lcs in cs]  for c in [origp, origp+P3(0,0,zedgelength)] ]
    for lcs in cs[:-1]:
        conts.append([origp+lcs, origp+lcs+P3(0,0,zedgelength)])
    return conts

def LoadGPS(self):
    fin = open(self.fname)
    for line in fin:  
        if not line.strip():  
            break
    self.lx0, self.ly0 = None, None
    px, py, pz = 0, 0, 0
    self.gpsseq = [ ]
    self.gpsseqbad = [ ]
    ngps, nconsecutivediscards = 0, 0
    for line in fin:
        if line[0] == "Q":  # Qt00003371u03930AC8y01B42975x007E088Fa1C46
            t = int(line[2:10], 16)
            u = int(line[11:19], 16)
            ly = int(line[20:28], 16)
            lx = int(line[29:37], 16)
            a = int(line[38:42], 16)
            if self.lx0 is None:
                self.lx0, self.ly0 = lx, ly
                earthrad = 6378137
                lyfac = 2*math.pi*earthrad/360/600000
                exfac = lyfac*math.cos(math.radians(self.ly0/600000))
                nconsecutivediscards = 0
                px, py, pz = 0, 0, a*0.1
            x, y, z = (lx - self.lx0)*exfac, (ly - self.ly0)*lyfac, a*0.1
            if (abs(x - px) < 200 and abs(y - py) < 200 and abs(z - pz) < 150):
                self.gpsseq.append((t, x, y, z))
                px, py, pz = x, y, z
                nconsecutivediscards = 0
            else:
                if nconsecutivediscards >= 5 and ngps < 500:
                    self.lx0 = None  # start again
                self.gpsseqbad.append((t, x, y, z))
                nconsecutivediscards += 1
            ngps += 1

        # should interpret the V values Vt000129B0v002Bd00839C
                
        elif line[0] == "R": # Rt00019ECDd"2016-06-22T10:57:01.600"e000F423Fn00000682f00000682o000F423F  
            self.gdatetime = re.search('"([^"]*)"', line).group(1)

        #print("Start time %02d:%02d end %02d:%02d" % (int(Qpls0[0]["u"]/3600000), (int(Qpls0[0]["u"]/60000) % 60), int(Qpls0[-1]["u"]/3600000), (int(Qpls0[-1]["u"]/60000) % 60)))
        #self.t0, self.t1 = self.DeriveFlightTime(self.GetPath3()[0])
        #m, c = self.gpsclockconvert[0]
        #u0, u1 = self.t0*m + c, self.t1*m + c
        #print("Flight time start: %02d:%02d end %02d:%02d" % (int(u0/3600000), (int(u0/60000) % 60), int(u1/3600000), (int(u1/60000) % 60)))
        
    print("%d gps readings, discarding %d at: %s" % (len(self.gpsseq), len(self.gpsseqbad), self.gdatetime))
    if self.gpsseq:
        lt = self.gpsseq[-1][0]
        i0, i1 = DeriveFlightTime(self.gpsseq)
        #i0, i1 = 0, len(self.gpsseq) - 1
        self.gpsseqpreflight = self.gpsseq[:i0]
        self.gpsseqpostflight = self.gpsseq[i1:]
        self.gpsseq = self.gpsseq[i0:i1]
        
        # should backdate the GPS timings to the regular u-times
        print("Flight time %d minutes out of %d record minutes" % ((self.gpsseq[-1][0]-self.gpsseq[0][0])/60000, lt/60000))
        self.flightT0 = self.gpsseq[0][0]
        self.flightT1 = self.gpsseq[-1][0]


def DeriveFlightTime(gpsseq):
    def d2d(i, j):
        return (P2(gpsseq[i][1], gpsseq[i][2]) - P2(gpsseq[j][1], gpsseq[j][2])).Len()
    dis, dds = 10, 15
    i0, i1 = 0, len(gpsseq) - 1
    for i in range(dis, len(gpsseq) - dis*5):
        tdiff = (gpsseq[i][0] - gpsseq[i-dis][0])*0.001
        vv = (P2(gpsseq[i][1], gpsseq[i][2]) - P2(gpsseq[i-dis][1], gpsseq[i-dis][2])).Len()/tdiff
        if vv > 1.1:
            # extra filter if we still have significant movement 15 seconds later
            tdiff1 = (gpsseq[i+dis*dds][0] - gpsseq[i+dis*(dds-1)][0])*0.001
            vv1 = d2d(i+dis*dds, i+dis*(dds-1))/tdiff1
            if vv1 > 2.1:  
                i0 = i-dis
                break
                
    # usually the gps has stabilized by the end of the flight so only need one stationary test
    for i in range(len(gpsseq)-1, dis-1, -1):
        tdiff = (gpsseq[i][0] - gpsseq[i-dis][0])*0.001
        vv = d2d(i, i-dis)/tdiff
        if vv > 1.1:
            i1 = i
            break
    return i0, i1



from basicgeo import P3, Quat

# vector from top of control frame to corner of base bar with keel pointing due north (0,1,0)
magdeclination = 0
controlbase = P3(69.0, 42.06344442205491, -156.2492943010685) 
magquat = Quat(math.cos(math.radians(-magdeclination/2)), 0, 0, math.sin(math.radians(-magdeclination/2)))

# vector from top of control frame to corner of right hand basebar is aligned with vector0 of device on that upright
uprightdihedral = math.atan2(controlbase.x, -controlbase.z)
keelrot = Quat(math.cos(uprightdihedral/2), 0, -math.sin(uprightdihedral/2), 0)
uprightrake = math.asin(controlbase.y/controlbase.Len())
keelrake = Quat(math.cos(uprightrake/2), -math.sin(uprightrake/2), 0, 0)

uprightvert = keelrake*keelrot
uprightvert = keelrot  # for now


def LoadOrient(self):
    fin = open(self.fname)
    for line in fin:  
        if not line.strip():  
            break

    def s16(sx):  
        x = int(sx, 16)
        return x - 65536 if x >= 32768 else x
        
    raworient = [ ]
    badraworient = [ ]
    for line in fin:
        # Zt00002319 xFFFB yFFF4 z000D a016D b0346 cFE9E w242B x3065 yEAE4 z0004 s0000
        # 0123456789 01234 56789 01234 56789 01234 56789 01234 56789 01234 56789 0123456789
        if line[0] == 'Z':
            t = int(line[2:10], 16)
            a = P3(s16(line[11:15])*0.01, s16(line[16:20])*0.01, s16(line[21:25])*0.01)                      # acceleration
            g = P3(s16(line[26:30])*0.01, s16(line[31:35])*0.01, s16(line[36:40])*0.01)       # gravity
            q = Quat(s16(line[41:45]), s16(line[46:50]), s16(line[51:55]), s16(line[56:60]))  # quaternion 
            s = line[61:65]       # calibration state

            if min(g) < -13 or max(g) > 13 or abs(P3(*g).Len() - 9.8) > 0.2:
                badraworient.append((t, g))
                continue
            raworient.append((t, a, q, g))

    print("throwing away %d readings leaving %d orient readings" % (len(badraworient), len(raworient)))
    self.orient = raworient
    if not raworient:
        return
        
    g1avg = sum(g[1]  for t, a, q, g in self.orient)/len(self.orient)
    g1std = math.sqrt(sum((g[1] - g1avg)**2  for t, a, q, g in self.orient)/len(self.orient))
    g1stdf = g1std*3
    raworient = [ (t, a, q, g)  for t, a, q, g in raworient  if abs(g[1] - g1avg) < g1stdf ]
    print("thinning a further %d readings for g[1] being %f away from avg %f" % (len(self.orient) - len(raworient), g1stdf, g1avg))
    self.orient = raworient
    
    orientsegs = [ [ ] ]
    for t, a, q, g in self.orient:
        if not orientsegs[-1] or orientsegs[-1][-1][0] > t - 200:
            orientsegs[-1].append((t, a, q, g))
        else:
            orientsegs.append([])
    self.orientsegs = [ orientseg  for orientseg in orientsegs  if orientseg and orientseg[-1][0]-orientseg[0][0] > 10000 ]
    nmaxgap = max((ors1[0][0] - ors0[-1][0]  for ors0, ors1 in zip(self.orientsegs, self.orientsegs[1:])), default=0)
    print("segmenting orientation to %d pieces with at most %d seconds between them" % (len(self.orientsegs), nmaxgap/1000))

