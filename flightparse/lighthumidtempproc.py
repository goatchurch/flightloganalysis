import math, re

def LoadLHT(self):
    fin = open(self.fname)
    
    for line in fin:  
        if not line.strip():  
            break

    self.dtmps, self.humid, self.light = [ ], [ ], [ ]
    self.Shumid, self.Ghumid, self.irtmp = [ ], [ ], [ ]
    for line in fin:
        if line[0] == "D":  # Dt00001795i00r0113
            t = int(line[2:10], 16)
            i = int(line[11:13], 16)
            r = int(line[14:18], 16)
            while len(self.dtmps) <= i:
                self.dtmps.append([])
            self.dtmps[i].append((t, r/16))
        elif line[0] == "H": # Ht0000183Ar6FCEa5E3C
            t = int(line[2:10], 16)
            r = int(line[11:15], 16)
            a = int(line[16:20], 16)
            h = -6 + (125 * ((r & 0xFFFC) / 65536.0))
            c = -46.85 + (175.72 * ((a & 0xFFFC) / 65536.0)) 
            self.humid.append((t, h*0.01, c))
        elif line[0] == "L": # Lt0000186Cl0056
            t = int(line[2:10], 16)
            l = int(line[11:15], 16)
            self.light.append((t, l/1024.0))
        elif line[0] == 'S': # St001D8FBBr6CF8a5EEB
            t = int(line[2:10], 16)
            r = int(line[11:15], 16)
            a = int(line[16:20], 16)
            h = (r*100.0)/0xFFFF
            c = (a*175.0)/0xFFFF - 45.0 
            self.Shumid.append((t, h*0.01, c))
        elif line[0] == 'G': # Gt001D9036r6194a6184
            t = int(line[2:10], 16)
            r = int(line[11:15], 16)
            a = int(line[16:20], 16)
            h = (r*125.25)/65536 - 6
            c = (a*175.25/65536) - 46.85 
            self.Ghumid.append((t, h*0.01, c))
        elif line[0] == 'I': # It001D9020r392Ea3992
            t = int(line[2:10], 16)
            r = int(line[11:15], 16)
            a = int(line[16:20], 16)
            i = r*0.02 - 273.15
            c = a*0.02 - 273.15
            self.irtmp.append((t, i, c))
            

"""
# relate the temperature with the light
c = 0.025
k = [self.light[i][0]  for i in range(1, len(self.light))  if self.light[i][1] < c and self.light[i-1][1] >= c]
sendactivity(contours=[[(t*self.tfac, 0), (t*self.tfac, 50)]  for t in k])
"""


def convexpbiway(tvs, fac):
    t0 = tvs[-1][0]
    sv = tvs[-1][1]
    rtvs = [ ]
    for t, v in reversed(tvs):
        sv = sv*(1-fac) + v*fac
        rtvs.append((t, sv))
    rtvs.reverse()
    t0 = rtvs[0][0]
    sv = rtvs[0][1]
    res = [ ]
    for t, v in rtvs:
        sv = sv*(1-fac) + v*fac
        res.append((t, sv))
    return res

# matches the temperature spikes with the light (doesn't seem to match the troughs of shadow very accurately)
def PlotHTempSpikes(rm):
    tvs = [ (t, c)  for t, h, c in rm.humid ]
    tvs1 = convexpbiway(tvs, 0.6)
    tvs2 = convexpbiway(tvs1, 0.6)
    tfac = rm.tfac
    rm.sendactivity(contours=[[(t*rm.tfac, v)  for t, v in tvs]], materialnumber=1)
    rm.sendactivity(contours=[[(t*rm.tfac, v)  for t, v in tvs2]], materialnumber=3)

    riseseq = [ ]
    i0 = 0
    for i in range(1, len(tvs2)):
        if tvs2[i][1] < tvs2[i-1][1]:
            if i > i0 + 3:
                riseseq.append((i0, i-1))
            i0 = i

    rm.sendactivity(contours=[[(tvs2[i0][0]*tfac, tvs2[i0][1]), (tvs2[i1][0]*tfac, tvs2[i1][1])]  for i0, i1 in riseseq], materialnumber=1)
    rm.sendactivity(contours=[[(tvs2[i0][0]*tfac, 0), (tvs2[i1][0]*tfac, tvs2[i1][1]-tvs2[i0][1])]  for i0, i1 in riseseq], materialnumber=1)

    light1 = convexpbiway(rm.light, 0.6)
    light2 = convexpbiway(light1, 0.6)
    rm.sendactivity(contours=[[(t*rm.tfac, v*10)  for t, v in rm.light]], materialnumber=3)
    rm.sendactivity(contours=[[(t*rm.tfac, v*10)  for t, v in light2]], materialnumber=0)
