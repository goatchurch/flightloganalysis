import math
from .utils import TrapeziumAvg, AirDensity

def LoadWind(self, bAverageTrap):
    fin = open(self.fname)
    for line in fin:  
        if not line.strip():  
            break

    rawwind = [ ]
    self.dbaro = [ ]
    for line in fin:
        if line[0] == 'W':   # Wt001DAB0Cw70F6n000000C0
            t = int(line[2:10], 16)
            w = int(line[11:15], 16) 
            n = int(line[16:24], 16)
            rawwind.append((t, w))
        elif line[0] == 'X': # Xt001D8FBCd2037r02BD
            t = int(line[2:10], 16)
            d = int(line[11:15], 16)
            r = int(line[16:20], 16)
            p = (d*(1.0/(0x3FFF*0.4)) - 1.25)*6894.75728  
            c = r*(200.0/0x7FF) - 50; 
            self.dbaro.append((t, p, c))

    if bAverageTrap:
        tstep = 100
        t0 = int(rawwind[0][0]/1000)*1000
        wvs = TrapeziumAvg(t0, tstep, [(t, 80000/w)  for t, w in rawwind])
        self.wind = [(t0+(i+0.5)*tstep, v)  for i, v in enumerate(wvs)]
        print("wind steps of %.2fseconds" % (tstep*0.001))
    else:
        self.wind = rawwind
        

def LoadBaro(self, baroletter='F', bAverageTrap=False):
    assert baroletter in ['B', 'F']
    fin = open(self.fname)
    for line in fin:  
        if not line.strip():  
            break

    rawbaro = [ ]
    badradbaro = [ ]
    for line in fin:
        if line[0] == baroletter:
            t = int(line[2:10], 16)
            p = int(line[11:17], 16) 
            if 60000<p<110000:   # filter the very worst spikes
                rawbaro.append((t, p))
            else:
                badradbaro.append((t, p))
                
    # lower level spike detection
    nspikeavginterval2, ranbnd = 1000, 1200
    spikeindexes = set()
    for i0 in range(0, len(rawbaro)-nspikeavginterval2, nspikeavginterval2):
        i1 = min(i0 + nspikeavginterval2*2, len(rawbaro))
        avg = sum(b  for t, b in rawbaro[i0:i1])/(i1 - i0)
        spikeindexes.update(i+i0  for i, b in enumerate(rawbaro[i0:i1])  if abs(b[1]-avg) > ranbnd)
    assert not spikeindexes, ("baro spikes found", spikeindexes)
                
    if bAverageTrap:
        tstep = 100
        t0 = int(rawbaro[0][0]/1000)*1000
        bvs = TrapeziumAvg(t0, tstep, [(t, p)  for t, p in rawbaro])
        baro = [(t0+(i+0.5)*tstep, v)  for i, v in enumerate(bvs)]
        print("baro steps of %.2fseconds, which should be despiked and fitted back timed to the stepgap" % (tstep*0.001))
    else:
        baro = rawbaro

    if len(baro):
        bsoffs = sum(b  for t, b in baro)/len(baro)
    if baroletter == 'F':
        self.baro, self.bsoffs = baro, bsoffs
    else:
        self.bbaro, self.bbsoffs = baro, bsoffs


def CalcZVelocity(self):
    tr = 3000
    tzcs = TrapeziumAvg(0, tr, self.dtmps[0])
    tzhs = [ 60 ] # humidity TrapeziumAvg(0, tr, rm.dtmps[0])
    
    bgap = 250    # equivalent to an interval average
    sumdt = 0
    g = 9.81
    self.zvelocity = [ ]
    for (t0, b0), (t1, b1) in zip(self.baro, self.baro[bgap:]):
        dt = t1 - t0
        db = b1 - b0
        bm = (b0+b1)/2
        tm = (t0+t1)/2
        ic = int((tm - 0)/tr)
        at = tzcs[max(0, min(len(tzcs)-1, ic))]
        ah = tzhs[max(0, min(len(tzhs)-1, ic))]
        ad = AirDensity(bm, at, ah)
        dz = -db/g*ad
        vz = dz/(dt*0.001)
        self.zvelocity.append((tm, vz))
        sumdt += dt
    print("avg tgap", sumdt/len(self.zvelocity), "ms")    

"""
    # Wt00001670wFFFFn00000004
    if line[0] == 'W':  
        return {"C":"W", "t":int(line[2:10], 16), "w":int(line[11:15], 16), "n":int(line[16:24], 16) }

    # Zt00002319 xFFFB yFFF4 z000D a016D b0346 cFE9E w242B x3065 yEAE4 z0004 s0000
    # 0123456789 01234 56789 01234 56789 01234 56789 01234 56789 01234 56789 0123456789
    if line[0] == 'Z':
        return {"C":"Z", "t":int(line[2:10], 16), 
                "ax":int(line[11:15], 16), "ay":s16(int(line[16:20], 16)), "az":s16(int(line[21:25], 16)),  # acceleration
                "a":s16(int(line[26:30], 16)), "b":s16(int(line[31:35], 16)), "c":s16(int(line[36:40], 16)), # gravity vector
                "w":s16(int(line[41:45], 16)), "x":s16(int(line[46:50], 16)), "y":s16(int(line[51:55], 16)), "z":s16(int(line[56:60], 16)), # quaternion
                "s":int(line[61:65], 16) } # calibration state

    # Ft0056F8DAp0129C9
    if line[0] == 'F':  
        return {"C":"F", "t":int(line[2:10], 16), "p":int(line[11:17], 16) }
"""
