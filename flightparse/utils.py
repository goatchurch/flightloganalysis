from math import exp, sqrt

def Along(lam, a, b):
    if a == b:
        return a
    return a * (1 - lam) + b * lam

def AlongG(lam, a, b):
    if a == b:
        return a
    vres = a * (1 - lam) + b * lam
    vlen = a.Len() * (1 - lam) + b.Len() * lam
    return vres*(vlen/(vres.Len() or 1))

def ziptvsA(tvs, tvs1, lAlong):
    tvsres = [ ]
    i, j = 0, 1
    while tvs[i][0] < tvs1[j][0]:
        i += 1
    while tvs1[j][0] < tvs[i][0]:
        j += 1
    while i < len(tvs) and j < len(tvs1):
        while tvs1[j][0] < tvs[i][0]:
            j += 1
            if j == len(tvs1):
                return tvsres
        assert tvs1[j-1][0] <= tvs[i][0] <= tvs1[j][0], ("out of range", tvs1[j-1][0], tvs[i][0], tvs1[j][0])
        lam = (tvs[i][0] - tvs1[j-1][0]) / (tvs1[j][0] - tvs1[j-1][0])
        p = lAlong(lam, tvs1[j-1][1], tvs1[j][1])
        tvsres.append(tvs[i]+(p,))
        i += 1
    return tvsres

def ziptvs(tvs, tvs1):
    return ziptvsA(tvs, tvs1, Along)
def ziptvsG(tvs, tvs1):
    return ziptvsA(tvs, tvs1, AlongG)

def TrapeziumAvg(tstart, tstep, tvs, szero=0):
    if type(tvs) == list:
        if len(tvs) <= 2:
            return []
        gtvs = (x  for x in tvs)
    else:
        gtvs = tvs
    res = [ ]
    st0 = tstart
    st1 = tstart + tstep
    lt, lv = tstart, gtvs.__next__()[1]
    s = szero
    for t, v in gtvs:
        while t > st1:
            sl = (v - lv)*(1/(t - lt)) if (t - lt) != 0 else szero
            mv = lv + sl*(st1 - lt)
            s = s + (lv + mv)*(st1 - lt)
            res.append(s*(0.5/tstep))
            st0 = st1
            st1 = st0 + tstep
            lt, lv = st0, mv
            s = szero
        if t >= tstart:
            assert t >= lt
            s = s + (lv + v)*(t - lt)
        lt, lv = t, v
    return res

def expfilter(tvs, rE):
    t0, v0 = tvs[0]
    ftvs = [ ]
    for t, v in tvs:
        v1 = v0 - (v - v0)*(exp(-(t - t0)*0.001*rE) - 1)
        ftvs.append((t, v1))
        t0, v0 = t, v1
    return ftvs


def SaturationVapourPressure(tempC):
    A, B, C = 8.1332, 1762.39, 235.66
    return 10**(A - B/(tempC + C))*133.322387415
    
def DewpointTemperature(tempC, humid):
    A, B, C = 8.1332, 1762.39, 235.66
    svp = 10**(A - B/(tempC + C))*133.322387415
    pvp = svp*humid/100
    return -C - B/(math.log10(pvp/133.322387415) - A)

# http://en.wikipedia.org/wiki/Density_of_air
def AirDensity(absolutepressure, tempC, relativehumidity100):
    saturationvapourpressure = SaturationVapourPressure(tempC)
    PPwatervapour = saturationvapourpressure*relativehumidity100/100
    PPdryair = absolutepressure - PPwatervapour
    Rdryair = 287.058
    Rwatervapour = 461.495
    tempK = tempC + 273.15
    return PPdryair/(Rdryair*tempK) + PPwatervapour/(Rwatervapour*tempK)

def SimpleLinearRegressionR(tvs):
    sumt, sumtsq, sumv, sumvsq, sumtv, n = 0, 0, 0, 0, 0, 0
    for t, v in tvs:
        sumt += t
        sumtsq += t*t
        sumv += v
        sumvsq += v*v
        sumtv += t*v
        n += 1
    m = (sumtv*n - sumt*sumv) / (sumtsq*n - sumt*sumt)
    c = sumv/n - m*sumt/n
    r = (sumtv*n - sumt*sumv) / sqrt((sumtsq*n - sumt*sumt)*(sumvsq*n - sumv*sumv))
    return m, c, r
    
