import math
import numpy as np
from basicgeo import P2

class Covar2:
    def __init__(self, pts=None):
        self.x2s, self.y2s = 0.0, 0.0
        self.xs, self.ys = 0.0, 0.0
        self.xys = 0.0
        self.n = 0                  # number of points
        if pts:
            for p in pts:
                self.add(p)
        
    def add(self, p):
        self.xs += p[0]
        self.ys += p[1]
        self.x2s += p[0]**2
        self.y2s += p[1]**2
        self.xys += p[0]*p[1]
        self.n += 1
        
    def merge(self, cm):
        self.xs += cm.xs
        self.ys += cm.ys
        self.x2s += cm.x2s
        self.y2s += cm.y2s
        self.xys += cm.xys
        self.n += cm.n
        
    def flatmat(self):
        n = self.n
        n2 = n*n
        mxx = self.x2s/n - self.xs*self.xs/n2
        mxy = self.xys/n - self.xs*self.ys/n2
        myx = mxy
        myy = self.y2s/n - self.ys*self.ys/n2

        m = np.array([[mxx, mxy], [mxy, myy]])
        ei = np.linalg.eig(m)
        if ei[0][0] < ei[0][1]:
            rsq = ei[0][0]
            nv = P2(ei[1][0,0], ei[1][1,0])
        else:
            rsq = ei[0][1]
            nv = P2(ei[1][0,1], ei[1][1,1])
        return (rsq, nv) # smaller rsq is better, nv is normal vector

