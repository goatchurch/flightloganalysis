# Description #

This code is for experimental analysis of various data from my hang-gliding flights, primarily from my 
[arduino kit](https://bitbucket.org/goatchurch/arduinosketchbook) wired up with a comprehensive set of sensors.

The *sendactivity()* function is part of the [twistcodewiki](https://bitbucket.org/goatchurch/twistcodewiki)
framework that allows for interactive coding. 

# License #

This code is released as Public Domain/WTFPL.  Do anything you like with it.



