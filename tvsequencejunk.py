-----
alpha-beta filter:

fin = open("/home/goatchurch/datalogging/datastreams/homefridge2014-11-17.TXT")
cont = [ ]
cont2 = [ ]
mxt = 0
for line in fin:
    t = int(line[6:], 16)
    if line[0] == 'C':
        cont.append((t*0.001/60*0.2, int(line[2:6], 16)/16.0))
        mxt = t
    else:
        cont2.append((t*0.001/60*0.2, int(line[2:6], 16)/1023.0*20))

a, b = 0.04, 0.00005
dt = 0.5
vk, dvk = cont[0][1], 0
cont3 = [ ]
for t, v in cont:
    vk += dvk * dt   # add on velocity
    verr = v - vk    # error measurement
    vk += a * verr   # pull value to measured value
    dvk += (b * verr) / dt  # pull velocity in direction of difference
    cont3.append((t, vk)) 
sendactivity("clearallcontours")
sendactivity("contours", contours=[cont])
sendactivity("contours", contours=[cont3], materialnumber=1)

tm = cont[-1][0]
sendactivity("contours", contours=[[(0,c),(tm,c)]  for c in range(21)], materialnumber=1)
sendactivity("contours", contours=[[(0,c),(tm,c)]  for c in range(0,21,5)], materialnumber=3)
sendactivity("contours", contours=[[(t*0.001/60*0.2,0),(t*0.001/60*0.2,20)]  for t in range(0, int(mxt), 1000*3600)], materialnumber=1)

--------------

import math

k = [math.exp(-n*n/150)  for n in range(-16, 17)]
sk = sum(k)
k = [x/sk  for x in k]
print(k)

cont3 = [ ]
for i in range(len(cont) - len(k)):
    xk = sum(x[1]*kx for x, kx in zip(cont[i:], k))
    cont3.append((cont[i][0], xk)) 
sendactivity("clearallcontours")
sendactivity("contours", contours=[cont])
sendactivity("contours", contours=[cont3], materialnumber=1)

tm = cont[-1][0]
sendactivity("contours", contours=[[(0,c),(tm,c)]  for c in range(21)], materialnumber=1)
sendactivity("contours", contours=[[(0,c),(tm,c)]  for c in range(0,21,5)], materialnumber=3)
sendactivity("contours", contours=[[(t*0.001/60*0.2,0),(t*0.001/60*0.2,20)]  for t in range(0, int(mxt), 1000*3600)], materialnumber=1)



k = [math.exp(-n*n/150)  for n in range(-16, 17)]
sk = sum(k)
k = [x/sk  for x in k]
print(k)

cont3 = [ ]
for i in range(16, len(cont) - len(k)):
    xk = sum(x[1]*kx for x, kx in zip(cont[i-16:], k))
    cont3.append((cont[i][0], xk)) 

-----------------------------



fin = open("/home/goatchurch/datalogging/datastreams/doesfridge2014-11-20nightM.TXT")
cont = [ ]
cont2 = [ ]
cont4 = [ ]
mxt = 0
Mmt = 0
for line in fin:
    t = int(line[6:], 16)
    lt = t*0.001/60*0.2
    if line[0] == 'C':
        cont.append((lt, int(line[2:6], 16)/16.0))
        mxt = t
    elif line[0] == 'L':
        cont2.append((lt, int(line[2:6], 16)/1023.0*20))
    elif line[0] == 'A':
        anat = int(line[2:6], 16)/300.0 - 22
        cont4.append((lt, anat))
            
print(len(cont))
print(len(cont2))
print(len(cont4))


sendactivity("clearalltriangles")
sendactivity("clearallcontours")
sendactivity("contours", contours=[cont4], materialnumber=3)
sendactivity("contours", contours=[cont], materialnumber=0)


sendactivity("contours", contours=[cont2], materialnumber=2)
  
tm = cont[-1][0]
sendactivity("contours", contours=[[(0,c),(tm,c)]  for c in range(21)], materialnumber=1)
sendactivity("contours", contours=[[(0,c),(tm,c)]  for c in range(0,21,5)], materialnumber=3)
sendactivity("contours", contours=[[(t*0.001/60*0.2,0),(t*0.001/60*0.2,20)]  for t in range(0, int(mxt), 1000*3600)], materialnumber=1)



--------------------------
fin = open("/home/goatchurch/datalogging/datastreams/doesfridge2014-11-18afternoon.TXT")
cont = [ ]
cont2 = [ ]
mxt = 0
for line in fin:
    t = int(line[6:], 16)
    lt = t*0.001/60*0.2
    if line[0] == 'C':
        cont.append((lt, int(line[2:6], 16)/16.0))
        mxt = t
    elif not cont2 or cont2[-1][0] < lt - 0.01:
        cont2.append((lt, int(line[2:6], 16)/1023.0*20))
print(len(cont))
print(len(cont2))

sendactivity("clearalltriangles")
sendactivity("clearallcontours")
sendactivity("contours", contours=[cont])
sendactivity("contours", contours=[cont2], materialnumber=1)
tm = cont[-1][0]
sendactivity("contours", contours=[[(0,c),(tm,c)]  for c in range(21)], materialnumber=1)
sendactivity("contours", contours=[[(0,c),(tm,c)]  for c in range(0,21,5)], materialnumber=3)
sendactivity("contours", contours=[[(t*0.001/60*0.2,0),(t*0.001/60*0.2,20)]  for t in range(0, int(mxt), 1000*3600)], materialnumber=1)

px = (0,0)
def pyclick(x, v):
    global px
    print(x[0]-px[0], x[1] - px[1])
    px = x
          
    
-----------------------------

fin = open("/home/goatchurch/datalogging/datastreams/doesfridge2014-11-20nightM.TXT")
cont = [ ]
contL = [ ]
for line in fin:
    if line[0] == 'A':
        t = int(line[6:], 16)
        anat = int(line[2:6], 16)/300.0 - 22
        lt = t*0.001/60*0.2
        cont.append((lt, anat))
    if line[0] == 'L':
        t = int(line[6:], 16)
        anat = int(line[2:6], 16)/300.0 - 22
        lt = t*0.001/60*0.2
        contL.append((lt, 40*(anat+20)+20))
    if line[0] == 'C':
        t = int(line[6:], 16)
        anat = int(line[2:6], 16)/300.0 - 22
        lt = t*0.001/60*0.2
        cont.append((lt, 40*(anat+20)+20))

sendactivity("clearalltriangles")
sendactivity("clearallcontours")
sendactivity("contours", contours=[cont], materialnumber=0)
sendactivity("contours", contours=[contL], materialnumber=3)


def lf(cont):
    n = len(cont)
    st = sum(t  for t, v in cont)
    st2 = sum(t*t  for t, v in cont)
    stv = sum(t*v  for t, v in cont)
    sv = sum(v  for t, v in cont)
    sv2 = sum(v*v  for t, v in cont)
    m = (stv*n - st*sv)/(st2*n - st*st)
    return m, sv/n

sendactivity("clearallpoints")
pointsup = [ ]
pointsdown = [ ]
for i in range(10, len(cont)-5, 5):
    pointsup.append(lf(cont[i-5:i+5]))
    if cont[i-1][1]>cont[i+4][1]:
        pointsdown.append(pointsup.pop())
sendactivity("points", points=pointsup, materialnumber=2)
sendactivity("points", points=pointsdown, materialnumber=3)
sendactivity("contours", contours=[[(0,-100),(0,100)]], materialnumber=2)
    


-----------------------------
# plot some pulse data (with buzzing in the way)


fin = open("/home/goatchurch/datalogging/templogs/045.TXT")
cont = [ ]
for line in fin:
    if line[0] == 'P':
        t = int(line[7:], 16)
        pl = int(line[1:7], 16)*0.0001
        lt = t*0.001/60
        cont.append((lt, pl))

sendactivity("clearalltriangles")
sendactivity("clearallcontours")
sendactivity("contours", contours=[cont], materialnumber=0)

sendactivity("contours", contours=[[(i/60.,0), (i/60.,0.2)]  for i in range(1000)], materialnumber=1)







